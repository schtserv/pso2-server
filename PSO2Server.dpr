program PSO2Server;

uses
  Forms,
  fmain in 'fmain.pas' {Form1},
  UPlayer in 'UPlayer.pas',
  UEnc in 'UEnc.pas',
  libeay32 in 'libeay32.pas',
  UDebug in 'UDebug.pas',
  OpenSSLUtils in 'OpenSSLUtils.pas',
  Uerror in 'Uerror.pas',
  UDB in 'UDB.pas',
  Ulogin in 'Ulogin.pas',
  ULobby in 'ULobby.pas',
  UPsoUtil in 'UPsoUtil.pas',
  UMovement in 'UMovement.pas',
  UQuestBoard in 'UQuestBoard.pas',
  UParty in 'UParty.pas',
  UForest in 'UForest.pas',
  UInventory in 'UInventory.pas',
  UMonster in 'UMonster.pas',
  UShop in 'UShop.pas',
  UStats in 'UStats.pas',
  Pet_Mag in 'Pet_Mag.pas',
  casino in 'casino.pas',
  UUseItem in 'UUseItem.pas',
  UDrop in 'UDrop.pas',
  UFriendList in 'UFriendList.pas',
  UMail in 'UMail.pas',
  UPAid in 'UPAid.pas',
  Uskills in 'Uskills.pas',
  clientorder in 'clientorder.pas',
  UMag in 'UMag.pas',
  UAttack in 'UAttack.pas',
  UAttackID in 'UAttackID.pas',
  ItemGenerator in 'ItemGenerator.pas',
  GameEvent in 'GameEvent.pas',
  UTreasureShop in 'UTreasureShop.pas',
  UTicketShop in 'UTicketShop.pas',
  UTeamFnc in 'UTeamFnc.pas',
  UMyRoom in 'UMyRoom.pas',
  UTimerHelper in 'UTimerHelper.pas',
  UTitleAssistant in 'UTitleAssistant.pas',
  RoomAction in 'RoomAction.pas',
  UScriptEngine in 'UScriptEngine.pas',
  GameInteraction in 'GameInteraction.pas',
  StackTrace in 'StackTrace.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
