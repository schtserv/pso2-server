unit UMovement;

interface
uses windows,types,sysutils,UPlayer;

type

    tFullMotion = record
        UID:dword;
        Empty1:dword;
        D_04_1:dword;
        TUID:dword;
        Empty2:dword;
        D_04_2:dword;
        TimeStamp:single;
        Rotation:Tvec4;
        CurPos:Tvec4;
        unknownPos:Tvec4;
        ZoneID:dword;

    end;


Procedure Motion0704(x:integer;data:ansistring);
Procedure Motion7104(x:integer;data:ansistring);
procedure Motion0804(x:integer;data:ansistring);
procedure Motion3c04(x:integer;data:ansistring);
procedure PlayerMenuStatus(x:integer;s:ansistring);
procedure SpawnPlayer(x:integer;pp:integer=0);
procedure PlayerIsNotBusy(x:integer);
procedure PlayerIsBusy(x:integer);
procedure KickSomething(x:integer;s:ansistring);
procedure makeplayermove(x:integer);
procedure makeplayermove2(x:integer);
procedure GetMonsterAttention(x:integer;data:ansistring);


const ENT1_ID = 1;
        ENT1_TYPE = 2;
        ENT1_A = 4;
        ENT2_ID = 8;
        ENT2_TYPE = $10;
        ENT2_A = $20;
        TIMESTAMP = $40;
        ROT_X = $80;
        ROT_Y = $100;
        ROT_Z = $200;
        ROT_W = $400;
        CUR_X = $800;
        CUR_Y = $1000;
        CUR_Z = $2000;
        UNKNOWN4 = $4000;
        UNK_X = $8000;
        UNK_Y = $10000;
        UNK_Z = $20000;
        UNKNOWN5 = $40000;
        UNKNOWN6 = $80000;
        UNKNOWN7 = $100000;

        var testbox:integer = $30;

implementation

uses fmain, UPsoUtil, UInventory, UQuestBoard, UDebug, UForest, UStats,
  Ulogin, UUseItem, UParty, UDrop, Uskills, UAttack;

Procedure Motion0704(x:integer;data:ansistring);
var fm:^tFullMotion;
    s,a:ansistring;
    i,p:integer;
begin
    setlength(s,sizeof(tFullMotion));
    fillchar(s[1],sizeof(tFullMotion),0);
    s:=#$40#$00#$00#$00#$04#$07#$40#$00+s;
    fm:=@s[9];

    if data[7] <> #$40 then
    if player[x].last81 = '' then begin
        player[x].last81:=copy(data,9,6);
        {if data[7] <> #$70 then begin
        a:=#$48#$00#$00#$00#$04#$07#$70#$00#$9B#$6E#$02#$00#$00#$70#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$DC#$A6#$9B#$00#$00#$00
                +#$00#$00#$04#$00#$00#$00#$F1#$D2#$BD#$3F#$00#$00#$00#$3C#$00#$00
                +#$00#$00#$1B#$B8#$00#$00#$15#$42#$00#$00#$1B#$B8#$99#$3C#$F5#$C6
                +#$00#$00#$FE#$FF#$FF#$FF#$00#$00;
                move(player[x].last81[1],a[9],6);
                move(player[x].gid,a[$1b],4);
                move(player[x].CurPos,a[$33],8);
                move(player[x].Rotation,a[$2b],8);
                move(player[x].lastarea[1],a[$43],4);
                sendtolobby(player[x].room,x,a);
        end; }
        makeplayermove(x);
    end;

    //fill the data a bit
    if player[x].last81=copy(data,9,6) then begin
    fm.UID:=player[x].GID;
    fm.D_04_1:=4; //object type here
    fm.TUID:=player[x].GID;
    fm.D_04_2:=4;
    fm.Rotation:=player[x].Rotation;
    fm.CurPos:=player[x].CurPos;


    //unpack the data
    p:=$12;
    move(data[$f],i,3); //might be F
    if i and ENT1_ID <> 0 then begin
        move(data[p],fm.UID,4);
        inc(p,8);
    end;
    if i and ENT1_TYPE <> 0 then begin
        move(data[p],fm.D_04_1,2);
        inc(p,2);
    end;
    if i and ENT1_A <> 0 then begin
        move(data[p],pansichar(@fm.D_04_1)[2],2);  //unknown
        inc(p,2);
    end;
    if i and ENT2_ID <> 0 then begin
        move(data[p],fm.TUID,4);
        inc(p,8);
    end;
    if i and ENT2_TYPE <> 0 then begin
        move(data[p],fm.D_04_2,2);
        inc(p,2);
    end;
    if i and ENT2_A <> 0 then begin
        move(data[p],pansichar(@fm.D_04_2)[2],2); // unknown
        inc(p,2);
    end;
    if i and TIMESTAMP <> 0 then begin
        move(data[p],fm.timestamp,4);
        inc(p,4);
    end;
    if i and ROT_X <> 0 then begin
        move(data[p],fm.rotation.x,2);
        inc(p,2);
    end;
    if i and ROT_Y <> 0 then begin
        move(data[p],fm.rotation.y,2);
        inc(p,2);
    end;
    if i and ROT_Z <> 0 then begin
        move(data[p],fm.rotation.z,2);
        inc(p,2);
    end;
    if i and ROT_W <> 0 then begin
        move(data[p],fm.rotation.r,2);
        inc(p,2);
    end;
    if i and CUR_X <> 0 then begin
        move(data[p],fm.curpos.x,2);
        inc(p,2);
    end;
    if i and CUR_Y <> 0 then begin
        move(data[p],fm.curpos.y,2);
        inc(p,2);
    end;
    if i and CUR_Z <> 0 then begin
        move(data[p],fm.curpos.z,2);
        inc(p,2);
    end;
    if i and UNKNOWN4 <> 0 then begin
        move(data[p],fm.curpos.r,2);
        inc(p,2);
    end;
    if i and UNK_X <> 0 then begin
        move(data[p],fm.unknownPos.x,2);
        inc(p,2);
    end;
    if i and UNK_Y <> 0 then begin
        move(data[p],fm.unknownPos.y,2);
        inc(p,2);
    end;
    if i and UNK_Z <> 0 then begin
        move(data[p],fm.unknownPos.z,2);
        inc(p,2);
    end;
    if i and UNKNOWN5 <> 0 then begin
        move(data[p],fm.unknownPos.r,2);
        inc(p,2);
    end;
    if i and UNKNOWN6 <> 0 then begin
        if i and UNKNOWN7 <> 0 then
            fm.ZoneID:=byte(data[p])
        else
            move(data[p],fm.ZoneID,4);
    end;

    //update player data
    player[x].Rotation:=fm.Rotation;
    player[x].CurPos:=fm.CurPos;
    end;

    //send to all player in room
    Sendtolobby(player[x].room,x,data);

    //debug(2,datatohex(data));
    //debug(2,format('%.3f %.3f %.3f %.3f',[HalfPrecisionToFloat(fm.unknownPos.x)
    //,HalfPrecisionToFloat(fm.unknownPos.y),HalfPrecisionToFloat(fm.unknownPos.z)
    //,HalfPrecisionToFloat(fm.unknownPos.r)]));

    
    if player[x].firstping > 2 then  //prevent bas position warp
    if (player[x].floor > -1) then 
        TestWave(HalfPrecisionToFloat(player[x].CurPos.x),HalfPrecisionToFloat(player[x].CurPos.z),x,player[x].TeamID,player[x].Floor);
    {if player[x].Floor=0 then //objective
       if player[x].teammission < 3 then begin
        inc(player[x].teammission);
        if player[x].teammission = 3 then sendtoplayer(x,team[player[x].teamid].Mission);

       end;   }
end;

Procedure Motion7104(x:integer;data:ansistring);
var fm:^tFullMotion;
begin
    fm:=@data[9];
    if fm.UID = 0 then fm.UID:=fm.TUID;
    fm.TimeStamp:=0;
    
    sendtolobby(player[x].room,x,data);
end;

function testaction(s,an:ansistring):boolean;
var i:integer;
begin
    result:=false;
    if s = an then result:=true;
    for i:=length(s) downto 1 do
        if s[i] = 'g' then begin
            s[i]:='j'; //jump and ground
            break;
        end;
    if s = an then result:=true;
end;

function GenerateGenericAction(s:ansistring):tingameaction;
var x,i:integer;
    a,b:ansistring;
const
    Atype:array[0..16] of byte = (0,0,0,0,1,1,0,0,1,0,0,0,1,0,0,0,0);
    aele:array[0..16] of byte = (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    PaRef:array[0..16,0..12] of byte =
    (
    (0,0,0,0,0,0,0,0,0,0,0,0,0),
    (0,1,2,3,4,5,6,7,8,127,143,175,0),
    (0,9,10,11,12,13,14,15,16,128,144,176,0),
    (0,49,50,51,52,53,54,55,56,133,149,0,0),  //fix me last pa
    (0,57,58,59,60,61,62,63,64,134,150,0,0),  //fix me last pa
    (0,65,66,67,68,69,70,71,72,135,151,0,0),  //fix me last pa
    (0,0,0,0,0,0,0,0,0,0,0,0,0),   //rod
    (0,0,0,0,0,0,0,0,0,0,0,0,0),   //tlis
    (0,119,120,121,122,123,124,125,126,155,156,0,0),  //fix me last pa
    (0,111,112,113,114,115,116,117,118,153,154,181,0),
    (0,163,164,165,0,0,0,0,0,0,0,0,0),   //fix me last pa
    (0,166,167,168,169,170,171,0,0,0,0,0,0), //fix me last pa
    (0,73,74,75,76,77,78,79,80,136,152,177,0),
    (0,1,2,3,0,0,0,0,0,0,0,0,0),
    (0,17,18,19,20,21,22,23,24,129,145,0,0), //fix me last pa
    (0,25,26,27,28,29,30,31,32,130,146,0,0),  //fix me last pa
    (0,33,34,35,36,37,38,39,40,131,147,0,0)  //fix me last pa
    );
begin
    result.name:='generic';
    result.action:=0; //none
    result.types:=0;
    result.element:=0;
    result.mul:=1;
    result.PA:=0;

    x:=pos(':',s);
    if x>0 then begin
        //potentialy an attack
        a:=copy(s,1,x-1);
        delete(s,1,x);
        i:=0;
        if a = 'sword' then i:=1;
        if a = 'wlance' then i:=2;
        if a = 'gunslash' then i:=3;
        if a = 'rifle' then i:=4;
        if a = 'launcher' then i:=5;
        if a = 'rod' then i:=6;
        if a = 'talis' then i:=7;
        if a = 'compoundbow' then i:=8;
        if a = 'katana' then i:=9;
        if a = 'jetboots' then i:=10;
        if a = 'dblade' then i:=11;
        if a = 'tmachinegun' then i:=12;

        if a = 'takt' then i:=13; //pet

        if a = 'partisan' then i:=14;
        if a = 'tdagger' then i:=15;
        if a = 'dsaber' then i:=16;

        if i > 0 then result.action:=ACTION_ATTACK;
        result.types:=Atype[i];
        result.element:=aele[i];

        //test for tech

        x:=pos('pa',s);
        if x > 0 then begin
            //got a pa
            delete(s,1,x+1);
            b:='0';
            if length(s) > 0 then begin
                b:=s[1];
                if length(s) > 1 then
                if (byte(s[2])>=$30) and (byte(s[2])<$39) then
                    b:=b+s[2];
                x:=strtointdef(b,0);
                result.PA:=paref[i,x];
            end;
        end;

        if pos('guard',s) > 0 then begin
            //guard
            result.action:=ACTION_DEF;
            result.mul:=1.5;
            result.PA:=0;
        end;
    end;
end;

procedure GetMonsterAttention(x:integer;data:ansistring);
var id,i:integer;
begin
    //if the monster is a boss retrigger it so the boss doesnt stay stuck after animation
    move(data[$15],id,4);
    for i:=0 to team[player[x].TeamID].MonstCount[player[x].Floor]-1 do
    if team[player[x].TeamID].Monst[player[x].Floor,i].id = id then begin
        if team[player[x].TeamID].Monst[player[x].Floor,i].boss = 1 then
        if (team[player[x].TeamID].Monst[player[x].Floor,i].status <> 0) and (team[player[x].TeamID].Monst[player[x].Floor,i].status<>10) then begin
          team[player[x].TeamID].Monst[player[x].Floor,i].status:=MONSTER_MOVE;
          team[player[x].TeamID].Monst[player[x].Floor,i].target:=0;
          UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_MOVE);
        end;
    end;

end;

procedure Motion0804(x:integer;data:ansistring);
var a:ansistring;
    i,l:integer;
    id:dword;
    ac:tingameaction;
begin
    data[$11]:=#4;
    data[$6]:=#$80;
    //convert count
    i:=ReadMagic(copy(data,$49,4), $922D, $45);
    a:=GetMagic(i,$4315, $7A);
    move(a[1],data[$49],4);

    i:=(i+3) and $FC; //do padding
    i:=i+8;

    l:=ReadMagic(copy(data,$49+i,4), $922D, $45);
    a:=GetMagic(l,$4315, $7A);
    move(a[1],data[$49+i],4);

    sendtolobby(player[x].room,x,data);
    {if form1.memo5.lines.IndexOf(pansichar(@data[$4d])) = -1 then  //only unlisted
        form1.Memo5.Lines.add(pansichar(@data[$4d]));      }
    //store known action for the next attack/in game action
    //the player control many enemy + pet so make sure the atk is for him
    move(data[$15],id,4);
    if id = player[x].GID then begin
        for i:=0 to ActionCount-1 do begin
            if testaction(lowercase(ActionType[i].name), lowercase(pansichar(@data[$4d]))) then begin
                if data[$19] = #0 then player[x].lastaction:=ActionType[i];
                if data[$19] = #1 then player[x].petaction:=ActionType[i];
                break;
            end;
        end;
        if i = actioncount then begin
            //log
            //generic acitions
            ac:=GenerateGenericAction(lowercase(pansichar(@data[$4d])));
            if ac.action<>0 then begin
            if data[$19] = #0 then player[x].lastaction:=ac;
            if data[$19] = #1 then player[x].petaction:=ac;
            end;

            if UnknownAction.IndexOf(pansichar(@data[$4d])) = -1 then begin
                UnknownAction.add(pansichar(@data[$4d]));
                //debug(DBG_ERROR,'Unknown action : '+pansichar(@data[$4d]));
                UnknownAction.SaveToFile(path+'debug\actions.txt');
            end;
        end;
    end;
end;

procedure Motion3c04(x:integer;data:ansistring);
var a:ansistring;
    i,l:integer;
    ping:int64;
begin
    data[$11]:=#4;
    data[$6]:=#$81;
    ping:=player[x].ping+(gettickcount()-player[x].pingticks);
    move(data[$15],l,4);
    if player[x].GID = l then begin
        move(data[$25],player[x].Rotation,8);
        move(data[$2d],player[x].CurPos,8);
        move(data[$35],ping,8);
        player[x].lastarea1:=copy(data,$3d,4);
        player[x].lastarea2:=copy(data,$21,4);
    end;
    move(ping,data[$35],8);
    sendlobbychar2(player[x].room,x,data);

    if player[x].GID = l then makeplayermove(x);
end;

procedure PlayerIsNotBusy(x:integer);
var s:ansistring;
begin
    s:=#$18#$00#$00#$00#$0E#$2B#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].GID,s[9],4);
    sendtoparty(x,s);
end;

procedure PlayerIsBusy(x:integer);
var s:ansistring;
begin
    s:=#$18#$00#$00#$00#$0E#$2B#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$01#$00#$00#$00;
    move(player[x].GID,s[9],4);
    sendtoparty(x,s);
end;

procedure PlayerMenuStatus(x:integer;s:ansistring);
begin
    move(player[x].GID,s[9],4);
    SendToLobby(player[x].room,-1,s);
end;

procedure SpawnPlayer(x:integer;pp:integer=0);
var s,a:ansistring;
    i,l:integer;
begin
    //depending of the room move the player around
    l:=2;
    if player[x].room>50 then l:=1;
    player[x].inboss:=0;
    i:=random(l*2)-l;
    l:=random(l*2)-l;
    if pp = 0 then begin
    player[x].CurPos.x:=FloatToHalf(HalfPrecisionToFloat(player[x].CurPos.x)+i);
    player[x].CurPos.z:=FloatToHalf(HalfPrecisionToFloat(player[x].CurPos.z)+l);
    end;


    player[x].last81:='';
    s:=#$50#$03#$00#$00#$08#$04#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$3C#$08#$39#$00#$00
	+#$F5#$B3#$00#$00#$43#$68#$61#$72#$61#$63#$74#$65#$72#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$01#$00#$00#$00#$D8#$00#$00#$00#$01#$00#$00#$00
	+#$02#$00#$00#$00#$00#$00#$00#$00#$2F#$00#$00#$00#$64#$8F#$D3#$01
	+#$56#$5F#$9C#$00#$81#$B1#$8E#$57#$03#$00#$B8#$0B#$00#$00#$00#$00
	+#$50#$00#$61#$00#$74#$00#$61#$00#$74#$00#$65#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$67#$01
	+#$78#$00#$99#$FE#$10#$FF#$99#$FE#$8C#$FB#$5D#$FE#$22#$06#$00#$00
	+#$00#$00#$00#$00#$C0#$F9#$96#$F5#$D6#$FB#$80#$0C#$C1#$F9#$56#$EF
	+#$D6#$FB#$EA#$16#$F0#$D8#$D6#$FB#$1D#$00#$DA#$00#$00#$00#$3C#$00
	+#$3C#$00#$5E#$02#$AA#$10#$2B#$EB#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$67#$01#$78#$00
	+#$99#$FE#$10#$FF#$99#$FE#$8C#$FB#$5D#$FE#$22#$06#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$30#$92#$22#$AB#$D0#$07#$30#$92#$22#$AB#$D0#$07
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$AF#$60
	+#$10#$27#$00#$00#$00#$00#$00#$00#$26#$2E#$B0#$10#$10#$27#$7E#$96
	+#$D6#$B1#$E8#$03#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$09#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$05#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$21#$4E#$21#$4E#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$0E#$07#$02#$01#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$00#$00#$FF#$02#$00#$00
	+#$02#$00#$02#$00#$CF#$00#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$01#$00#$01#$00#$00#$00#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$01#$00#$01#$00#$00#$00#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$01#$00#$01#$00#$00#$00#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$01#$00#$01#$00#$00#$00#$00#$00#$01#$00#$01#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$1E#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00 //00 01 = in team
	+#$50#$00#$0#$00#$0#$00#$0#$00#$0#$00#$0#$00#$0#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].hp,s[$49],4); //player life
    move(player[x].gid,s[9],4);
    move(player[x].gid,s[$61],4);
    move(player[x].activechar.charid,s[$5d],4);
    move(player[x].activeChar.base.name[0],s[$71],616-12);
    move(player[x].CurPos,s[$1d],8);
    move(player[x].Rotation,s[$15],8);

    s[$2ce]:=#0;
    s[$2cd]:=#0;
    if partycount(player[x].partyid) > 1 then s[$2ce]:=#1;
    if player[x].isgm = 1 then s[$2cd]:=#1;

    for i:=0 to length(player[x].aname)-1 do
        s[$2d1+(i*2)]:=player[x].aname[i+1];
    sendtoplayer(x,s);

    s[$59]:=#$27; //remove control
    if (player[x].room < Room_Team) and (player[x].room <> Room_Dressing) then SendToLobby(player[x].room,x,s)
    else for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].room = player[x].room) and (player[i].Floor = player[x].Floor) and (i <> x) then
            sendtoplayer(i,s);
    //if (player[x].room <> Room_Dressing) then SendToLobby(player[x].room,x,s);

    //send status
    a:=#$14#$00#$00#$00#$2C#$01#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#0#0#0#0;
    move(player[x].status,a[17],4);
    move(player[x].gid,a[9],4);
    sendtolobby(player[x].room,-1,a);

    //send all other player to him
    if (player[x].room < room_casinogame) and (player[x].room > room_casinogame+7 ) then
            player[x].casinogame:=0; //not in game
    if (player[x].room <> Room_Dressing) then
    for l:=0 to 199 do
        if (playerok(l)) and (player[l].room = player[x].room) and (l<>x) and((player[l].Floor = player[x].Floor) or (player[x].room < Room_Team)) then begin
            move(player[l].hp,s[$49],4); //player life
            move(player[l].gid,s[9],4);
            move(player[l].gid,s[$61],4);
            move(player[l].activechar.charid,s[$5d],4);
            move(player[l].activeChar.base.name[0],s[$71],616-12);
            move(player[l].CurPos,s[$1d],8);
            move(player[l].Rotation,s[$15],8);

            s[$2ce]:=#0;
            if partycount(player[l].partyid) > 1 then s[$2ce]:=#1;
            s[$2cd]:=#0;
            if player[l].isgm = 1 then s[$2cd]:=#1;

            for i:=0 to length(player[l].aname)-1 do
                s[$2d1+(i*2)]:=player[l].aname[i+1];
            sendtoplayer(x,s);

            SendPlayerBonusToMe(x,l);

            //send the player status
            a:=#$14#$00#$00#$00#$2C#$01#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#0#0#0#0;
            move(player[l].status,a[17],4);
            move(player[l].gid,a[9],4);
            sendtoplayer(x,a);

        end;


    //sendArmor(x);
    SendPA(x);

    //send skill list
    SendSkillList(x);

    sendmyskill(x);

    //send pet if needed

end;


procedure makeplayermove(x:integer);
begin
    player[x].movetimer:=300;
end;

procedure makeplayermove2(x:integer);
var l:integer;
    a:ansistring;
    ping:int64;
begin
    if player[x].last81 <> '' then begin
    a:=#$48#$00#$00#$00#$04#$07#$70#$00#$9B#$6E#$02#$00#$00#$70#$dc#$a6
    +#$9b#$00#$00#$00#$00#$00#$04#$00#$00#$00#$DC#$A6#$9B#$00#$00#$00
    +#$00#$00#$04#$00#$00#$00#$F1#$D2#$BD#$3F#$00#$00#$00#$3C#$00#$00
    +#$00#$00#$1B#$B8#$00#$00#$15#$42#$00#$00#$1B#$B8#$99#$3C#$F5#$C6
    +#$00#$00#$FE#$FF#$FF#$FF#$00#$00;
    move(player[x].last81[1],a[9],6);
    move(player[x].gid,a[$f],4);
    move(player[x].gid,a[$1b],4);
    move(player[x].CurPos,a[$33],8);
    move(player[x].Rotation,a[$2b],8);
    move(player[x].lastarea1[1],a[$43],4);
    ping:=player[x].ping+(gettickcount-player[x].pingticks);
    move(ping,a[$3b],8);
    if player[x].lastarea2 <> '' then move(player[x].lastarea2[1],a[$27],4);
    sendtolobby(player[x].room,x,a);
    end;
    //send proper data
    for l:=0 to srvcfg.MaxPlayer-1 do
        if (playerok(l)) and (player[l].room = player[x].room) and (l<>x) then begin
            if player[l].last81 <> '' then begin
                a:=#$48#$00#$00#$00#$04#$07#$70#$00#$9B#$6E#$02#$00#$00#$70#$dc#$a6
                +#$9b#$00#$00#$00#$00#$00#$04#$00#$00#$00#$DC#$A6#$9B#$00#$00#$00
                +#$00#$00#$04#$00#$00#$00#$F1#$D2#$BD#$3F#$00#$00#$00#$3C#$00#$00
                +#$00#$00#$1B#$B8#$00#$00#$15#$42#$00#$00#$1B#$B8#$99#$3C#$F5#$C6
                +#$00#$00#$FE#$FF#$FF#$FF#$00#$00;
                move(player[l].last81[1],a[9],6);
                move(player[l].gid,a[$1b],4);
                move(player[l].gid,a[$f],4);
                move(player[l].CurPos,a[$33],8);
                move(player[l].Rotation,a[$2b],8);
                move(player[l].lastarea1[1],a[$43],4);
                if player[x].lastarea2 <> '' then move(player[x].lastarea2[1],a[$27],4);
                ping:=player[x].ping+(gettickcount-player[x].pingticks);
                move(ping,a[$3b],8);
                sendtoplayer(x,a);

                //send equipement and outfit
                sendtoplayer(x,GetPlayerEquiped(l));
            end;
        end;
end;


function GetPlayerByID(id:dword):integer;
var i:integer;
begin
    result:=-1;
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID = id then result:=i;
end;


procedure KickSomething(x:integer;s:ansistring);
var a,b:ansistring;
    p:^tInventoryItem;
    it:dword;
    w:^word;
    f:single;
    id:^int64;
    mid,dmg,pl,at:integer;

    fid,tid,ftype,ttype:dword;
    i,c:integer;
    atk:TAttackData;
    itm:ansistring;
    ia:tingameaction;
    te:TTechEffect;
begin
    {000000: 50 00 00 00 06 01 00 00 56 5F 9C 00 00 00 00 00     P.......V_�.....
    000010: 04 00 00 00 B2 00 00 00 00 00 00 00 06 00 01 06     ....�...........
    000020: F4 94 47 BC 44 4C 15 BC BB 54 00 00 01 00 00 00     ��G�DL.��T......
    000030: 5D 4C E6 B7 B7 54 00 00 71 BA 3C A9 BB 38 00 00     ]L混T..q�<��8..
    000040: 08 00 04 20 00 00 80 00 00 00 00 00 00 00 00 00     ... ..�.........
    }
    {a:=DataToHex(s);
    form1.Memo1.Lines.Add(a);
    form1.Memo1.Lines.Add('');   }

    {000000: 50 00 00 00 04 52 40 00 56 5F 9C 00 00 00 00 00     P....R@.V_�..... //reply for monster
000010: 04 00 00 00 CC 00 00 00 00 00 00 00 06 00 A9 11     ....�.........�.
000020: 56 5F 9C 00 00 00 00 00 04 00 00 00 E0 09 69 63     V_�.........�.ic
000030: 4A 00 00 00 D3 00 00 00 02 00 00 00 B3 D6 F3 36     J...�.......���6    //damage, life, critical (1 = normal, 2 = critical)
000040: 5F 41 00 00 08 00 04 00 80 00 A0 08 00 00 00 00     _A......�.�..... }

    //who does the action
    move(s[9],fid,4);
    ftype:=0; //monster
    if s[$11] = #4 then begin
        ftype:=1; //player

    end else begin
        if fid = player[x].GID then
        if s[$d] = #1 then ftype:=2; //pet
        if s[$11] = #$16 then ftype:=3; //npc
    end;

    //who is the target
    move(s[$15],tid,4);
    ttype:=0; //monster
    if s[$1d] = #4 then begin
        ttype:=1; //player
    end else begin
        if s[$19] = #1 then ttype:=2; //pet
        if s[$1d] = #$16 then ttype:=3; //npc
    end;
    // test if its just an object
    i:=1;
    if ttype = 0 then begin
        mid := IDIsAMonster(player[x].TeamID,tid,player[x].Floor);
        if mid = -1 then i:=0;
    end;

    if i = 0 then begin
        //this is an object, destroy it
        //this is an item so
            id:=@s[$15];
            //destroy it
            a:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
            +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00
            +#$03#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00;
            move(player[x].gid,a[9],4);
            move(player[x].gid,a[$21],4);
            move(s[$15],a[$15],12);
            sendtofloor(player[x].room,player[x].Floor,a);

            a[6]:=#$f;
            sendtofloor(player[x].room,player[x].Floor,a);

            a:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
            +#$02#$00#$00#$00;
            move(player[x].gid,a[9],4);
            move(s[$15],a[$15],12);
            move(s[$15],it,4);

            debug(1,inttohex(it,8));
            b:=a;

            //test for action

            a:=RemoveObjFromFloor(x,player[x].TeamID,player[x].floor,id^);
            if a = 'oa_mine_capture' then b[$21]:=#5;  //mabe 4 if 3 doesnt work

            if a <> '-' then sendtofloor(player[x].room,player[x].Floor,b);


            if a = 'ob_9900_0361' then begin
                a:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
                +#$03#$00#$00#$00;
                move(player[x].gid,a[9],4);
                move(s[$15],a[$15],12);
                move(s[$15],it,4);
                sendtofloor(player[x].room,player[x].Floor,a);

                for c:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(c) and (player[c].room = player[x].room) and (player[c].Floor = player[x].Floor) then begin
                        GenerateDrop(c,-1,HalfPrecisionToFloat(pword(@s[$31])^)-1
                            ,HalfPrecisionToFloat(pword(@s[$33])^),HalfPrecisionToFloat(pword(@s[$35])^));
                        GenerateDrop(c,-1,HalfPrecisionToFloat(pword(@s[$31])^)
                            ,HalfPrecisionToFloat(pword(@s[$33])^),HalfPrecisionToFloat(pword(@s[$35])^));
                        GenerateDrop(c,-1,HalfPrecisionToFloat(pword(@s[$31])^)+1
                            ,HalfPrecisionToFloat(pword(@s[$33])^),HalfPrecisionToFloat(pword(@s[$35])^));
                        GenerateDrop(c,-1,HalfPrecisionToFloat(pword(@s[$31])^)
                            ,HalfPrecisionToFloat(pword(@s[$33])^),HalfPrecisionToFloat(pword(@s[$35])^)-1);
                        GenerateDrop(c,-1,HalfPrecisionToFloat(pword(@s[$31])^)
                            ,HalfPrecisionToFloat(pword(@s[$33])^),HalfPrecisionToFloat(pword(@s[$35])^)+1);

                    end;
            end;

            if (a='ob_9900_0308') or (a='ob_9900_0309') or (a='ob_9900_0310') or (a='ob_9900_0311') then begin
                for c:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(c) and (player[c].room = player[x].room) and (player[c].Floor = player[x].Floor) then
                        GenerateDrop(c,-1,HalfPrecisionToFloat(pword(@s[$31])^)
                            ,HalfPrecisionToFloat(pword(@s[$33])^),HalfPrecisionToFloat(pword(@s[$35])^));
                
            end;

    end else begin
        //process the attack
        f:=1; //default multiplier
        //find the attack type
        at:=0; //physical
        atk.dmg:=0;
        ia:=GetActionHandler(pdword(@s[$21])^);
        if ia.action = -1 then exit; //do nothing if unknown
        atk.action:=ia.action;
        atk.use:=ia.name;
        if ftype = 0 then atk.src:=GetMonsterAtkInfo(x,fid,ia);
        if ftype = 1 then begin  //player
            mid := GetPlayerByID(fid);
            atk.src:=GetPlayerAtkInfo(mid,ia);
        end;
        if ftype = 2 then atk.src:=GetPetAtkInfo(x,ia);
        if ftype = 3 then atk.src:=GetNpcAtkInfo(x,fid,ia);


        //target
        if ttype = 0 then atk.dst:=GetMonsterDefInfo(x,tid,atk.src.atype,pdword(@s[$2d])^);
        if ttype = 1 then begin  //player
            pl := GetPlayerByID(tid);
            atk.dst:=GetPlayerDefInfo(pl,atk.src.atype);
        end;
        if ttype = 2 then begin
            pl := GetPlayerByID(tid);
            atk.dst:=GetPetDefInfo(x,atk.src.atype);
        end;
        if ttype = 3 then atk.dst:=GetNpcDefInfo(x,tid,atk.src.atype,pdword(@s[$2d])^);

        if atk.action = ACTION_USE then begin
            //use items
            PlayerUseItem(x,atk.use);

        end else if atk.action = ACTION_BOOST then begin
            if ttype = 0 then ApplyBoostToMoonster(x,tid,atk.use,s);
            if ttype = 1 then ApplyBoostToPlayer(pl,atk.use,s);
            if ttype = 3 then ApplyBoostToNPC(x,fid,atk.use,s);
            
        end else begin
            //process the attack
            //do some bonus thing
            if byte(s[$41]) and 2 = 2 then atk.dst.mul:=atk.dst.mul * 1.4;
            if byte(s[$42]) and 1 = 1 then atk.src.mul:=atk.src.mul * 1.25;
            if byte(s[$42]) and 6 = 6 then atk.src.mul:=atk.src.mul * 1.25;
            if byte(s[$42]) and 8 = 8 then atk.src.mul:=atk.src.mul * 1.5;
            if byte(s[$42]) and $10 = $10 then atk.src.mul:=atk.src.mul * 1.2
            else atk.src.mul:=atk.src.mul * 0.9;
            if byte(s[$44]) and $10 = $10 then atk.dst.mul:=atk.dst.mul * 0.8;
            if byte(s[$44]) and $20 = $20 then atk.dst.mul:=atk.dst.mul * 1.1
            else atk.dst.mul:=atk.dst.mul * 0.9;
            c:=(byte(s[$44]) and 6) div 2;
            if c = 1 then atk.src.mul:=atk.src.mul * 1.1;
            if c = 2 then atk.src.mul:=atk.src.mul * 1.2;
            if c = 3 then atk.src.mul:=atk.src.mul * 1.3;

            if atk.dst.dex = 0 then atk.dst.dex:=1;
            if atk.src.dex = 0 then atk.src.dex:=1;
            at:=round(atk.src.base / (atk.dst.dex / atk.src.dex));
            at:=(at-atk.src.base);
            if at < 0 then at := at div 4;
            atk.dmg:=round(((atk.src.atk * atk.src.mul) - (atk.dst.def   * atk.dst.mul)) + at);
            if atk.dmg < 0 then atk.dmg:=0;
            
            at:=atk.dmg div 7;
            atk.dmg:=atk.dmg div 5;

            atk.dmg:=random(atk.dmg - at)+at;

            if atk.dmg < 0 then atk.dmg:=0;
            if atk.dmg > 1000 then
                   atk.dmg:=999;      //find why i get 99999999

            if ttype = 0 then begin
                //debug(2,'Part ID '+inttohex(pdword(@s[$2d])^,8));
                mid := IDIsAMonster(player[x].TeamID,tid,player[x].Floor);
                i:=0;
                if fid = player[x].GID then i:=1;
                if copy(s,$31,8) = #0#0#0#0#0#0#0#0 then debug(2,'Bad Attack ID '+inttohex(pdword(@s[$21])^,8));
                ApplyDamageToMonster(x,player[x].TeamID,mid,player[x].Floor,atk.dmg,i,copy(s,$31,8));
                if ia.PA > 0 then begin
                    te:=GetTechStatus(-1,ia.PA);
                    if (ia.element<>0) and (te.chance = 0) then begin
                      te.chance:=20; //minimal chance of effect
                      te.time:=10;
                      te.lvl:=1;
                    end;
                    if random(100) < te.chance then begin
                        //apply effects
                        ApplyEffectToMonster(player[x].TeamID,player[x].floor,mid,te,ia.element);
                    end;
                end;
                
            end;
            if ttype = 1 then begin
                ApplyDamageToPlayer(pl,player[x].TeamID,player[x].Floor,atk.dmg,copy(s,$9,12),copy(s,$31,8));
                te:=GetTechStatus(-1,ia.PA);
                if (ia.element<>0) and (te.chance = 0) then begin
                  te.chance:=20; //minimal chance of effect
                  te.time:=10;
                  te.lvl:=1;
                end;
                if random(100) < te.chance then begin
                    ApplyEffectToPlayer(pl,te,ia.element);
                end;
            end;
            if ttype = 2 then ApplyDamageToPet(pl,player[x].TeamID,player[x].Floor,atk.dmg,copy(s,$9,12),copy(s,$31,8));
            if ttype = 3 then begin
                //debug(2,'Part ID '+inttohex(pdword(@s[$2d])^,8));
                if fid = tid then debug(2,'Previous action is to self '+inttohex(pdword(@s[$21])^,8));
                ApplyDamageToNPC(x,player[x].TeamID,fid,atk.dmg,copy(s,$31,8));
            end;
        end;

    end;


   { if s[$1d] = #4 then begin //target is a player
        id:=@s[$9];
        mid := IDIsAMonster(player[x].TeamID,id^,player[x].Floor);
        if mid > -1 then begin
            //this is a monster send the damage
            dmg:=GetDamageFromMonster(x,id^,0); //fixed just for the test
            if byte(s[$46]) and 8 = 8 then dmg:=dmg*2; //boosted attack 

            ApplyDamageToPlayer(x,player[x].TeamID,mid,player[x].Floor,dmg,copy(s,$31,8));

        end else begin
            //not sure yet, what can attack the player other than a monster?

        end;

    end else begin
        id:=@s[$15];
        mid := IDIsAMonster(player[x].TeamID,id^,player[x].Floor);
        if mid > -1 then begin
            //this is a monster send the damage
            if byte(s[$43]) and 4 = 0 then dmg:=GetDamageToMonster(x,id^,2) //apt damage
            else begin
                if byte(s[$42]) and $10 = 0 then dmg:=GetDamageToMonster(x,id^,0)//physical
                else dmg:=GetDamageToMonster(x,id^,1); //range
            end;
            if byte(s[$46]) and 8 = 8 then dmg:=dmg*2;

            ApplyDamageToMonster(x,player[x].TeamID,mid,player[x].Floor,dmg,copy(s,$31,8));

        end else begin

         end;
     end;     }
end;

end.
