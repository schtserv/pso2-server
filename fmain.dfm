object Form1: TForm1
  Left = 229
  Top = 136
  Caption = 'SchtServ PSO2 Server 0.3'
  ClientHeight = 448
  ClientWidth = 652
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 652
    Height = 448
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'General'
      object GroupBox1: TGroupBox
        Left = 2
        Top = 4
        Width = 323
        Height = 272
        Caption = 'Ship information : '
        TabOrder = 0
        object Label1: TLabel
          Left = 14
          Top = 26
          Width = 58
          Height = 13
          Caption = 'Max Player :'
        end
        object Label2: TLabel
          Left = 14
          Top = 73
          Width = 37
          Height = 13
          Caption = 'Ship IP:'
        end
        object Label3: TLabel
          Left = 172
          Top = 26
          Width = 48
          Height = 13
          Caption = 'Ship port :'
        end
        object Label10: TLabel
          Left = 242
          Top = 26
          Width = 54
          Height = 13
          Caption = 'Block port :'
        end
        object Label14: TLabel
          Left = 14
          Top = 121
          Width = 62
          Height = 13
          Caption = 'Lobby event:'
        end
        object Label20: TLabel
          Left = 172
          Top = 121
          Width = 55
          Height = 13
          Caption = 'Cafe event:'
        end
        object SpinEdit1: TSpinEdit
          Left = 14
          Top = 44
          Width = 133
          Height = 22
          MaxValue = 200
          MinValue = 0
          TabOrder = 0
          Value = 100
        end
        object Edit1: TEdit
          Left = 14
          Top = 90
          Width = 132
          Height = 21
          TabOrder = 1
          Text = '127.0.0.1'
        end
        object SpinEdit2: TSpinEdit
          Left = 172
          Top = 44
          Width = 63
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 2
          Value = 12900
        end
        object Button1: TButton
          Left = 12
          Top = 236
          Width = 94
          Height = 25
          Caption = 'Save setting'
          TabOrder = 3
          OnClick = Button1Click
        end
        object SpinEdit3: TSpinEdit
          Left = 242
          Top = 44
          Width = 63
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 4
          Value = 12901
        end
        object ComboBox4: TComboBox
          Left = 14
          Top = 140
          Width = 145
          Height = 19
          Style = csOwnerDrawFixed
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 5
          Text = 'Aqua party'
          OnChange = ComboBox4Change
          Items.Strings = (
            'Aqua party'
            'GodziLee'
            'Calinourse'
            'Halloween'
            'November'
            'Christmas'
            'New years'
            'Valentine'
            'Spring'
            'Easter'
            'Weding'
            'June2017')
        end
        object CheckBox1: TCheckBox
          Left = 14
          Top = 176
          Width = 97
          Height = 17
          Caption = 'Red Alert'
          TabOrder = 6
          OnClick = CheckBox1Click
        end
        object ComboBox5: TComboBox
          Left = 172
          Top = 140
          Width = 145
          Height = 19
          Style = csOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 7
          OnChange = ComboBox4Change
          Items.Strings = (
            'Day City'
            'Fireworks City'
            'Spring garden'
            'Winter')
        end
      end
      object GroupBox2: TGroupBox
        Left = 334
        Top = 4
        Width = 229
        Height = 272
        Caption = 'Status : '
        TabOrder = 1
        object Label4: TLabel
          Left = 12
          Top = 48
          Width = 90
          Height = 13
          Caption = 'Current user count:'
        end
        object Label5: TLabel
          Left = 12
          Top = 70
          Width = 76
          Height = 13
          Caption = 'Max user count:'
        end
        object Label6: TLabel
          Left = 12
          Top = 26
          Width = 36
          Height = 13
          Caption = 'Status: '
        end
        object Label7: TLabel
          Left = 56
          Top = 26
          Width = 38
          Height = 13
          Caption = 'Inactive'
        end
        object Label8: TLabel
          Left = 116
          Top = 48
          Width = 6
          Height = 13
          Caption = '0'
        end
        object Label9: TLabel
          Left = 104
          Top = 70
          Width = 6
          Height = 13
          Caption = '0'
        end
        object Label21: TLabel
          Left = 12
          Top = 92
          Width = 67
          Height = 13
          Caption = 'Game count : '
        end
        object Label22: TLabel
          Left = 104
          Top = 92
          Width = 6
          Height = 13
          Caption = '0'
        end
        object Button2: TButton
          Left = 12
          Top = 117
          Width = 103
          Height = 26
          Caption = 'Start server'
          TabOrder = 0
          OnClick = Button2Click
        end
        object Button3: TButton
          Left = 119
          Top = 117
          Width = 100
          Height = 26
          Caption = 'Stop Server'
          TabOrder = 1
          OnClick = Button3Click
        end
        object Button30: TButton
          Left = 14
          Top = 154
          Width = 75
          Height = 25
          Caption = 'Start Show'
          TabOrder = 2
          OnClick = Button30Click
        end
      end
      object GroupBox3: TGroupBox
        Left = 4
        Top = 284
        Width = 559
        Height = 87
        Caption = 'Mass message : '
        TabOrder = 2
        object Label12: TLabel
          Left = 266
          Top = 58
          Width = 30
          Height = 13
          Caption = 'Type :'
        end
        object Edit2: TEdit
          Left = 14
          Top = 22
          Width = 531
          Height = 21
          TabOrder = 0
        end
        object Button13: TButton
          Left = 470
          Top = 54
          Width = 75
          Height = 25
          Caption = 'Send'
          TabOrder = 1
          OnClick = Button13Click
        end
        object ComboBox2: TComboBox
          Left = 319
          Top = 55
          Width = 145
          Height = 22
          Style = csOwnerDrawFixed
          ItemIndex = 0
          TabOrder = 2
          Text = 'GoldenTicker '
          Items.Strings = (
            'GoldenTicker '
            'AdminMessage'
            'AdminMessageInstant'
            'SystemMessage'
            'EventInformationYellow'
            'EventInformationGreen'
            'PopupMessage')
        end
      end
      object Button35: TButton
        Left = 350
        Top = 386
        Width = 75
        Height = 25
        Caption = 'Button35'
        TabOrder = 3
        OnClick = Button35Click
      end
      object Button47: TButton
        Left = 480
        Top = 384
        Width = 75
        Height = 25
        Caption = 'Button47'
        TabOrder = 4
        OnClick = Button47Click
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Options'
      ImageIndex = 5
      object Label17: TLabel
        Left = 302
        Top = 16
        Width = 159
        Height = 13
        Caption = 'EXP and Mesetas Boost for tester'
      end
      object CheckBox2: TCheckBox
        Left = 10
        Top = 10
        Width = 97
        Height = 17
        Caption = 'Announcement'
        TabOrder = 0
        OnClick = CheckBox2Click
      end
      object Memo4: TMemo
        Left = 8
        Top = 32
        Width = 279
        Height = 335
        TabOrder = 1
      end
      object Button25: TButton
        Left = 208
        Top = 376
        Width = 75
        Height = 25
        Caption = 'Save'
        TabOrder = 2
        OnClick = Button25Click
      end
      object Memo8: TMemo
        Left = 300
        Top = 34
        Width = 181
        Height = 161
        Lines.Strings = (
          'Memo8')
        TabOrder = 3
      end
      object Button37: TButton
        Left = 406
        Top = 202
        Width = 75
        Height = 25
        Caption = 'Save'
        TabOrder = 4
        OnClick = Button37Click
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'GM'
      ImageIndex = 6
      object GroupBox7: TGroupBox
        Left = 10
        Top = 10
        Width = 241
        Height = 259
        Caption = 'GM List : '
        TabOrder = 0
        object Label16: TLabel
          Left = 4
          Top = 196
          Width = 46
          Height = 13
          Caption = 'Password'
        end
        object Memo6: TMemo
          Left = 2
          Top = 15
          Width = 237
          Height = 172
          Align = alTop
          Lines.Strings = (
            'Memo6')
          ScrollBars = ssBoth
          TabOrder = 0
        end
        object Edit5: TEdit
          Left = 6
          Top = 218
          Width = 229
          Height = 21
          TabOrder = 1
          Text = 'cookie123'
        end
      end
      object Button32: TButton
        Left = 10
        Top = 276
        Width = 75
        Height = 25
        Caption = 'Save'
        TabOrder = 1
        OnClick = Button32Click
      end
      object GroupBox8: TGroupBox
        Left = 262
        Top = 10
        Width = 273
        Height = 259
        Caption = 'Ban list : '
        TabOrder = 2
        object Memo7: TMemo
          Left = 6
          Top = 20
          Width = 251
          Height = 225
          Lines.Strings = (
            'Memo7')
          TabOrder = 0
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Log test'
      ImageIndex = 2
      object Label11: TLabel
        Left = 2
        Top = 284
        Width = 44
        Height = 13
        Caption = 'Comment'
      end
      object ListBox1: TListBox
        Left = 0
        Top = 0
        Width = 357
        Height = 277
        Align = alCustom
        ItemHeight = 13
        TabOrder = 0
        OnClick = ListBox1Click
      end
      object Button5: TButton
        Left = 364
        Top = 2
        Width = 75
        Height = 25
        Caption = 'Load'
        TabOrder = 1
        OnClick = Button5Click
      end
      object Memo2: TMemo
        Left = 2
        Top = 302
        Width = 647
        Height = 125
        Lines.Strings = (
          'Memo2')
        TabOrder = 2
      end
      object Button6: TButton
        Left = 574
        Top = 284
        Width = 75
        Height = 17
        Caption = 'Save'
        TabOrder = 3
        OnClick = Button6Click
      end
      object Button7: TButton
        Left = 366
        Top = 68
        Width = 75
        Height = 25
        Caption = 'Send'
        TabOrder = 4
        OnClick = Button7Click
      end
      object Button8: TButton
        Left = 450
        Top = 98
        Width = 75
        Height = 25
        Caption = 'Save packet'
        TabOrder = 5
        OnClick = Button8Click
      end
      object Button9: TButton
        Left = 366
        Top = 98
        Width = 75
        Height = 25
        Caption = 'Save Obj'
        TabOrder = 6
        OnClick = Button9Click
      end
      object Button10: TButton
        Left = 450
        Top = 68
        Width = 75
        Height = 25
        Caption = 'Send Next'
        TabOrder = 7
        OnClick = Button10Click
      end
      object Button11: TButton
        Left = 366
        Top = 128
        Width = 75
        Height = 25
        Caption = 'Save NPC'
        TabOrder = 8
        OnClick = Button11Click
      end
      object ComboBox1: TComboBox
        Left = 462
        Top = 4
        Width = 145
        Height = 21
        TabOrder = 9
        Text = 'ComboBox1'
      end
      object Button12: TButton
        Left = 532
        Top = 98
        Width = 75
        Height = 25
        Caption = 'Copy Delphi'
        TabOrder = 10
        OnClick = Button12Click
      end
      object Button14: TButton
        Left = 532
        Top = 126
        Width = 75
        Height = 25
        Caption = 'Copy Hex'
        TabOrder = 11
        OnClick = Button14Click
      end
      object Button16: TButton
        Left = 494
        Top = 32
        Width = 75
        Height = 25
        Caption = 'Search'
        TabOrder = 12
        OnClick = Button16Click
      end
      object Edit3: TEdit
        Left = 364
        Top = 36
        Width = 121
        Height = 21
        TabOrder = 13
        Text = '0221'
      end
      object Button19: TButton
        Left = 384
        Top = 268
        Width = 75
        Height = 25
        Caption = 'Button19'
        TabOrder = 14
        OnClick = Button19Click
      end
      object Button20: TButton
        Left = 366
        Top = 160
        Width = 75
        Height = 25
        Caption = 'Map Section'
        TabOrder = 15
        OnClick = Button20Click
      end
      object Button22: TButton
        Left = 450
        Top = 128
        Width = 75
        Height = 25
        Caption = 'Copy monster'
        TabOrder = 16
        OnClick = Button22Click
      end
      object Button33: TButton
        Left = 450
        Top = 160
        Width = 75
        Height = 25
        Caption = 'Save quest info'
        TabOrder = 17
        OnClick = Button33Click
      end
      object Button34: TButton
        Left = 366
        Top = 194
        Width = 75
        Height = 25
        Caption = 'Copy pos'
        TabOrder = 18
        OnClick = Button34Click
      end
      object Button45: TButton
        Left = 531
        Top = 160
        Width = 75
        Height = 25
        Caption = 'Save pipe'
        TabOrder = 19
        OnClick = Button45Click
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Debug'
      ImageIndex = 1
      object Button4: TButton
        Left = 572
        Top = 396
        Width = 75
        Height = 25
        Caption = 'Clear'
        TabOrder = 0
        OnClick = Button4Click
      end
      object ComboBox3: TComboBox
        Left = 2
        Top = 388
        Width = 145
        Height = 22
        Style = csOwnerDrawFixed
        ItemIndex = 1
        TabOrder = 1
        Text = 'Errors'
        Items.Strings = (
          'None'
          'Errors'
          'ALL')
      end
      object PageControl2: TPageControl
        Left = 0
        Top = 0
        Width = 644
        Height = 379
        ActivePage = TabSheet7
        Align = alTop
        TabOrder = 2
        object TabSheet7: TTabSheet
          Caption = 'Debug'
          object MemoDebug: TMemo
            Left = 0
            Top = 0
            Width = 636
            Height = 351
            Align = alClient
            Lines.Strings = (
              'Memo1')
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'Action'
          ImageIndex = 1
          object MemoTest: TMemo
            Left = 0
            Top = 0
            Width = 636
            Height = 351
            Align = alClient
            Lines.Strings = (
              'Memo1')
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Tool'
      ImageIndex = 3
      object GroupBox4: TGroupBox
        Left = 12
        Top = 14
        Width = 437
        Height = 167
        Caption = 'XorSub key'
        TabOrder = 0
        object Label13: TLabel
          Left = 8
          Top = 22
          Width = 92
          Height = 13
          Caption = 'Value               Size'
        end
        object StringGrid1: TStringGrid
          Left = 6
          Top = 40
          Width = 165
          Height = 120
          ColCount = 2
          FixedCols = 0
          RowCount = 8
          FixedRows = 0
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          ColWidths = (
            64
            64)
          RowHeights = (
            24
            24
            24
            24
            24
            24
            24
            24)
        end
        object Button15: TButton
          Left = 182
          Top = 40
          Width = 75
          Height = 25
          Caption = 'Find'
          TabOrder = 1
          OnClick = Button15Click
        end
        object Memo3: TMemo
          Left = 270
          Top = 40
          Width = 143
          Height = 113
          Lines.Strings = (
            'Memo3')
          TabOrder = 2
        end
        object Button28: TButton
          Left = 184
          Top = 92
          Width = 75
          Height = 25
          Caption = 'EXE'
          TabOrder = 3
          OnClick = Button28Click
        end
      end
      object GroupBox5: TGroupBox
        Left = 12
        Top = 186
        Width = 595
        Height = 227
        Caption = 'Items name '
        TabOrder = 1
        object Label15: TLabel
          Left = 348
          Top = 60
          Width = 38
          Height = 13
          Caption = 'Label15'
        end
        object ListBox2: TListBox
          Left = 8
          Top = 22
          Width = 331
          Height = 197
          ItemHeight = 13
          TabOrder = 0
          OnClick = ListBox2Click
        end
        object Button17: TButton
          Left = 346
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Copy'
          TabOrder = 1
          OnClick = Button17Click
        end
        object Button18: TButton
          Left = 428
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Set'
          TabOrder = 2
          OnClick = Button18Click
        end
        object Button26: TButton
          Left = 510
          Top = 24
          Width = 75
          Height = 25
          Caption = 'Quick Translate'
          TabOrder = 3
          OnClick = Button26Click
        end
        object Button29: TButton
          Left = 508
          Top = 190
          Width = 75
          Height = 25
          Caption = 'Copy list'
          TabOrder = 4
          OnClick = Button29Click
        end
        object Button36: TButton
          Left = 510
          Top = 160
          Width = 75
          Height = 25
          Caption = 'Merge file'
          TabOrder = 5
          OnClick = Button36Click
        end
        object test5: TButton
          Left = 508
          Top = 64
          Width = 75
          Height = 25
          Caption = 'test5'
          TabOrder = 6
          OnClick = test5Click
        end
      end
      object Button23: TButton
        Left = 462
        Top = 32
        Width = 75
        Height = 25
        Caption = 'Test'
        TabOrder = 2
        OnClick = Button23Click
      end
      object Edit4: TEdit
        Left = 464
        Top = 72
        Width = 121
        Height = 21
        TabOrder = 3
        Text = 'Edit4'
      end
      object Button24: TButton
        Left = 464
        Top = 98
        Width = 73
        Height = 25
        Caption = 'read float16'
        TabOrder = 4
        OnClick = Button24Click
      end
      object Button27: TButton
        Left = 466
        Top = 140
        Width = 137
        Height = 25
        Caption = 'Extract Event items'
        TabOrder = 5
        OnClick = Button27Click
      end
      object Button38: TButton
        Left = 542
        Top = 98
        Width = 75
        Height = 25
        Caption = 'Send Status'
        TabOrder = 6
        OnClick = Button38Click
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Map Tool'
      ImageIndex = 4
      object Image1: TImage
        Left = 205
        Top = 0
        Width = 439
        Height = 420
        Align = alClient
        ExplicitWidth = 447
        ExplicitHeight = 428
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 205
        Height = 420
        Align = alLeft
        Caption = ' Objects :  '
        TabOrder = 0
        object ListBox3: TListBox
          Left = 2
          Top = 15
          Width = 201
          Height = 342
          Align = alTop
          ItemHeight = 13
          TabOrder = 0
          OnClick = ListBox3Click
        end
        object Button21: TButton
          Left = 4
          Top = 360
          Width = 61
          Height = 25
          Caption = 'Load'
          TabOrder = 1
          OnClick = Button21Click
        end
        object Button31: TButton
          Left = 136
          Top = 360
          Width = 65
          Height = 25
          Caption = 'copy'
          TabOrder = 2
          OnClick = Button31Click
        end
        object Button42: TButton
          Left = 3
          Top = 391
          Width = 75
          Height = 25
          Caption = 'Send'
          TabOrder = 3
          OnClick = Button42Click
        end
        object Button43: TButton
          Left = 85
          Top = 391
          Width = 75
          Height = 25
          Caption = 'Clear'
          TabOrder = 4
          OnClick = Button43Click
        end
        object Button44: TButton
          Left = 72
          Top = 360
          Width = 58
          Height = 25
          Caption = 'Convert'
          TabOrder = 5
          OnClick = Button44Click
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'SE / PS'
      ImageIndex = 7
      object ListBox4: TListBox
        Left = 14
        Top = 18
        Width = 173
        Height = 151
        ItemHeight = 13
        TabOrder = 0
      end
      object Button39: TButton
        Left = 204
        Top = 21
        Width = 75
        Height = 25
        Caption = 'Set'
        TabOrder = 1
        OnClick = Button39Click
      end
      object Button40: TButton
        Left = 204
        Top = 52
        Width = 75
        Height = 25
        Caption = 'Clear'
        TabOrder = 2
        OnClick = Button40Click
      end
      object Button41: TButton
        Left = 204
        Top = 144
        Width = 75
        Height = 25
        Caption = 'Button41'
        TabOrder = 3
        OnClick = Button41Click
      end
      object Memo9: TMemo
        Left = 14
        Top = 192
        Width = 523
        Height = 217
        Lines.Strings = (
          'Memo9')
        TabOrder = 4
      end
      object Send: TButton
        Left = 543
        Top = 384
        Width = 75
        Height = 25
        Caption = 'Send'
        TabOrder = 5
        OnClick = SendClick
      end
      object GroupBox9: TGroupBox
        Left = 304
        Top = 21
        Width = 201
        Height = 148
        Caption = 'Add account'
        TabOrder = 6
        object Label18: TLabel
          Left = 16
          Top = 20
          Width = 48
          Height = 13
          Caption = 'Username'
        end
        object Label19: TLabel
          Left = 16
          Top = 68
          Width = 46
          Height = 13
          Caption = 'Password'
        end
        object Edit6: TEdit
          Left = 32
          Top = 39
          Width = 153
          Height = 21
          TabOrder = 0
          Text = 'Edit6'
        end
        object Edit7: TEdit
          Left = 32
          Top = 87
          Width = 153
          Height = 21
          TabOrder = 1
          Text = 'Edit6'
        end
        object Button46: TButton
          Left = 112
          Top = 114
          Width = 75
          Height = 25
          Caption = 'create'
          TabOrder = 2
          OnClick = Button46Click
        end
      end
    end
  end
  object ServerSocket1: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    OnAccept = ServerSocket1Accept
    OnClientDisconnect = ServerSocket1ClientDisconnect
    OnClientRead = ServerSocket1ClientRead
    OnClientError = ServerSocket1ClientError
    Left = 540
    Top = 26
  end
  object ServerSocket2: TServerSocket
    Active = False
    Port = 0
    ServerType = stNonBlocking
    OnAccept = ServerSocket1Accept
    OnClientDisconnect = ServerSocket1ClientDisconnect
    OnClientRead = ServerSocket1ClientRead
    OnClientError = ServerSocket1ClientError
    Left = 540
    Top = 56
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 30000
    OnTimer = Timer1Timer
    Left = 618
    Top = 26
  end
  object ClientSocket1: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 0
    OnConnect = ClientSocket1Connect
    OnError = ClientSocket1Error
    Left = 616
    Top = 114
  end
  object Timer2: TTimer
    Enabled = False
    OnTimer = Timer2Timer
    Left = 618
    Top = 64
  end
  object Timer3: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer3Timer
    Left = 576
    Top = 28
  end
  object SaveDialog1: TSaveDialog
    Left = 578
    Top = 58
  end
  object ServerSocket3: TServerSocket
    Active = False
    Port = 12099
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 614
    Top = 142
  end
  object ServerSocket4: TServerSocket
    Active = False
    Port = 12199
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 614
    Top = 158
  end
  object ServerSocket5: TServerSocket
    Active = False
    Port = 12299
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 616
    Top = 184
  end
  object ServerSocket6: TServerSocket
    Active = False
    Port = 12399
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 614
    Top = 210
  end
  object ServerSocket7: TServerSocket
    Active = False
    Port = 12499
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 614
    Top = 234
  end
  object ServerSocket8: TServerSocket
    Active = False
    Port = 12599
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 614
    Top = 260
  end
  object ServerSocket9: TServerSocket
    Active = False
    Port = 12699
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 616
    Top = 282
  end
  object ServerSocket10: TServerSocket
    Active = False
    Port = 12799
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 616
    Top = 312
  end
  object ServerSocket11: TServerSocket
    Active = False
    Port = 12899
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 614
    Top = 334
  end
  object ServerSocket12: TServerSocket
    Active = False
    Port = 12999
    ServerType = stNonBlocking
    OnAccept = ServerSocket3Accept
    OnClientError = ServerSocket3ClientError
    Left = 612
    Top = 362
  end
  object Timer4: TTimer
    Interval = 500
    OnTimer = Timer4Timer
    Left = 580
    Top = 92
  end
  object OpenDialog1: TOpenDialog
    Left = 582
    Top = 126
  end
  object ServerSocket13: TServerSocket
    Active = False
    Port = 8080
    ServerType = stNonBlocking
    OnClientRead = ServerSocket13ClientRead
    OnClientError = ServerSocket13ClientError
    Left = 538
    Top = 100
  end
end
