unit UMail;

interface

uses types,sysutils;

type
    TMailEntry = record
        Owner,From,datetime,unread,id:dword;
        fromname:array[0..$21] of widechar;
        subject:array[0..$29] of widechar;
        body:array[0..1023] of widechar;
    end;

procedure initMail();
procedure NotifyForMail(x:integer);
procedure GetMailBox(x:integer;s:ansistring);
procedure GetMessageBody(x:integer;id:dword);
procedure sendmail(x:integer;s:ansistring);
procedure DeleteMail(x:integer;s:ansistring);

var
    mail:array of TMailEntry;
    mailcount:integer = 0;
    mailid:dword;

implementation

uses fmain, UPlayer, UPsoUtil;

procedure initMail();
var f,l:integer;
begin
    f:=fileopen('data\mail.dat',$40);
    if f > -1 then begin
        l:=fileseek(f,0,2);
        fileseek(f,0,0);
        setlength(mail,(l div sizeof(TMailEntry)) + 100);
        fileread(f,mail[0],l);
        mailcount:=l div sizeof(TMailEntry);
        fileclose(f);
        for l:=0 to mailcount-1 do mail[l].id:=l;
        mailid:=mailcount;
    end else setlength(mail,100);

    {mail[0].Owner:=$9c5f56;
    mail[0].From:=$9c5f56;
    mail[0].datetime:=0;
    mail[0].unread:=1;
    mail[0].id:=1;
    mail[0].fromname[0]:='P';
    mail[0].fromname[1]:=#0;
    mail[0].subject[0]:='S';
    mail[0].subject[1]:=#0;
    mail[0].body[0]:='B';
    mail[0].body[1]:=#0;
    mailcount:=1;    }
end;

procedure sendmail(x:integer;s:ansistring);
var i,f:integer;
    id:dword;
begin
    if length(s) > 1240 then s:=copy(s,1,1240);
    if mailcount mod 100 = 99 then begin
        setlength(mail,mailcount+101);
    end;
     {000000: F8 00 00 00 1A 0B 04 00 DC A6 9B 00 00 00 00 00     �.......ܦ�..... //target player
000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000030: 01 00 00 00 00 00 00 00 00 00 00 00 11 04 00 51     ...............Q
000040: 50 00 69 00 6B 00 61 00 73 00 6F 00 6F 00 00 00     P.i.k.a.s.o.o... //from name
000050: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000060: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000080: 00 00 00 00 73 00 75 00 62 00 6A 00 65 00 63 00     ....s.u.b.j.e.c. //subject
000090: 74 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     t...............
0000A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000D0: 00 00 00 00 00 00 00 00 B6 70 00 00 6D 00 65 00     ........�p..m.e.
0000E0: 73 00 73 00 61 00 67 00 65 00 20 00 62 00 6F 00     s.s.a.g.e. .b.o.
0000F0: 64 00 79 00 00 00 00 00     d.y.....}

    move(s[9],mail[mailcount].Owner,4);
    move(s[9],id,4);
    mail[mailcount].id:=mailid;
    inc(mailid);
    mail[mailcount].From:=player[x].gid;
    mail[mailcount].datetime:=Round((now() - 25569) * 86400); //current time in unix format :)
    mail[mailcount].unread:=1;
    move(s[$41],mail[mailcount].fromname[0],$44);
    move(s[$85],mail[mailcount].subject[0],$54);
    move(s[$dd],mail[mailcount].body[0],length(s)-$dc);
    inc(mailcount);

    //confirm user
    sendtoplayer(x,#$10#$00#$00#$00#$1A#$0C#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00);

    //if the target is online, notify the new mail
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].gid = id) then begin
            sendtoplayer(i,#$08#$00#$00#$00#$1A#$0D#$00#$00);
        end;

    f:=filecreate(path+'data\mail.dat');
    filewrite(f,mail[0],sizeof(TMailEntry)*mailcount);
    fileclose(f);
end;

//used to send new mail notification when player login
procedure NotifyForMail(x:integer);
var i,c:integer;
begin
    c:=0;
    for i:=0 to mailcount-1 do
        if (mail[i].Owner = player[x].gid) and (mail[i].unread = 1) then c:=1;
    if c = 1 then sendtoplayer(x,#$08#$00#$00#$00#$1A#$0D#$00#$00);
end;


procedure GetMailBox(x:integer;s:ansistring);
var a,b:ansistring;
    i,c:integer;
begin
{XOR: 36A1   SUB: BF
000000: CC 01 00 00 1A 01 04 00 00 00 02 00 65 00 00 00     �...........e... //65 and 41 04 42 03 found in the request
000010: 01 00 00 00 41 04 42 03 66 36 00 00 50 00 69 00     ....A.B.f6..P.i.
000020: 6B 00 61 00 73 00 6F 00 6F 00 00 00 62 36 00 00     k.a.s.o.o...b6..
000030: 66 00 66 00 66 00 00 00 60 36 00 00 8E FC E8 00     f.f.f...`6..���.
000040: 00 00 00 00 56 5F 9C 00 00 00 00 00 DC A6 9B 00     ....V_�.....ܦ�.
000050: 00 00 00 00 DC A6 9B 00 00 00 00 00 01 00 00 00     ....ܦ�.........
000060: 00 00 00 00 C7 AD F1 57 00 00 00 00 4D 00 49 00     ....ǭ�W....M.I.
000070: 55 00 52 00 45 00 54 00 48 00 00 00 00 00 00 00     U.R.E.T.H.......
000080: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000090: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000B0: 52 00 65 00 3A 00 20 00 73 00 75 00 62 00 6A 00     R.e.:. .s.u.b.j.
0000C0: 65 00 63 00 74 00 00 00 00 00 00 00 00 00 00 00     e.c.t...........
0000D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000E0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000F0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000100: 00 00 00 00 22 ED E8 00 00 00 00 00 56 5F 9C 00     ...."��.....V_�.
000110: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000120: 00 00 00 00 0A 00 00 00 00 00 00 00 94 89 F0 57     ............���W
000130: 00 00 01 00 D5 30 EC 30 F3 30 C9 30 EA 30 B9 30     ....�0�0�0�0�0�0
000140: C8 30 B7 30 B9 30 C6 30 E0 30 00 00 00 00 00 00     �0�0�0�0�0......
000150: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000160: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000170: 00 00 00 00 00 00 00 00 4D 00 49 00 55 00 52 00     ........M.I.U.R.
000180: 45 00 54 00 48 00 20 00 68 30 6E 30 D5 30 EC 30     E.T.H. .h0n0�0�0
000190: F3 30 C9 30 7B 76 32 93 8C 5B 86 4E 6E 30 4A 30     �0�0{v2��[�Nn0J0
0001A0: E5 77 89 30 5B 30 00 00 00 00 00 00 00 00 00 00     �w�0[0..........
0001B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0001C0: 00 00 00 00 00 00 00 00 00 00 00 00     ............}
    setlength(b,$c8);
    fillchar(b[1],$30,0);
    a:='';
    c:=0;
    for i:=mailcount-1 downto 0 do
        if mail[i].Owner = player[x].GID then begin
            move(mail[i].id,b[1],4); //mail id
            move(mail[i].owner,b[9],4);//player id
            move(mail[i].from,b[$11],4);//from id
            move(mail[i].from,b[$19],4);//from id
            b[$21]:=#1; //mail type 1 = normal, A = system
            move(mail[i].datetime,b[$29],4);//time
            b[$2f]:=#0;
            if mail[i].unread = 0 then b[$2f]:=#1;
            move(mail[i].fromname[0],b[$31],$44);
            move(mail[i].subject[0],b[$75],$54);
            a:=a+b;
            inc(c);
        end;
    a:=#0#0#0#0#$1a#1#4#0#0#0+chr(c)+#0 //count
    +copy(s,$d,4)+#1#0#0#0+copy(s,$11,4)
    + TextToUni(player[x].aname+#0,$36A1,$BF) //player name
    + GetCharNameWithMagic(x,$36A1,$BF) //char name
    +GetMagic(c,$36A1,$BF) + a; //messages
    c:=length(a);
    move(c,a[1],4);
    sendtoplayer(x,a);
    
end;

procedure GetMessageBody(x:integer;id:dword);
var i,l:integer;
    s,a:ansistring;
begin
    for i:=0 to mailcount-1 do
        if (mail[i].Owner = player[x].gid) and (mail[i].id = id) then begin
            s:=#$3C#$00#$00#$00#$1A#$07#$04#$00#$8E#$FC#$E8#$00#$00#$00#$00#$00
            +#$01#$00#$00#$00;
            move(id,s[9],4);
            l:=0;
            while mail[i].body[l] <> #0 do inc(l);
            inc(l);
            setlength(a,l*2);
            move(mail[i].body[0],a[1],l*2);
            if l and 1 = 1 then a:=a+#0#0;
            s:=s+GetMagic(l,$5913,$82)+a;
            l:=length(s);
            move(l,s[1],4);
            sendtoplayer(x,s);
            mail[i].unread:=0;
        end;
end;

procedure DeleteMail(x:integer;s:ansistring);
var i,c:integer;
    id:dword;
    a:ansistring;
begin
    c:=13;
    while c < length(s) do begin
        move(s[c],id,4);
        for i:=0 to mailcount-1 do
        if (mail[i].Owner = player[x].gid) and (mail[i].id = id) then begin

            a:=#$1C#$00#$00#$00#$1A#$03#$04#$00#$4B#$42#$00#$00#$14#$C0#$EB#$00
	        +#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00;
            move(mail[i].id,a[$d],4);
            sendtoplayer(x,a);
            move(mail[i+1],mail[i],(mailcount-i-1) * sizeof(tmailentry));
            dec(mailcount);
            break;
        end;
        inc(c,12);
    end;
end;

end.
