unit StackTrace;

{
 install JCL(jedi librairy) from tool GetIt
}

interface

uses
SysUtils, Classes, JclDebug;

const
  OutputFile = 'Except.log';

var
  writeToFile : boolean = false;

  procedure SaveDebug(s:string);


implementation

procedure SaveDebug(s:string);
var t : TextFile;
begin
    if writeToFile then
        begin
          try
            Assign( t, OutputFile);
            Append( t);
            Writeln( t, s);
            Close( t);
          except
          end;
        end;
end;

function GetExceptionStackInfoProc(P: PExceptionRecord): Pointer;
var
    LLines: TStringList;
    LText: String;
    LResult: PChar;
    E: Exception;
begin
    LLines := TStringList.Create;
    try
        JclLastExceptStackListToStrings(LLines, True, True, True, True);
        LText := LLines.Text;
        if p.ExceptionFlags and 1=1 then      // This seems to be an indication of internal Delphi exception,
            begin                                    // thus we can access 'Exception' class
              try
                E := Exception( p.ExceptObject);
                if (E is Exception) then
                  LText := 'Delphi exception, type '+E.ClassName+', message: '+E.Message+#13#10+LText;
              except
              end;
            end;
        LText := '-----------------------------------------------------------------------'#13#10+
          'Exception: '+inttohex(p.ExceptionCode,8)+#13#10+LText;

        LResult := StrAlloc(Length(LText));
        StrCopy(LResult, PChar(LText));
        Result := LResult;
        SaveDebug(Ltext);
    finally
        LLines.Free;
    end;
end;

function GetStackInfoStringProc(Info: Pointer): string;
begin
Result := string(PChar(Info));
end;

procedure CleanUpStackInfoProc(Info: Pointer);
begin
StrDispose(PChar(Info));
end;

procedure InitExceptionLogging;
var
  f : file;
begin
  try
    Assign( f, OutputFile);
    Rewrite( f);
    Close( f);
    writeToFile := true;
  except
    writeToFile := false;
  end;
end;

initialization
// Enable raw mode (default mode uses stack frames which aren't always generated by the compiler)
Include(JclStackTrackingOptions, stRawMode);
// Disable stack tracking in dynamically loaded modules (it makes stack tracking code a bit faster)
Include(JclStackTrackingOptions, stStaticModuleList);
if JclStartExceptionTracking then
begin
Exception.GetExceptionStackInfoProc := GetExceptionStackInfoProc;
Exception.GetStackInfoStringProc := GetStackInfoStringProc;
Exception.CleanUpStackInfoProc := CleanUpStackInfoProc;
end;
InitExceptionLogging();
//create file


finalization
// Stop Jcl exception tracking and unregister our provider.
if JclExceptionTrackingActive then
begin
Exception.GetExceptionStackInfoProc := nil;
Exception.GetStackInfoStringProc := nil;
Exception.CleanUpStackInfoProc := nil;
JclStopExceptionTracking;
end;
end.
