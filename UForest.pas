unit UForest;

interface
uses types,sysutils,windows,classes,math,UPlayer,UStats;


procedure GoForest1(x,p:integer);
procedure EnterSectionMap(x:integer;id:pdword);
Function RemoveObjFromFloor(p,T,lvl:integer;ID:int64):ansistring;
procedure AddObjectToFloor(T,lvl:integer;obj:ansistring);
procedure loadmonsterlist(p,t,lvl:integer;fn,evfn:ansistring);
procedure spawnMonster(x,t,lvl:integer);
Procedure triggerMonster(x:integer;s:ansistring);
function IDIsAMonster(t,id,lvl:dword):integer;
procedure ApplyDamageToMonster(x,t,mid,lvl,dmg,tag:integer;pos:ansistring);
procedure UpdateMonsterStatus(tid,lvl,entry,value:integer);
procedure spawnMonsterGroup(t,lvl,g:integer);
procedure TestWave(px,py:single;x,t,lvl:integer);
procedure AddItemToFloor(T,lvl:integer;obj:ansistring);
procedure ApplyDamageToPlayer(x,t,lvl,dmg:integer;mid,pos:ansistring);
procedure ResurectPlayer(x,t:integer);
procedure TriggerObject(x:integer;s:ansistring);
procedure ChangeFloor(x:integer; a:ansistring; debugfloor:integer = -1);
procedure ApplyDamageToPet(x,t,lvl,dmg:integer;mid,pos:ansistring);
procedure SendFenceStatus(p,id:integer; ob:ansistring);
procedure SendMeteo(t:integer);
function playerisnear(x,i,d:integer) : boolean;
procedure AddObjectToPlayerFloor(T,lvl:integer;obj:ansistring);
procedure AddPlayerExp(p,exp:integer);
procedure ApplyDamageToNPC(x,t,mid,dmg:integer;pos:ansistring);
//procedure TestPartyWarp(p:integer);
procedure MakeMonsterTarget(x:integer;s:ansistring);
procedure ApplyEffectToPlayer(x:integer;te:ttecheffect;ele:integer);
procedure WarpParty(p:integer);
procedure ApplyEffectToMonster(t,lvl,mid:integer;te:ttecheffect;ele:integer);
procedure UnhidePipe(x:integer);
procedure ReloadFloor(x:integer);
function GetMissionPacket(x:integer):ansistring;
procedure DistributeEventExp(x,m:integer);
procedure DistributeEventMesetas(x,m:integer);
procedure DistributeReward(t:integer);

const
    MONSTER_CREATE = 1;
    MONSTER_APEAR = 2;
    MONSTER_DIE = 4;
    MONSTER_MOVE = 5;
    MONSTER_ATTACK = 7;

    ACTION_NONE = 0;
    ACTION_ATTACK = 1;
    ACTION_USE = 3;
    ACTION_DEF = 4;
    ACTION_BOOST = 5;

    PA_TwisterFall = 2;
    PA_OverEnd = 5;

    ELE_NONE = 0;
    ELE_FIRE = 1;
    ELE_ICE = 2;
    ELE_LIGHTNING = 3;
    ELE_WIND = 4;
    ELE_LIGHT = 5;
    ELE_DARK = 6;
    ELE_MIX = 7;

    ActionCount = 107;
    ActionType:array[0..106] of TInGameAction = (
    (name:'MonomateS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'DimateS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'TrimateS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'TelepipeS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'ShiftaRideS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'DebandRideS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'MoonAtomizerS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),
    (name:'StarAtomizerS'; action:ACTION_USE; types:0;element:0;mul:1;PA:0),

    (name:'Sword:AttackGA1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'Sword:AttackGA2'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'Sword:AttackGA3'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),
    (name:'Sword:GuardG'; action:ACTION_DEF; types:0;element:0;mul:1.5;PA:0),
    (name:'ActionSub::End'; action:ACTION_NONE; types:0;element:0;mul:1;PA:0),
    (name:'Sword:AttackGB2'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:PA_TwisterFall),
    (name:'Sword:AttackGB5'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:PA_OverEnd),

    (name:'WLance:Attack1G'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:0),
    (name:'WLance:Attack2G'; action:ACTION_ATTACK; types:0; element:0;mul:1.1;PA:0),
    (name:'WLance:Attack3G'; action:ACTION_ATTACK; types:0; element:0;mul:1.2;PA:0),
    (name:'WLance:AttackGB5'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:13),

    (name:'GunSlash:AttackGA1S'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:0),
    (name:'GunSlash:AttackGA2S'; action:ACTION_ATTACK; types:0; element:0;mul:1.1;PA:0),
    (name:'GunSlash:AttackGA3S'; action:ACTION_ATTACK; types:0; element:0;mul:1.2;PA:0),
    (name:'GunSlash:AttackGA1R'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'GunSlash:AttackGA2R'; action:ACTION_ATTACK; types:1; element:0;mul:1.1;PA:0),
    (name:'GunSlash:AttackGA3R'; action:ACTION_ATTACK; types:1; element:0;mul:1.2;PA:0),
    (name:'GunSlash:AttackGB3'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:51),

    (name:'Rifle:AttackGA1'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'Rifle:AttackGA2'; action:ACTION_ATTACK; types:1; element:0;mul:1.1;PA:0),
    (name:'Rifle:AttackGA3'; action:ACTION_ATTACK; types:1; element:0;mul:1.2;PA:0),
    (name:'Rifle:AttackGPA2'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:58),
    (name:'Rifle:AttackGPA3'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:59),

    (name:'Launcher:AttackGA1'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'Launcher:AttackGA2'; action:ACTION_ATTACK; types:1; element:0;mul:1.1;PA:0),
    (name:'Launcher:AttackGA3'; action:ACTION_ATTACK; types:1; element:0;mul:1.2;PA:0),
    (name:'Launcher:AttackGA1C'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'Launcher:AttackGPA1'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:65),  //Divine Launcher
    (name:'Launcher:AttackGPA2'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:66), //Concentrate One

    (name:'Unarmed:AttackGA1'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:0),
    (name:'Unarmed:AttackGA2'; action:ACTION_ATTACK; types:0; element:0;mul:1.1;PA:0),
    (name:'Unarmed:AttackGA3'; action:ACTION_ATTACK; types:0; element:0;mul:1.2;PA:0),
    (name:'Unarmed:AttackGB1'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:41),     //Ducking Blow

    (name:'Rod:AttackGA1'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:0),
    (name:'Rod:AttackGA2'; action:ACTION_ATTACK; types:0; element:0;mul:1.1;PA:0),
    (name:'Rod:AttackGA3'; action:ACTION_ATTACK; types:0; element:0;mul:1.2;PA:0),
    (name:'Talis:AttackGA1'; action:ACTION_ATTACK; types:0; element:0;mul:1;PA:0),
    (name:'Talis:AttackGA2'; action:ACTION_ATTACK; types:0; element:0;mul:1.1;PA:0),
    (name:'Talis:AttackGA3'; action:ACTION_ATTACK; types:0; element:0;mul:1.2;PA:0),

    (name:'Tech:FoieC1'; action:ACTION_ATTACK; types:2; element:ELE_FIRE;mul:1;PA:81),
    (name:'Tech:FoieC2'; action:ACTION_ATTACK; types:2; element:ELE_FIRE;mul:2;PA:81),
    (name:'Tech:IlgrantzC1'; action:ACTION_ATTACK; types:2; element:ELE_LIGHT;mul:1;PA:65),
    (name:'Tech:IlgrantzC2'; action:ACTION_ATTACK; types:2; element:ELE_LIGHT;mul:2;PA:65), //44
    (name:'Tech:ShiftaRide'; action:ACTION_BOOST; types:2; element:ELE_LIGHT;mul:1;PA:85), //44
    (name:'Tech:DebandRide'; action:ACTION_BOOST; types:2; element:ELE_LIGHT;mul:1;PA:90),

    (name:'CompoundBow:A1Gc0'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'CompoundBow:A1Gc1'; action:ACTION_ATTACK; types:1; element:0;mul:1.7;PA:0),
    (name:'CompoundBow:PA1Gc0'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:119),
    (name:'CompoundBow:PA1Gc1'; action:ACTION_ATTACK; types:1; element:0;mul:1.7;PA:119),
    (name:'CompoundBow:PA2Gc0'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:120),
    (name:'CompoundBow:PA2Gc1'; action:ACTION_ATTACK; types:1; element:0;mul:1.7;PA:120),
    (name:'CompoundBow:A2Gc0'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'CompoundBow:A2Gc1'; action:ACTION_ATTACK; types:1; element:0;mul:1.7;PA:0),
    (name:'CompoundBow:A3Gc0'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:0),
    (name:'CompoundBow:A3Gc1'; action:ACTION_ATTACK; types:1; element:0;mul:1.7;PA:0),
    (name:'CompoundBow:PA1C'; action:ACTION_ATTACK; types:1; element:0;mul:1.7;PA:119),
    (name:'CompoundBow:PA2C'; action:ACTION_ATTACK; types:1; element:0;mul:1;PA:120),


    (name:'Katana:GuardG'; action:ACTION_DEF; types:0;element:0;mul:1.5;PA:0),
    (name:'Katana:AttackGA1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'Katana:AttackGA2'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'Katana:AttackGA3'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),
    (name:'Katana:PA11SlashG'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:181),
    (name:'Katana:PA11DushG'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:181),
    (name:'Katana:PA11SlashA'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:181),
    (name:'Katana:PA11DushA'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:181),

    (name:'JetBoots:AttackGA1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'JetBoots:AttackGA2'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'JetBoots:AttackGA3'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),
    (name:'JetBoots:Switch1D'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'JetBoots:Switch2D'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),
    (name:'JetBoots:Switch3D'; action:ACTION_ATTACK; types:0;element:0;mul:1.3;PA:0),
    (name:'JetBoots:AttackGB1C1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:163),
    (name:'JetBoots:AttackGB1C2'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:163),
    (name:'JetBoots:AttackGB1C3'; action:ACTION_ATTACK; types:0;element:0;mul:1.5;PA:163),
    (name:'JetBoots:AttackGB2'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:164),
    (name:'JetBoots:Switch1AirD'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'JetBoots:Switch2AirD'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),
    (name:'JetBoots:Switch3AirD'; action:ACTION_ATTACK; types:0;element:0;mul:1.3;PA:0),
    (name:'JetBoots:AttackShiftJ'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),

    (name:'DBlade:GuardGG'; action:ACTION_DEF; types:0;element:0;mul:1.5;PA:0),
    (name:'DBlade:GuardNG'; action:ACTION_NONE; types:0;element:0;mul:1;PA:0),
    (name:'DBlade:GuardGA'; action:ACTION_DEF; types:0;element:0;mul:1.5;PA:0),
    (name:'DBlade:GuardNA'; action:ACTION_NONE; types:0;element:0;mul:1;PA:0),
    (name:'DBlade:PA1G'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:166),
    (name:'DBlade:AttackGA1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'DBlade:AttackGA2'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'DBlade:AttackGA3'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),

    (name:'TMachineGun:AttackGA1'; action:ACTION_ATTACK; types:1;element:0;mul:1;PA:0),
    (name:'TMachineGun:AttackGA2'; action:ACTION_ATTACK; types:1;element:0;mul:1.1;PA:0),
    (name:'TMachineGun:AttackGA3'; action:ACTION_ATTACK; types:1;element:0;mul:1.2;PA:0),

    (name:'Takt:AttackGA1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'Takt:AttackGA2'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'Takt:AttackGA3'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'AtkN1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:0),
    (name:'AtkN2'; action:ACTION_ATTACK; types:0;element:0;mul:1.1;PA:0),
    (name:'AtkN3'; action:ACTION_ATTACK; types:0;element:0;mul:1.2;PA:0),
    (name:'AtkPa1'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:1),
    (name:'AtkPa2'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:2),
    (name:'AtkPa3'; action:ACTION_ATTACK; types:0;element:0;mul:1;PA:3)
    );


implementation

uses  UDB, UInventory, UMovement, UQuestBoard, fmain, UMonster,
  UPsoUtil, casino, UDrop, UParty, UDebug, ULobby, GameEvent, ItemGenerator,
  UTimerHelper, UScriptEngine, GameInteraction;


function GetMissionPacket(x:integer):ansistring;
var i:integer;
begin
    result:=team[player[x].teamid].Mission;
    move(player[x].partyid,result[$15],4);
    i:=$25;
    while result[i] <> #0 do inc(i);
    i:=(i+3) and $ffc;
    inc(i,5);
    while result[i] <> #0 do inc(i);
    i:=(i+3) and $ffc;
    inc(i);
    safemove(player[x].ping,result,i,8);
end;

function GenerateMonsterPacket(a,b:ansistring):ansistring;
var i:integer;
begin
    if a = '' then exit; //no monster loaded dont do anything
    i:=(readmagic(copy(a,$a1,4),$250B,$b2)+3) and $fc; //size of current string
    delete(a,$a1,i+4); //remove the old string
    insert(getmagic(length(b)+1,$250B,$b2), a,$a1);
    b:=b+#0;
    while length(b) and 3 <> 0 do b:=b+#0;
    insert(b, a,$a5);
    i:=length(a);
    safemove(i,a,1,4);
    i:=$11a9;
    safemove(i,a,$13,2);
    result:=a;
end;

procedure ResurectPlayer(x,t:integer);
var s:ansistring;
    i:integer;
begin
    s:=#$48#$00#$00#$00#$04#$2D#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$D2#$00#$00#$00
	+#$02#$00#$00#$00#$2A#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    {i:=player[x].activechar.base.job;
    player[x].hp := lvlstats[i,player[x].activechar.base.jobstat[i].lvl,Job_HP];
    i:= (player[x].activechar.base.race shl 1) or player[x].activechar.base.sex;}
    player[x].hp:=player[x].maxhp;//round(player[x].hp*racemod[i,RM_HP]);
    safemove(t,s,$41,4);
    safemove(player[x].hp,s,$3d,4);
    sendtolobby(player[x].room,-1,s);
end;

procedure SendFenceStatus(p,id:integer; ob:ansistring);
var x,i,k:integer;
    s:ansistring;
begin
    x:=1;
    while x<length(ob) do begin
        move(ob[x],i,4);
        move(ob[x+8],k,4);
        if k = id then begin
            s:=#$4c#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
            if ob[x+$43] = #0 then s:=s+#$D4#$5C#$00#$00'CLOSE'#0#0#0
            else s:=s+#$D5#$5C#$00#$00'OPEN'#0#0#0#0;

            safemove(player[p].gid,s,9,4);
            safemove(ob[x+8],s,$15,12); //copy the id
            safemove(ob[x+8],s,$25,12); //copy the id
            Sendtoplayer(p,s);
            break;
        end;

        inc(x,i);
    end;
end;


procedure SendMeteo(t:integer);
var s,sc,a:ansistring;
    i:integer;
begin
    //XOR: 4E01   SUB: 83
    if team[t].meteo.Count = 0 then exit;
    s:=team[t].meteo[team[t].cmeteo]+#0;
    while length(s) and 3 <> 0 do s:=s+#0;
    s:=#$4C#$00#$00#$00#$17#$0F#$04#$00#$A9#$11#$00#$00#$00#$00#$00#$00
    +#$03#$00#$00#$00#$36#$01#$00#$00#$03#$00#$00#$00#$2D#$C4#$75#$17
    +#$0A#$00#$00#$00#$04#$00#$00#$00#$03#$00#$00#$00#$02#$00#$00#$00
    +#$02#$00#$00#$00#$FF#$FF#$FF#$FF#$01#$03#$00#$00+GetMagic(length(s),$4e01,$83)
    +s+#$00#$00#$00#$00;

    a:='Naberias'#0#0#0#0;
    if team[t].planet = 4 then a:='Amduscia'#0#0#0#0;
    sc:=#$4C#$00#$00#$00#$17#$0F#$04#$00#$A9#$11#$00#$00#$00#$00#$00#$00
    +#$03#$00#$00#$00#$36#$01#$00#$00#$03#$00#$00#$00#$2D#$C4#$75#$17
    +#$0A#$00#$00#$00#$04#$00#$00#$00#$03#$00#$00#$00#$02#$00#$00#$00
    +#$02#$00#$00#$00#$FF#$FF#$FF#$FF#$01#$03#$00#$00+GetMagic(length(a),$4e01,$83)
    +a+#$00#$00#$00#$00;

    i:=length(s);
    safemove(i,s,1,4);

    i:=length(sc);
    safemove(i,sc,1,4);

    //sendtolobby(t+ROOM_TEAM,-1,s);
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].TeamID = t) and (player[i].cmap <> '') and (player[i].room>=room_team) then begin
            safemove(player[i].cmap[1],s,13,$30);
            safemove(player[i].cmap[1],sc,13,$30);
            if player[i].Floor>=0 then sendtoplayer(i,s)
            else sendtoplayer(i,sc);
        end;
end;

procedure GoForest1(x,p:integer);
var s:ansistring;
    i,l:integer;
    d:pdword;
begin
    leavelobby(x);
    player[x].room:=Room_Team+player[x].teamid;

    player[x].floor:=0;
    if team[player[x].TeamID].Multi = 1 then
    if team[player[x].TeamID].splitfloor[0] = 0 then begin
        //look for party floor
        {if team[player[x].TeamID].p1 = player[x].partyid then player[x].Floor:= team[player[x].TeamID].party1floor-1 ;
        if team[player[x].TeamID].p2 = player[x].partyid then player[x].Floor:= team[player[x].TeamID].party2floor-1 ;
        if team[player[x].TeamID].p3 = player[x].partyid then player[x].Floor:= team[player[x].TeamID].party3floor-1 ;   }
        if team[player[x].TeamID].p2 = player[x].partyid then inc(player[x].Floor,1); //floor always fallow
        if team[player[x].TeamID].p3 = player[x].partyid then inc(player[x].Floor,2);
        //player[x].Floor:=2;
    end;
    if p = 1 then player[x].floor:=player[x].pipefloor;

    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);



    s:=team[player[x].TeamID].FloorMap[player[x].Floor]; //filegetpacket('quest\'+inttohex(team[player[x].TeamID].questid,1)+'\floor'+inttostr(player[x].Floor+1)+'.map');
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);

    player[x].cmap:=copy(s,$25,$30);


    sendtoplayer(x,#$54#$07#$00#$00#$04#$28#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$01#$00#$00#$00#$52#$49#$00#$00#$00#$00#$00#$00#$00#$03#$00#$00
	+#$CF#$30#$F3#$30#$BF#$30#$FC#$30#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$11#$00#$00#$00#$00#$00#$11#$00#$00#$11#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$01#$00#$00#$00#$01#$03#$00#$00#$EC#$30#$F3#$30
	+#$B8#$30#$E3#$30#$FC#$30#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$10#$00
	+#$00#$00#$00#$00#$10#$00#$00#$00#$00#$10#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$02#$00#$00#$00#$02#$03#$00#$00#$D5#$30#$A9#$30#$FC#$30#$B9#$30
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$10#$00#$00#$10#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00
	+#$03#$03#$00#$00#$D5#$30#$A1#$30#$A4#$30#$BF#$30#$FC#$30#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$10#$00#$00#$00#$00#$00#$10#$00#$00#$10
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00#$04#$03#$00#$00
	+#$AC#$30#$F3#$30#$CA#$30#$FC#$30#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$10#$00#$00#$00#$00#$00#$10#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$05#$00#$00#$00#$05#$03#$00#$00#$C6#$30#$AF#$30
	+#$BF#$30#$FC#$30#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$10#$00
	+#$00#$10#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$06#$00#$00#$00#$06#$03#$00#$00#$D6#$30#$EC#$30#$A4#$30#$D0#$30
	+#$FC#$30#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$10#$00#$00#$00#$00#$00
	+#$10#$00#$00#$10#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$07#$00#$00#$00
	+#$07#$03#$00#$00#$D0#$30#$A6#$30#$F3#$30#$B5#$30#$FC#$30#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$01#$11#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$08#$00#$00#$00#$09#$03#$00#$00
	+#$B5#$30#$E2#$30#$CA#$30#$FC#$30#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$11#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00);



    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    sendtoplayer(x,s);

    //s:=filegetpacket('quest\'+inttohex(team[player[x].TeamID].questid,1)+'\floor'+inttostr(player[x].Floor+1)+'.spawn1');
    s:=team[player[x].TeamID].floorspawn[0,player[x].Floor];

    if p = 1 then begin
        //look for the pipe and spawn there
        i:=1;
        while i < length(team[player[x].teamid].FloorObj[player[x].pipefloor]) do begin
        d:=@team[player[x].teamid].FloorObj[player[x].pipefloor][i+8];
        if player[x].pipeid = d^ then begin
            //item found get its type
            if team[player[x].teamid].FloorObj[player[x].pipefloor][i+12] = #2 then begin
                //found it!
                move(team[player[x].teamid].FloorObj[player[x].pipefloor][i+$14],s[1],16);
            end;
            move(team[player[x].teamid].FloorObj[player[x].pipefloor][i],l,4);
            inc(i,l);
        end else begin
            move(team[player[x].teamid].FloorObj[player[x].pipefloor][i],l,4);
            inc(i,l);
        end;
    end;
    end;

    move(s[9],player[x].curpos,8);
    move(s[1],player[x].rotation,8);
    SpawnPlayer(x);
    //send all other player in game

    s:=#$28#$00#$00#$00#$04#$9D#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$01#$00#$00#$00#$6C#$69#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);

    s:=#$2C#$00#$00#$00#$04#$45#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);

    s:=#$24#$00#$00#$00#$04#$B0#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);


    //send map obj

    sendtoplayer(x,team[player[x].teamid].FloorObj[player[x].floor]);
    sendtoplayer(x,player[x].FloorObj[player[x].floor]);


    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$18#$00#$00#$00#$0B#$25#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
	+#$10#$00#$00#$00#$00#$00#$00#$00 ); //monster or item id?

    sendtoplayer(x,#$18#$00#$00#$00#$0B#$42#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
	+#$10#$00#$00#$00#$01#$00#$00#$00);

    //unknown
   { sendtoplayer(x,#$24#$00#$00#$00#$0B#$10#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
	+#$10#$00#$00#$00#$7B#$36#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
	+#$00#$00#$00#$00);
  move(player[x].partyid,s[$15],4);
    sendtoplayer(x,s);

  s:=#$28#$00#$00#$00#$0B#$00#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
    +#$10#$00#$00#$00#$1D#$E9#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
    +#$00#$00#$00#$00#$02#$00#$00#$00;
    move(player[x].partyid,s[$15],4);
    sendtoplayer(x,s);       }


    //needed for items
    s:=#$1C#$00#$00#$00#$0F#$07#$04#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$0E#$4B#$00#$00#$FF#$32#$00#$00;
    safemove(player[x].gid,s,9,4);
    sendtoplayer(x,s );

    sendtoplayer(x,player[x].FloorItems[player[x].Floor]);


    //1e1c  //should triger sendarmor

    player[x].firstping:=0;
    
    spawnMonster(x,player[x].TeamID,player[x].Floor);

    SendMeteo(player[x].TeamID);

    s:=#$10#$00#$00#$00#$15#$14#$00#$00#0#0#0#0#0#0#0#0;
    i:= team[player[x].TeamID].floor_num[player[x].floor];
    safemove(i,s,9,4);
    sendtoplayer(x,s);

    if player[x].questcompleted = 1 then UnhidePipe(x);

end;



procedure ChangeFloor(x:integer; a:ansistring; debugfloor:integer = -1);
var s:ansistring;
    i,l,pf:integer;
    d:pdword;
begin
    leavelobby(x);

    //inc(player[x].Floor);
    pf:=player[x].Floor;
    if debugfloor > -1 then player[x].Floor:=debugfloor
    else begin
        player[x].Floor:=team[player[x].TeamID].doorref[pf,byte(a[$19])].fl;

        if team[player[x].TeamID].Multi = 1 then
        for l:=0 to 3 do
        if (team[player[x].TeamID].splitfloor[l] = pf+1) and (pf+1 < team[player[x].TeamID].splitfloor[l])  then begin
            //look for party floor
            if team[player[x].TeamID].p2 = player[x].partyid then inc(player[x].Floor,1);
            if team[player[x].TeamID].p3 = player[x].partyid then inc(player[x].Floor,2);
            //player[x].Floor:=2;
        end;
    end;

    player[x].room:=Room_Team+player[x].teamid;



    sendtoplayer(x,#$0C#$00#$00#$00#$1E#$0C#$00#$00#$7D#$00#$00#$00);


    try


    s:=team[player[x].TeamID].FloorMap[player[x].Floor];
    //s:=filegetpacket('quest\'+inttohex(team[player[x].TeamID].questid,1)+'\floor'+inttostr(player[x].Floor+1)+'.map');
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);
    player[x].cmap:=copy(s,$25,$30);

    s:=#$14#$00#$00#$00#$06#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    sendtoplayer(x,s);

    if debugfloor = -1 then begin
      move(team[player[x].TeamID].doorref[pf,byte(a[$19])].pos[0],player[x].rotation,8);
      move(team[player[x].TeamID].doorref[pf,byte(a[$19])].pos[4],player[x].curpos,8);


      //defined door override
      //if team[player[x].TeamID].door_pos[byte(a[$19])].used = 1 then begin
      if team[player[x].TeamID].doorref[pf,byte(a[$19])].uni <> $ffff then begin
        l:=team[player[x].TeamID].doorref[pf,byte(a[$19])].uni;
          if player[x].Floor > pf then begin
              move(team[player[x].TeamID].door_pos[l].posb[0],player[x].Rotation,8);
              move(team[player[x].TeamID].door_pos[l].posb[4],player[x].curpos,8);
          end else begin
              move(team[player[x].TeamID].door_pos[l].posa[0],player[x].Rotation,8);
              move(team[player[x].TeamID].door_pos[l].posa[4],player[x].curpos,8);
          end;
      end;
    end else begin
        move(team[player[x].TeamID].floorspawn[0,player[x].Floor][9],player[x].curpos,8);
    end;

    SpawnPlayer(x);

    s:=#$28#$00#$00#$00#$04#$9D#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$01#$00#$00#$00#$6C#$69#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);

    s:=#$2C#$00#$00#$00#$04#$45#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);

    s:=#$24#$00#$00#$00#$04#$B0#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00;
    safemove(player[x].gid,s,9,4);
    safemove(player[x].gid,s,$15,4);
    sendtoplayer(x,s);

    sendtoplayer(x,team[player[x].teamid].FloorObj[player[x].Floor]);
    sendtoplayer(x,player[x].FloorObj[player[x].floor]);


    sendtoplayer(x,#$08#$00#$00#$00#$03#$23#$00#$00);

    sendtoplayer(x,#$08#$00#$00#$00#$03#$2B#$00#$00);

    sendtoplayer(x,#$18#$00#$00#$00#$0B#$25#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
	+#$10#$00#$00#$00#$00#$00#$00#$00 ); //monster or item id?

    sendtoplayer(x,#$18#$00#$00#$00#$0B#$42#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
	+#$10#$00#$00#$00#$01#$00#$00#$00);

    //unknown
    {sendtoplayer(x,#$24#$00#$00#$00#$0B#$10#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
	+#$10#$00#$00#$00#$7B#$36#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
	+#$01#$00#$00#$00);      }


    //needed for items
    s:=#$1C#$00#$00#$00#$0F#$07#$04#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$0E#$4B#$00#$00#$FF#$32#$00#$00;
    safemove(player[x].gid,s,9,4);
    sendtoplayer(x,s );

    sendtoplayer(x,player[x].FloorItems[player[x].Floor]);

    //1e1c  //should triger sendarmor
    player[x].firstping:=0;
    
    spawnMonster(x,player[x].TeamID,player[x].Floor);

    //tell wich door the player own
    i:=-1;
    if team[player[x].TeamID].p1 = player[x].partyid then i:=0;
    if team[player[x].TeamID].p2 = player[x].partyid then i:=1;
    if team[player[x].TeamID].p3 = player[x].partyid then i:=2;
    if (i<>-1) then if team[player[x].TeamID].doorref[player[x].Floor,i].owner <> 255 then begin
    s:=#$24#$00#$00#$00#$0B#$10#$00#$00#$a9#$11#$00#$00#$00#$00#$00#$00  //unlock door
	+#$10#$00#$00#$00#$2D#$4F#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
	+#$04#$00#$00#$00;
    safemove(player[x].partyid,s,$15,4);
    safemove(team[player[x].TeamID].doorref[player[x].Floor,i].owner,s,$21,1);
    sendtoplayer(x,s);
    end;

    SendMeteo(player[x].TeamID);

    s:=#$10#$00#$00#$00#$15#$14#$00#$00#0#0#0#0#0#0#0#0;
    i:= team[player[x].TeamID].floor_num[player[x].floor];
    safemove(i,s,9,4);
    sendtoplayer(x,s);

    if player[x].questcompleted = 1 then UnhidePipe(x);

    except
        debug(DBG_ERROR,'Couldnt load floor '+inttostr(player[x].Floor+1)+' from door id : '+inttostr(byte(a[$19])));
    end;
end;

function IDIsAMonster(t,id,lvl:dword):integer;
var i:integer;
begin
    result:=-1;
    if (t > 99) then begin
        debug(DBG_ERROR,'Bad team id to test a monster : '+inttostr(t));
        exit;
    end;
    if lvl > 15 then begin
        debug(DBG_ERROR,'Bad Floor to test a monster : '+inttostr(lvl));
        exit;
    end;

    for i:=0 to team[t].monstcount[lvl]-1 do
        if team[t].Monst[lvl,i].id = id then begin
            result:=i;
            break;
        end;
end;


procedure AddPlayerExp(p,exp:integer);
var j,i:integer;
    s:ansistring;
begin
    j:=player[p].activechar.base.job;
    if form1.Memo8.Lines.IndexOf(player[p].username) > -1 then exp:=exp*10;
    inc(player[p].activeChar.base.jobstat[j].exp,exp);

    //did we pass the lvl exp
    while (player[p].activeChar.base.jobstat[j].exp >=
        lvlstats[j,player[p].activeChar.base.jobstat[j].lvl,Job_EXPTNL]) and (player[p].activeChar.base.jobstat[j].lvl<200) do begin
            inc(player[p].activeChar.base.jobstat[j].lvl);
            inc(player[p].activeChar.base.jobstat[j].lvl2);
            player[p].hp:= GetPlayerMaxHp(p);
            player[p].maxhp:=player[p].hp;
        end;
    //send the lvl/exp packet
    s:=#$5C#$00#$00#$00#$06#$05#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$00#$00#$00#$00#$D6#$7C#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$01#$01#$00#$00#$00#$00#$00#$00#$00#$FF#$02#$00
      +#$02#$00#$FF#$1F#$07#$00#$00#$00#$00#$00#$00#$00#$D4#$00#$00#$00
      +#$00#$00#$00#$00#$FF#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    s[$2D]:=ansichar(player[p].activechar.base.job);
    safemove(player[p].activeChar.base.jobstat[j].lvl,s,$2f,2);
    safemove(player[p].activeChar.base.jobstat[j].lvl,s,$31,2);
    safemove(player[p].activeChar.base.jobstat[j].exp,s,$3d,4);
    safemove(player[p].gid,s,$19,4);
    safemove(exp,s,$35,4);
    //move(team[player[p].TeamID].Monst[player[p].Floor,mid].id,s[9],4);
    sendtolobby(player[p].room,-1,s);
end;

function playerisnear(x,i,d:integer) : boolean;
begin
    result:=false;
    if round(sqrt(sqr(HalfPrecisionToFloat(player[x].CurPos.x) - HalfPrecisionToFloat(player[i].CurPos.x)) +
        sqr(HalfPrecisionToFloat(player[x].CurPos.y) - HalfPrecisionToFloat(player[i].CurPos.y)))) < d then result:=true;
end;

procedure ExpDelivery(x,mid:integer);
var i,j,e,p:integer;
    s:ansistring;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].room = player[x].room) and (player[i].Floor = player[x].floor)
            and playerisnear(x,i,15) then begin
        //if team[player[x].TeamID].Monst[player[x].Floor,mid].hit[i] <> $ff then begin
            //add exp
            p:=i;//team[player[x].TeamID].Monst[player[x].Floor,mid].hit[i];
            if (player[p].hp > 0) and (player[p].questcompleted = 0) then begin
                e:=team[player[x].TeamID].Monst[player[x].Floor,mid].exp;
                if x <> p then e:=(e*7) div 10;
                AddPlayerExp(p,e);
            end;
        end;
end;

procedure ApplyEffectToMonster(t,lvl,mid:integer;te:ttecheffect;ele:integer);
var a:ansistring;
    fl:single;
begin
    team[t].Monst[lvl,mid].statustime:=gettickcount+(te.time*1000);
    team[t].Monst[lvl,mid].statusefft:=0;
    if ele = ELE_FIRE then begin
        //burn
        if (team[t].Monst[lvl,mid].statuseffect < 10015) or (team[t].Monst[lvl,mid].statuseffect >10019)
            or (team[t].Monst[lvl,mid].statuseffect < 10015+te.lvl) then team[t].Monst[lvl,mid].statuseffect := 10015+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,a,9,4);
        safemove(team[t].Monst[lvl,mid].id,a,$15,4);
        safemove(team[t].Monst[lvl,mid].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(t+ROOM_TEAM,lvl,a);
    end;

    if ele = ELE_ICE then begin
        //burn
        if (team[t].Monst[lvl,mid].statuseffect < 10005) or (team[t].Monst[lvl,mid].statuseffect >10009)
            or (team[t].Monst[lvl,mid].statuseffect < 10005+te.lvl) then team[t].Monst[lvl,mid].statuseffect := 10005+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,a,9,4);
        safemove(team[t].Monst[lvl,mid].id,a,$15,4);
        safemove(team[t].Monst[lvl,mid].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(t+ROOM_TEAM,lvl,a);
    end;

    if ele = ELE_LIGHTNING then begin
        //burn
        if (team[t].Monst[lvl,mid].statuseffect < 10020) or (team[t].Monst[lvl,mid].statuseffect >10024)
            or (team[t].Monst[lvl,mid].statuseffect < 10020+te.lvl) then team[t].Monst[lvl,mid].statuseffect := 10020+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,a,9,4);
        safemove(team[t].Monst[lvl,mid].id,a,$15,4);
        safemove(team[t].Monst[lvl,mid].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(t+ROOM_TEAM,lvl,a);
    end;

    if ele = ELE_LIGHT then begin
        //dont do on boss
        if team[t].Monst[lvl,mid].boss = 1 then exit;
        //pannic
        if (team[t].Monst[lvl,mid].statuseffect < 10036) or (team[t].Monst[lvl,mid].statuseffect >10040)
            or (team[t].Monst[lvl,mid].statuseffect < 10036+te.lvl) then team[t].Monst[lvl,mid].statuseffect := 10036+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,a,9,4);
        safemove(team[t].Monst[lvl,mid].id,a,$15,4);
        safemove(team[t].Monst[lvl,mid].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(t+ROOM_TEAM,lvl,a);
    end;

    if ele = ELE_DARK then begin
        //poison
        if (team[t].Monst[lvl,mid].statuseffect < 10010) or (team[t].Monst[lvl,mid].statuseffect >10014)
            or (team[t].Monst[lvl,mid].statuseffect < 10010+te.lvl) then team[t].Monst[lvl,mid].statuseffect := 10010+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,a,9,4);
        safemove(team[t].Monst[lvl,mid].id,a,$15,4);
        safemove(team[t].Monst[lvl,mid].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(t+ROOM_TEAM,lvl,a);
    end;
end;

procedure ApplyEffectToPlayer(x:integer;te:ttecheffect;ele:integer);
var a:ansistring;
    fl:single;
begin
    player[x].statustime:=gettickcount+(te.time*1000);
    player[x].statusefft:=0;
    if ele = ELE_FIRE then begin
        //burn
        if (player[x].statuseffect < 10015) or (player[x].statuseffect >10019)
            or (player[x].statuseffect < 10015+te.lvl) then player[x].statuseffect := 10015+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(player[x].gid,a,9,4);
        safemove(player[x].gid,a,$15,4);
        safemove(player[x].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(player[x].room,player[x].Floor,a);
    end;

    if ele = ELE_ICE then begin
        //burn
        if (player[x].statuseffect < 10005) or (player[x].statuseffect >10009)
            or (player[x].statuseffect < 10005+te.lvl) then player[x].statuseffect := 10005+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(player[x].gid,a,9,4);
        safemove(player[x].gid,a,$15,4);
        safemove(player[x].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(player[x].room,player[x].Floor,a);
    end;

    if ele = ELE_LIGHTNING then begin
        //burn
        if (player[x].statuseffect < 10020) or (player[x].statuseffect >10024)
            or (player[x].statuseffect < 10020+te.lvl) then player[x].statuseffect := 10020+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(player[x].gid,a,9,4);
        safemove(player[x].gid,a,$15,4);
        safemove(player[x].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(player[x].room,player[x].Floor,a);
    end;

    if ele = ELE_LIGHT then begin
        //pannic
        if (player[x].statuseffect < 10036) or (player[x].statuseffect >10040)
            or (player[x].statuseffect < 10036+te.lvl) then player[x].statuseffect := 10036+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(player[x].gid,a,9,4);
        safemove(player[x].gid,a,$15,4);
        safemove(player[x].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(player[x].room,player[x].Floor,a);
    end;

    if ele = ELE_DARK then begin
        //poison
        if (player[x].statuseffect < 10010) or (player[x].statuseffect >10014)
            or (player[x].statuseffect < 10010+te.lvl) then player[x].statuseffect := 10010+(te.lvl-1);
        //apply the status with time
        a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$06#$00#$a9#$11#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$c3#$06#$01#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$24#$27#$00#$00 //thunder#$id
          +#$00#$00#$00#$00#$FF#$0F#$EF#$41#$00#$00#$00#$00#$00#$00#$00#$00
          +#$00#$00#$60#$00#$00#$00#$00#$00;
        safemove(player[x].gid,a,9,4);
        safemove(player[x].gid,a,$15,4);
        safemove(player[x].statuseffect,a,$2d,4);
        fl:=te.time;
        safemove(fl,a,$35,4);
        sendtofloor(player[x].room,player[x].Floor,a);
    end;
end;

function GetPlayerInTeam(t,lvl:integer):integer;
var i:integer;
begin
    result:=0;
    for i:=0 to srvcfg.MaxPlayer-1 do
      if playerok(i) and (player[i].TeamID = t) and (player[i].Floor = lvl) then result:=i;
end;

procedure TestNextWave(t,lvl,group:integer);
var i,l,x:integer;
begin
    l:=0;
        for i:=0 to team[t].MonstCount[lvl]-1 do
            if team[t].Monst[lvl,i].group = group then
            if team[t].Monst[lvl,i].wave = 1 then l:=1;
        if l = 0 then begin
            //send next wave
            l:=0;
            for i:=0 to team[t].MonstCount[lvl]-1 do
                if team[t].Monst[lvl,i].group = group then
                    if team[t].Monst[lvl,i].wave > 1 then begin
                        dec(team[t].Monst[lvl,i].wave);
                        l:=1;
                    end;

            spawnMonsterGroup(t,lvl,group);
            //run the script if no more to spawn
            if l = 0 then begin
              for x:=0 to 63 do
                if team[t].GroupInfo[lvl,x].id = group then break;
              if team[t].GroupInfo[lvl,x].onClear <> '' then
              for i:=0 to 127 do
                if team[t].CallBacks[i].name <> '' then
                if team[t].CallBacks[i].name = team[t].GroupInfo[lvl,x].onClear then begin
                    for l:=0 to team[t].CallBacks[i].cmd.Count-1 do
                      ProcessQuestEvent(GetPlayerInTeam(t,lvl),t,team[t].CallBacks[i].cmd[l]);
                    break;
                end;
            end;
        end;

end;


procedure ApplyDamageToMonster(x,t,mid,lvl,dmg,tag:integer;pos:ansistring);
var s:ansistring;
    i,l,p:integer;
    st:tstringlist;
begin
    move(pos[1],team[t].Monst[lvl,mid].curpos,8);
    if team[t].Monst[lvl,mid].status = MONSTER_DIE then begin
        if team[t].Monst[lvl,mid].atype = 'RagRappyA' then
        if (team[t].Monst[lvl,mid].DieTime = 0) and (team[t].Monst[lvl,mid].once = 0) then begin
            team[t].Monst[lvl,mid].once:=1;
            //if rappy
            s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
              +#$00#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$D0#$5C#$00#$00#$52#$61#$70#$70#$79#$44#$72#$6F#$70#$00#$00#$00;
            safemove(team[t].Monst[lvl,mid].id,s,$15,4);
            safemove(team[t].Monst[lvl,mid].id,s,$25,4);
            safemove(player[x].gid,s,9,4);
            sendtolobby(t+ROOM_TEAM,-1,s);
            for p:=0 to srvcfg.MaxPlayer-1 do
                if playerok(p) and (player[p].room = player[x].room) and (player[p].Floor = player[x].Floor) then
            GenerateDrop(p,0,HalfPrecisionToFloat(pword(@pos[1])^) ,HalfPrecisionToFloat(pword(@pos[3])^)
                ,HalfPrecisionToFloat(pword(@pos[5])^));


            s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
              +#$00#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$EB#$5C#$00#$00#$52#$61#$70#$70#$79#$45#$73#$63#$61#$70#$65#$45#$6E#$64#$00#$00;
            safemove(team[t].Monst[lvl,mid].id,s,$15,4);
            safemove(team[t].Monst[lvl,mid].id,s,$25,4);
            safemove(player[x].gid,s,9,4);
            sendtolobby(t+ROOM_TEAM,-1,s);
        end;
        exit; //already dead
    end;

    //tag the player to the monster for exp
    if tag = 1 then
    for i:=0 to 3 do begin
        if team[t].Monst[lvl,mid].hit[i] = $ff then team[t].Monst[lvl,mid].hit[i]:=x;
        if team[t].Monst[lvl,mid].hit[i] = x then break;
    end;

    //make rappy run if passive
    if (team[t].Monst[lvl,mid].passive = 1) and (team[t].Monst[lvl,mid].atype = 'RagRappyA') then begin
        s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$00#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$D0#$5C#$00#$00#$52#$61#$70#$70#$79#$44#$72#$6F#$70#$00#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,s,$15,4);
        safemove(team[t].Monst[lvl,mid].id,s,$25,4);
        safemove(player[x].gid,s,9,4);
        sendtolobby(t+ROOM_TEAM,-1,s);
        s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$00#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$EB#$5C#$00#$00#$52#$61#$70#$70#$79#$45#$73#$63#$61#$70#$65#$45#$6E#$64#$00#$00;
        safemove(team[t].Monst[lvl,mid].id,s,$15,4);
        safemove(team[t].Monst[lvl,mid].id,s,$25,4);
        safemove(player[x].gid,s,9,4);
        sendtolobby(t+ROOM_TEAM,-1,s);
    end;

    dec(team[t].Monst[lvl,mid].hp,dmg);
    if team[t].Monst[lvl,mid].hp <= 0 then team[t].Monst[lvl,mid].hp:=0;
    //send damage
    s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$C3#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
	+#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$A3#$08#$69#$63
	+#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
	+#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
    safemove(team[t].Monst[lvl,mid].id,s,$15,4);
    safemove(player[x].gid,s,$21,4);
    safemove(player[x].gid,s,$9,4);
    safemove(dmg,s,$31,3);  //damage
    if dmg and $1000000 > 0 then s[$39]:=#2; //critical
    safemove(team[t].Monst[lvl,mid].hp,s,$35,4); //life
    safemove(pos[1],s,$3d,8);
    SendToFloor(player[x].room,player[x].floor,s);

    if team[t].Monst[lvl,mid].hp <= 0 then begin
        //send pts
        if  (team[t].Monst[lvl,mid].pts > 0) and (player[x].questcompleted = 0) then begin
            s:=#$14#$00#$00#$00#$0B#$28#$00#$00#$03#$00#$00#$00#$7A#$D8#$69#$BB#$0A#$C1#$00#$00;
            move(team[t].Monst[lvl,mid].pts,s[9],4);
            pword(@pos[1])^ := FloatToHalf(HalfPrecisionToFloat(pword(@pos[1])^) + 0.2);
            pword(@pos[3])^ := FloatToHalf(HalfPrecisionToFloat(pword(@pos[3])^) + 0.2);
            safemove(pos[1],s,13,8);
            SendToFloor(player[x].room,player[x].floor,s);
        end;

        //kill it
        if (team[t].Monst[lvl,mid].passive = 1) and (team[t].Monst[lvl,mid].atype = 'RagRappyA') then begin
           UpdateMonsterStatus(player[x].TeamID,player[x].Floor,mid,3);
        end else
          UpdateMonsterStatus(player[x].TeamID,player[x].Floor,mid,MONSTER_DIE);
        team[t].Monst[lvl,mid].status := MONSTER_DIE;
        team[t].Monst[lvl,mid].wave:=0;
        team[t].Monst[lvl,mid].special:=0;
        team[t].Monst[lvl,mid].DieTime:=gettickcount;
        team[t].Monst[lvl,mid].DieTime2:=gettickcount;

        //does it have a nest master alive
        if team[t].Monst[lvl,mid].respawn = 101 then team[t].Monst[lvl,mid].respawn:=0; //clean auto respawn
        for i:=0 to team[t].MonstCount[lvl]-1 do
            if (team[t].Monst[lvl,i].wave = 1) and (team[t].Monst[lvl,i].group = team[t].Monst[lvl,mid].group) then begin
                if team[t].Monst[lvl,i].isnestmaster = 1 then begin
                    //must respawn
                    team[t].Monst[lvl,mid].respawn:=101;
                end;
            end;

        //if not rappy
        if team[t].Monst[lvl,mid].boss = 1 then begin
            st:=tstringlist.Create;
            st.Add('ver 1');
            st.Add('define OPT_FLOAT_53 = 53');
            st.Add('define OPT_FLOAT_1 = 1');
            st.Add('define OPT_FLOAT_0 = 0');
            st.Add('define OPT_INT_48 = 48');
            st.Add('define OPT_INT_45 = 45');
            st.Add('define OPT_INT_46 = 46');
            st.Add('define OPT_INT_44 = 44');
            st.Add('define OPT_INT_47 = 47');

            st.Add('object ob_9900_0361');
            st.Add('id 1757');
            st.Add('rotation 0,000 0,000 0,000 1,000');
            st.Add('position -29,547 1 -136,125 0,000');
            st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
            st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 08 00 00 00 00 00');
            st.Add('OPT_INT_47 -1');
            st.Add('OPT_INT_44 1');
            st.Add('OPT_INT_46 2');
            st.Add('OPT_INT_45 0');
            st.Add('OPT_INT_48 9');
            st.Add('OPT_FLOAT_0 1');
            st.Add('OPT_FLOAT_1 1');
            st.Add('OPT_FLOAT_53 5');
            s:=GenerateObjData(st,'\quest\');

            st.Free;
            safemove(pos[1],s,$1d,2);
            safemove(pos[5],s,$21,2);
            i:=GetNextTeamItemID(player[x].TeamID);
            safemove(i,s,9,4);
            AddObjectToFloor(player[x].TeamID,player[x].floor,s);


        end else
        if team[t].Monst[lvl,mid].atype <> 'RagRappyA' then begin
            for i:=0 to MonsterCount-1 do
                if team[t].Monst[lvl,mid].atype = Monsters[i].mtype then begin
                    for p:=0 to srvcfg.MaxPlayer-1 do
                        if playerok(p) and (player[p].room = player[x].room) and (player[p].Floor = player[x].Floor) then
                        if random(99) <= drop[Monsters[i].drop].dar then
                            GenerateDrop(p,Monsters[i].drop,HalfPrecisionToFloat(pword(@pos[1])^) ,HalfPrecisionToFloat(pword(@pos[3])^)
                            ,HalfPrecisionToFloat(pword(@pos[5])^));
                    end;
        end;

        //deliver EXP
        ExpDelivery(x,mid);

        //process die event
        CurTargId:=team[t].Monst[lvl,mid].id;
        for i:=0 to team[t].Monst[lvl,mid].diecount-1 do
            ProcessQuestEvent(x,t,team[t].Monst[lvl,mid].dies[i]);

        //test for next wave
        testnextwave(t,lvl,team[t].Monst[lvl,mid].group);
    end;
end;




procedure ApplyDamageToNPC(x,t,mid,dmg:integer;pos:ansistring);
var s:ansistring;
    i,l,p,n:integer;
begin
    //find the npc
    for n:=0 to team[t].npccount-1 do
    if team[t].npcs[n].id = mid then begin
    if team[t].npcs[n].hp = 0 then exit; //already dead
    dec(team[t].npcs[n].hp,dmg);
    if team[t].npcs[n].hp <= 0 then team[t].npcs[n].hp:=0;
    //send damage
    s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$C3#$00#$00#$00#$00#$00#$00#$00#$16#$00#$8A#$03
	+#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$A3#$08#$69#$63
	+#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
	+#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
    safemove(team[t].npcs[n].id,s,$15,4);
    safemove(player[x].gid,s,$21,4);
    safemove(player[x].gid,s,$9,4);
    safemove(dmg,s,$31,3);  //damage
    if dmg and $1000000 > 0 then s[$39]:=#2; //critical
    safemove(team[t].npcs[n].hp,s,$35,4); //life
    safemove(pos[1],s,$3d,8);
    SendToFloor(player[x].room,player[x].floor,s);

    if team[t].npcs[n].hp <= 0 then begin
        //kill it
        s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$16#$00#$8A#$03
            +#$56#$5F#$9C#$00#$00#$00#$00#$00#$16#$00#$8A#$03#$00#$00#$00#$00
            +#$03#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00;
        safemove(player[x].gid,s,9,4);
        safemove(mid,s,$21,4);
        safemove(mid,s,$15,4);
        sendtolobby(player[x].room,-1,s);

        s[6]:=#$f;
        sendtolobby(player[x].room,-1,s);

        s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$16#$00#$8A#$03
            +#$02#$00#$00#$00;
        safemove(player[x].gid,s,9,4);
        safemove(mid,s,$15,4);
        sendtolobby(player[x].room,-1,s);
    end;
    end;
end;




procedure ApplyDamageToPlayer(x,t,lvl,dmg:integer;mid,pos:ansistring);
var s:ansistring;
    i,l:integer;
begin
    if player[x].hp <= 0 then exit; //already dead
    dec(player[x].hp,dmg);
    if player[x].hp <= 0 then player[x].hp:=0;
    //send damage
    s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$C3#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$56#$5F#$9C#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$A3#$08#$69#$63
	+#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
	+#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
    safemove(mid[1],s,$21,12);
    safemove(player[x].gid,s,$15,4);
    safemove(player[x].gid,s,$9,4);
    safemove(dmg,s,$31,3);  //damage
    if dmg and $1000000 > 0 then s[$39]:=#2; //critical
    safemove(player[x].hp,s,$35,4); //life
    safemove(pos[1],s,$3d,8);
    sendtolobby(player[x].room,-1,s);
    
    if player[x].hp <= 0 then begin
        inc(partys[player[x].partyid].death); //for stats
        //kill it
        s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00
            +#$03#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00;
        safemove(player[x].gid,s,9,4);
        safemove(player[x].gid,s,$21,4);
        safemove(player[x].gid,s,$15,4);
        sendtolobby(player[x].room,-1,s);

        s[6]:=#$f;
        sendtolobby(player[x].room,-1,s);

        s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$96#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$02#$00#$00#$00;
        safemove(player[x].gid,s,9,4);
        safemove(player[x].gid,s,$15,4);
        sendtolobby(player[x].room,-1,s);
    end;
end;

procedure ApplyDamageToPet(x,t,lvl,dmg:integer;mid,pos:ansistring);
var s:ansistring;
    i,l:integer;
begin
    if player[x].pet_hp[0] <= 0 then exit; //already dead
    dec(player[x].pet_hp[0],dmg);
    if player[x].pet_hp[0] <= 0 then player[x].pet_hp[0]:=0;
    //send damage
    s:=#$50#$00#$00#$00#$04#$52#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$C3#$00#$00#$00#$01#$00#$00#$00#$06#$00#$a9#$11
	+#$56#$5F#$9C#$00#$01#$00#$00#$00#$06#$00#$a9#$11#$A3#$08#$69#$63
	+#$31#$00#$00#$00#$B3#$00#$00#$00#$01#$00#$00#$00#$8F#$4F#$4A#$B8
	+#$EF#$D5#$00#$00#$09#$00#$08#$20#$00#$00#$A0#$00#$00#$00#$00#$00;
    safemove(mid[1],s,$21,12);
    safemove(player[x].gid,s,$15,4);
    safemove(player[x].gid,s,$9,4);
    safemove(dmg,s,$31,3);  //damage
    if dmg and $1000000 > 0 then s[$39]:=#2; //critical
    safemove(player[x].pet_hp[0],s,$35,4); //life
    safemove(pos[1],s,$3d,8);
    sendtolobby(player[x].room,-1,s);
    
    if player[x].pet_hp[0] <= 0 then begin
        //stun it
        s:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	      +#$04#$00#$00#$00#$19#$01#$00#$00#$01#$00#$00#$00#$06#$00#$A9#$11
	      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$49#$E6#$02#$00
	      +#$FF#$FF#$FF#$FF#$8D#$3A#$3F#$CB#$00#$00#$00#$00#$04#$00#$00#$00
	      +#$00#$00#$00#$00#$00#$00#$00#$00;
        safemove(player[x].gid,s,9,4);
        safemove(player[x].gid,s,$15,4);
        sendtolobby(player[x].room,-1,s);

        player[x].pet_stunned[0]:=gettickcount;

    end;
end;


procedure EnterSectionMap(x:integer;id:pdword);
var i,l,c:integer;
    s:ansistring;
begin
    if player[x].Floor > 15 then exit;

    if player[x].Floor = -1 then begin
        if player[x].casinogame = CASINO_SHOOTER then begin
            if ShooterRoom[player[x].shooter_status].shooter_status = 0 then begin
                ShooterRoom[player[x].shooter_status].shooter_status:=1;
                ShooterRoom[player[x].shooter_status].shooter_Time:=10000;
                ShooterRoom[player[x].shooter_status].gauge:=0;
            end;
            if ShooterRoom[player[x].shooter_status].shooter_status = 2 then
                player[x].shooterround:=4; //round already started allow 1 more
        end;
        exit;
    end;


    //change pipe pos
    for i:=0 to 63 do
        if team[player[x].TeamID].PipePos[player[x].Floor,i].id = id^ then begin
            s:=#$30#$00#$00#$00#$04#$02#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$AD#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
            +#$00#$00#$A8#$39#$00#$00#$A8#$39#$4A#$D3#$CC#$B8#$83#$54#$00#$00;
            safemove(team[player[x].TeamID].pipepos[player[x].Floor,i].data[0],s,$21,16);
            //find the pipe id
            l:=1;
            while l < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
                move(team[player[x].TeamID].FloorObj[player[x].Floor][l],c,4);
                if pansichar(@team[player[x].TeamID].FloorObj[player[x].Floor][l+$24]) = 'oa_telepipe_clear' then begin
                    safemove(team[player[x].TeamID].FloorObj[player[x].Floor][l+8],s,$15,4);
                    sendtoplayer(x,s);
                    break;
                end;
                inc(l,c);
            end;
        end;

    for i:=0 to 63 do
        if team[player[x].TeamID].MapSection[player[x].Floor,i].id = id^ then begin
            if length(player[x].FloorArea[player[x].Floor])<> 16 then
                player[x].FloorArea[player[x].Floor] :=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0; //prevent crash
            for l:=0 to 15 do
                player[x].FloorArea[player[x].Floor][l+1]:=
                    ansichar(byte(player[x].FloorArea[player[x].Floor][l+1]) or
                    team[player[x].TeamID].MapSection[player[x].Floor,i].data[l]);
                s:=#$34#$00#$00#$00#$0B#$13#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00
                  +#$10#$00#$00#$00#$7B#$36#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
                  +#0#0#0#0#$00#$18#$60#$00#$01#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

                safemove(team[player[x].TeamID].floor_num[player[x].floor],s,$21,4);
                safemove(player[x].partyid,s,$15,4);
                safemove(player[x].FloorArea[player[x].Floor][1],s,$25,$10);
                sendtoplayer(x,s);

                //random effect
                if player[x].Floor > -1 then begin
                    if random(40) = 1 then begin
                        s:=#$80#$00#$00#$00#$17#$00#$04#$00#$A7#$04#$00#$00#$00#$00#$00#$00
                        +#$01#$00#$00#$00#$36#$01#$00#$00#$03#$00#$00#$00#$BF#$5A#$34#$E4
                        +#$FF#$FF#$FF#$FF#$05#$00#$00#$00#$05#$00#$00#$00#$01#$00#$00#$00
                        +#$02#$00#$00#$00#$FF#$FF#$FF#$FF#$01#$0C#$00#$00#$03#$00#$00#$00
                        +#$D6#$77#$00#$00#$65#$66#$66#$5F#$6F#$6D#$65#$6E#$5F#$64#$61#$72
                        +#$6B#$65#$72#$5F#$73#$67#$6E#$5F#$6E#$00#$00#$00#$D5#$77#$00#$00
                        +#$30#$38#$5F#$71#$75#$65#$73#$74#$5F#$64#$61#$72#$6B#$65#$72#$5F
                        +#$61#$75#$72#$61#$00#$00#$00#$00#$F9#$77#$00#$00#$00#$00#$00#$00;
                        if random(2) = 1 then
                        s:=#$6C#$00#$00#$00#$17#$00#$04#$00#$08#$09#$00#$00#$00#$00#$00#$00
                        +#$01#$00#$00#$00#$36#$01#$00#$00#$03#$00#$00#$00#$45#$32#$30#$6D
                        +#$FF#$FF#$FF#$FF#$05#$00#$00#$00#$05#$00#$00#$00#$01#$00#$00#$00
                        +#$02#$00#$00#$00#$FF#$FF#$FF#$FF#$01#$0C#$00#$00#$03#$00#$00#$00
                        +#$D5#$77#$00#$00#$65#$66#$66#$5F#$6F#$6D#$65#$6E#$5F#$64#$72#$67
                        +#$5F#$76#$6F#$6C#$5F#$73#$67#$6E#$00#$00#$00#$00#$F9#$77#$00#$00
                        +#$00#$00#$00#$00#$F9#$77#$00#$00#$00#$00#$00#$00;
                        sendtoplayer(x,s);
                    end;
                end;
            break;
        end;
end;

Function RemoveObjFromFloor(p,T,lvl:integer;ID:int64):ansistring;
var i,x:integer;
    d:^int64;
begin
    result:='';
    x:=1;
    //loop trought all id
    while x < length(team[t].FloorObj[lvl]) do begin
        d:=@team[t].FloorObj[lvl][x+8];
        if id = d^ then begin
            //item found get its type
            result:=pansichar(@team[t].FloorObj[lvl][x+$24]);
            //special handling
            if result = 'ob_0210_0154' then begin
                hitHeliDrop(p,t,lvl,id);
                result:='-';
            end;
            if result = 'oa_dwarf_defense' then begin
                //verify 20 hit
                DwarfHit(p,id);
                result:='-';
                exit;
            end;

            move(team[t].FloorObj[lvl][x],i,4);
            //delete it
            delete(team[t].FloorObj[lvl],x,i);
        end else begin
            move(team[t].FloorObj[lvl][x],i,4);
            inc(x,i);
        end;
    end;
    //player floor
    x:=1;
    //loop trought all id
    while x < length(player[p].FloorObj[lvl]) do begin
        d:=@player[p].FloorObj[lvl][x+8];
        if id = d^ then begin
            //item found get its type
            result:=pansichar(@player[p].FloorObj[lvl][x+$24]);
            move(player[p].FloorObj[lvl][x],i,4);
            //delete it
            delete(player[p].FloorObj[lvl],x,i);
        end else begin
            move(player[p].FloorObj[lvl][x],i,4);
            inc(x,i);
        end;
    end;

end;


procedure AddObjectToFloor(T,lvl:integer;obj:ansistring);
begin
    team[t].FloorObj[lvl]:=team[t].FloorObj[lvl]+obj;
    sendtofloor(Room_Team+t,lvl,obj);
end;

procedure AddObjectToPlayerFloor(T,lvl:integer;obj:ansistring);
begin
    player[t].FloorObj[lvl]:=player[t].FloorObj[lvl]+obj;
    sendtoplayer(t,obj);
end;

procedure AddItemToFloor(T,lvl:integer;obj:ansistring);
begin
    player[t].FloorItems[lvl]:=player[t].FloorItems[lvl]+obj;
end;


procedure loadmonsterlist(p,t,lvl:integer;fn,evfn:ansistring);
var sl,sl2:tstringlist;
    x,grp,wave,i,l,ind,c:integer;
    a,b,ev:ansistring;
    onload,tmpts:tstringlist;
    lvlup:array[0..8] of single;
begin
    team[t].MonstCount[lvl]:=0;
    team[t].FloorEventCount[lvl]:=0;
    team[t].FloorInEvent[lvl]:=0;
    team[t].floor_once[lvl].Clear;
    //decimalseparator:='.';
    //for x:=0 to 7 do team[t].ActiveCode[x].used:=0;      //claer all active code in this field

    //fillchar(team[t].GroupInfo[lvl],sizeof(TGroupInfo)*64,0);
    for x:=0 to 63 do begin
        team[t].GroupInfo[lvl,x].id:=0;
        team[t].GroupInfo[lvl,x].active:=0;
        team[t].GroupInfo[lvl,x].alt:=0;
        team[t].GroupInfo[lvl,x].trigger:=0;
        team[t].GroupInfo[lvl,x].linked:=0;
        team[t].GroupInfo[lvl,x].isevent:=0;
        team[t].GroupInfo[lvl,x].doevent:=0;
        team[t].GroupInfo[lvl,x].onClear:='';
    end;

    fillchar(team[t].floor_inter[lvl],sizeof(tinteraction)*16,0) ; //clean the interaction

    grp:=0;
    wave:=0;
    sl:=tstringlist.create;
    sl2:=tstringlist.create;
    sl.LoadFromFile(path+fn);
    if fileexists(path+evfn) then sl2.loadfromfile(path+evfn);
    sl.AddStrings(sl2);
    sl2.Free;
    ind:=0;
    ev:='';
    onload:=tstringlist.create;
    team[t].bosswarpid[lvl]:=0;

    for i:=0 to 15 do begin
        team[t].doorref[lvl,i].owner:=255;
        team[t].doorref[lvl,i].uni:=$ffff;
    end;


    for x:=0 to sl.Count-1 do begin
        //process all entry
        a:=sl[x];
        try

        if ev <> '' then begin
            if a = 'eventend' then ev:='' else
            if ev = 'ontick' then partys[p].onTick.Add(a)
            else if ev = 'onload' then onload.Add(a)
            else begin
                //other custom event
                for i:=0 to 127 do begin
                  if team[t].CallBacks[i].name = ev then begin
                     team[t].CallBacks[i].cmd.Add(a);
                     break;
                  end else if team[t].CallBacks[i].name = '' then begin
                     team[t].CallBacks[i].cmd.Clear;
                     team[t].CallBacks[i].cmd.Add(a);
                     team[t].CallBacks[i].name:=ev;
                     break;
                  end;
                end;
            end;
        end else
        if ind = 1 then begin
            if copy(a,1,6) = 'write ' then begin
                team[t].data[ team[t].datacount].mods[team[t].data[ team[t].datacount].modcount]:=a;
                inc(team[t].data[ team[t].datacount].modcount);
            end else
            if copy(a,1,7) = 'dataend' then begin
                ind:=0;
                i:=team[t].datacount;
                inc(team[t].datacount);
                team[t].data[i].data:=b;
            end else begin
                i:=9;
                l:=0;
                while i < length(a) do begin
                    if a[i] <> ' ' then begin
                        b:=b+ansichar(strtoint('$'+copy(a,i,2)));
                    end else break;
                    inc(i,3);
                    inc(l);
                    if l = 16 then break;
                end;
            end;
        end else
        if ind = 2 then begin
            if copy(a,1,7) = 'dataend' then begin
                ind:=0;
                i:=team[t].datacount;
                inc(team[t].datacount);
                tmpts:=tstringlist.Create;
                tmpts.Text:=b;
                team[t].data[i].data:=GenerateObjData(tmpts,'\quest\',t);
                tmpts.Free;
            end else begin
                b:=b+a+#13#10;
            end;
        end else begin
            if copy(a,1,4) = 'var ' then begin
                if partys[p].varcount > 63 then begin
                    debug(DBG_CRITICAL,'VAR limit reatched in file '+fn);
                end else begin
                    delete(a,1,4);
                    i:=pos(' ',a);
                    partys[p].vars[partys[p].varcount].name:=copy(a,1,i-1);
                    delete(a,1,i);
                    partys[p].vars[partys[p].varcount].val:=strtoint(a);
                    inc(partys[p].varcount);
                end;
            end else

            if copy(a,1,12) = 'message_pod ' then begin
                delete(a,1,12);
                i:=pos(' ',a);
                l:=strtoint(copy(a,1,i-1));
                delete(a,1,i);
                c:=1;
                while c < length(team[t].FloorObj[lvl]) do begin
                    if pdword(@team[t].FloorObj[lvl][c+8])^ = l then
                       if pansichar(@team[t].FloorObj[lvl][c+$24]) = 'oa_msg_pack' then  //only if valid item
                          move(a[1],team[t].FloorObj[lvl][c+$30],length(a));
                    inc(c,pdword(@team[t].FloorObj[lvl][c])^);
                end;

            end else
            if copy(a,1,6) = 'group ' then begin
                delete(a,1,6);
                grp:=strtoint(a);
                //configure new group if needed
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = 0 then begin
                        team[t].GroupInfo[lvl,i].id:=grp;
                        team[t].GroupInfo[lvl,i].active:=1;
                        team[t].GroupInfo[lvl,i].trigger:=0;
                        team[t].GroupInfo[lvl,i].onclear:='';
                    end;
                    if team[t].GroupInfo[lvl,i].id = grp then break;
                end;
            end else
            if copy(a,1,10) = 'alternate ' then begin
                delete(a,1,10);
                //configure new group if needed
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = grp then begin
                        team[t].GroupInfo[lvl,i].alt:=strtoint(a);
                        break;
                    end;
                end;
            end else
            if a = 'group_disabled' then begin
                //configure new group if needed
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = grp then begin
                        team[t].GroupInfo[lvl,i].active:=0;
                        break;
                    end;
                end;
            end else
            if copy(a,1,8) = 'trigger ' then begin
                delete(a,1,8);
                //configure new group if needed
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = grp then begin
                        team[t].GroupInfo[lvl,i].active:=0;
                        team[t].GroupInfo[lvl,i].trigger:=strtoint(a);
                        break;
                    end;
                end;
            end else
            if a = 'group_isevent' then begin
                //configure new group if needed
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = grp then begin
                        team[t].GroupInfo[lvl,i].isevent:=1;
                        break;
                    end;
                end;
            end else
            if a = 'allow_event' then begin
                //configure new group if needed
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = grp then begin
                        team[t].GroupInfo[lvl,i].doevent:=1;
                        break;
                    end;
                end;
            end else
            if copy(a,1,17) = 'set_event_handler' then begin
                delete(a,1,18);
                ev:=a;
                //make sur name doesnt exists
                for i:=0 to 127 do
                  if team[t].callbacks[i].name = ev then ev:=''; //only first one win
            end else
            if copy(a,1,9) = 'function ' then begin
                delete(a,1,9);
                ev:=a;
                //make sur name doesnt exists
                for i:=0 to 127 do
                  if team[t].callbacks[i].name = ev then ev:=''; //only first one win
            end else
            if copy(a,1,4) = 'wave' then begin
                delete(a,1,5);
                wave:=strtoint(a);
            end else
            if copy(a,1,2) = 'id' then begin
                if team[t].MonstCount[lvl] > 127 then begin
                    debug(DBG_CRITICAL,'Monster limit reatched in file '+fn);
                end else begin
                delete(a,1,3);
                team[t].Monst[lvl,team[t].MonstCount[lvl]].ShiftaTick:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].DebandTick:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].ShiftaBoost:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].DebandBoost:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].statustime:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].statuseffect:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].statusefft:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].passive := 0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].properties:='';
                team[t].Monst[lvl,team[t].MonstCount[lvl]].disapear := 0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].id:=strtoint(a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]].wave:=wave;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].group:=grp;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].ctype:='';
                team[t].Monst[lvl,team[t].MonstCount[lvl]].eventcount:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].status:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].special:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].target:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].lvl:=1; //default lvl
                team[t].Monst[lvl,team[t].MonstCount[lvl]].diecount:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].hit[0]:=$ff;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].hit[1]:=$ff;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].hit[2]:=$ff;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].hit[3]:=$ff;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].respawn:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].DieTime2:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].DieTime:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].pts:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].rare:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].apear:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].boss:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].miniboss:=0;
                team[t].Monst[lvl,team[t].MonstCount[lvl]].isnestmaster:=0;
                inc(team[t].MonstCount[lvl]);
                end;
            end else
            if copy(a,1,4) = 'type' then begin
                delete(a,1,5);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].ctype:=a;
                //find the hp related
                if pos(' ',a)>0 then begin
                    //many monster, select the first one
                    l:=pos(' ',a);
                    a:=copy(a,1,l-1);
                end;
                for i:=0 to MonsterCount-1 do
                    if monsters[i].mtype = a then begin
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].hp:=monsters[i].life;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].mhp:=monsters[i].life;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].SATK:=monsters[i].SATK;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].RATK:=monsters[i].RATK;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].TATK:=monsters[i].TATK;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].SDEF:=monsters[i].SDEF;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].RDEF:=monsters[i].RDEF;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].TDEF:=monsters[i].TDEF;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].APT:=monsters[i].APT;
                        team[t].Monst[lvl,team[t].MonstCount[lvl]-1].exp:=monsters[i].exp;
                        
                    end;
            end else
            if copy(a,1,5) = 'apear' then begin
                delete(a,1,6);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].apear:=strtoint(a);

            end else
            if a = 'nestmaster' then begin
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].isnestmaster:=1;

            end else
            if copy(a,1,4) = 'area' then begin
                delete(a,1,5);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].area:=strtoint(a);

            end else
            if copy(a,1,4) = 'pts ' then begin
                delete(a,1,4);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].pts:=strtoint(a);

            end else
            if copy(a,1,9) = 'property ' then begin
                delete(a,1,9);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].properties:=a;

            end else
            if copy(a,1,3) = 'lvl' then begin
                delete(a,1,4);
                l:=strtoint(a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].lvl:=l;
                dec(l);
                //load the lvlup base
                for i:=0 to MonsterCount-1 do
                if Monsters[i].mtype = team[t].Monst[lvl,team[t].MonstCount[lvl]-1].ctype then begin
                    move(Monsters[i].lvlup[0], lvlup[0],9*4);
                end;

                //apply lvl bonus
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].hp,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].hp *lvlup[0])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].mhp,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].hp*lvlup[0])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].SATK,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].SATK*lvlup[1])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].RATK,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].RATK*lvlup[2])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].TATK,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].TATK*lvlup[3])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].SDEF,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].SDEF*lvlup[4])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].RDEF,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].RDEF*lvlup[5])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].TDEF,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].TDEF*lvlup[6])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].APT,round(l*(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].APT*lvlup[7])));
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].exp, (team[t].Monst[lvl,team[t].MonstCount[lvl]-1].exp * l) div 3);

            end else
            if copy(a,1,8) = 'rotation' then begin
                delete(a,1,9);
                i:=pos(' ',a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].rot[0]:=strtofloat(copy(a,1,i-1));
                delete(a,1,i);
                i:=pos(' ',a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].rot[1]:=strtofloat(copy(a,1,i-1));
                delete(a,1,i);
                i:=pos(' ',a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].rot[2]:=strtofloat(copy(a,1,i-1));
                delete(a,1,i);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].rot[3]:=strtofloat(a);
            end else
            if copy(a,1,8) = 'position' then begin
                delete(a,1,9);
                i:=pos(' ',a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].pos[0]:=strtofloat(copy(a,1,i-1));
                delete(a,1,i);
                i:=pos(' ',a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].pos[1]:=strtofloat(copy(a,1,i-1));
                delete(a,1,i);
                i:=pos(' ',a);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].pos[2]:=strtofloat(copy(a,1,i-1));
                delete(a,1,i);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].pos[3]:=strtofloat(a);
            end else
            if copy(a,1,7) = 'special' then begin
                //delete(a,1,6);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].special:=1;
                //inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].eventcount);
            end else
            if copy(a,1,7) = 'passive' then begin
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].passive:=1;
            end else
            if a = 'boss' then begin
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].boss:=1;
            end else
            if a = 'miniboss' then begin
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].miniboss:=1;
            end else
            if copy(a,1,8) = 'infected' then begin
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].rare:=1;
            end else
            if copy(a,1,7) = 'respawn' then begin
                delete(a,1,8);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].respawn:=strtoint(a);
            end else
            if copy(a,1,9) = 'disapear ' then begin
                delete(a,1,9);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].disapear:=strtoint(a);
            end else
            if copy(a,1,5) = 'event' then begin
                delete(a,1,6);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].events[team[t].Monst[lvl,team[t].MonstCount[lvl]-1].eventcount]:=a;
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].eventcount);
            end else
            if copy(a,1,3) = 'die' then begin
                delete(a,1,4);
                team[t].Monst[lvl,team[t].MonstCount[lvl]-1].dies[team[t].Monst[lvl,team[t].MonstCount[lvl]-1].diecount]:=a;
                inc(team[t].Monst[lvl,team[t].MonstCount[lvl]-1].diecount);
            end else
            if copy(a,1,11) = 'transferdef' then begin
                if team[t].warpcount > 15 then begin
                    debug(DBG_CRITICAL,'Warp limit reatched in file '+fn);
                end else begin
                    delete(a,1,12);
                    //move all of them
                    for i:=team[t].warpcount downto 1 do
                        move(team[t].warps[i-1],team[t].warps[i],sizeof(twarp));
                    inc(team[t].warpcount);
                    team[t].warps[0].id:=0;
                    for i:=0 to 15 do begin
                        team[t].warps[0].pos[i]:=strtoint('$'+copy(a,1,2));
                        delete(a,1,3);
                    end;
                end;
            end else
            if copy(a,1,8) = 'transfer' then begin
                if team[t].warpcount > 15 then begin
                    debug(DBG_CRITICAL,'Warp limit reatched in file '+fn);
                end else begin
                    delete(a,1,9);
                    for i:=1 to length(a) do if a[i]=' ' then break;
                    team[t].warps[team[t].warpcount].id:=strtoint(copy(a,1,i-1));
                    delete(a,1,i);
                    for i:=0 to 15 do begin
                        team[t].warps[team[t].warpcount].pos[i]:=strtoint('$'+copy(a,1,2));
                        delete(a,1,3);
                    end;
                    inc(team[t].warpcount);
                end;
            end else
            if copy(a,1,8) = 'bosswarp' then begin
                delete(a,1,9);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].bosswarpid[lvl]:=strtoint(copy(a,1,i-1));
                delete(a,1,i);
                team[t].bosswarpdata[lvl]:=a;
            end else
            if copy(a,1,9) = 'floorwarp' then begin
                delete(a,1,10);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].bosswarpid[lvl]:=strtoint(copy(a,1,i-1));
                delete(a,1,i);
                team[t].bosswarpdata[lvl]:='door:'+a;
            end else
            if copy(a,1,6) = 'switch' then begin
                if team[t].switchcount > 31 then begin
                    debug(DBG_CRITICAL,'Switch limit reatched in file '+fn);
                end else begin
                    delete(a,1,7);
                    for i:=1 to length(a) do if a[i]=' ' then break;
                    team[t].switchs[team[t].switchcount].id:=strtoint(copy(a,1,i-1));
                    delete(a,1,i);

                    for i:=1 to length(a) do if a[i]=' ' then break;
                    team[t].switchs[team[t].switchcount].cmd:=0;
                    if lowercase(copy(a,1,i-1)) = 'open' then team[t].switchs[team[t].switchcount].cmd:=1;
                    delete(a,1,i);

                    team[t].switchs[team[t].switchcount].target:=strtoint(a);

                    inc(team[t].switchcount);
                end;
            end else
            if copy(a,1,6) = 'doorp1' then begin
                delete(a,1,7);
                team[t].doorref[lvl,0].owner:=strtoint(a);
            end else
            if copy(a,1,6) = 'doorp2' then begin
                delete(a,1,7);
                team[t].doorref[lvl,1].owner:=strtoint(a);
            end else
            if copy(a,1,6) = 'doorp3' then begin
                delete(a,1,7);
                team[t].doorref[lvl,2].owner:=strtoint(a);
            end else
            if copy(a,1,13) = 'floor_number ' then begin
                delete(a,1,13);
                team[t].floor_num[l]:=strtoint(a);
            end else
            if copy(a,1,5) = 'door ' then begin
                delete(a,1,5);
                for i:=1 to length(a) do if a[i]=' ' then break;
                l:=strtoint(copy(a,1,i-1));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].fl:=strtoint(copy(a,1,i-1));

                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[0]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[1]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[2]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[3]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[4]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[5]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].doorref[lvl,l].pos[6]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                team[t].doorref[lvl,l].pos[7]:=FloatToHalf(strtofloat(a));

            end else
            if copy(a,1,9) = 'startpos ' then begin
                delete(a,1,9);
                team[t].floorspawn[0,lvl] :=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;

                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][1])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][3])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][5])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][7])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][9])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][11])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                pword(@team[t].floorspawn[0,lvl][13])^:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                pword(@team[t].floorspawn[0,lvl][15])^:=FloatToHalf(strtofloat(a));

            end else

            if copy(a,1,8) = 'doorpos ' then begin
                delete(a,1,8);

                for i:=1 to length(a) do if a[i]=' ' then break;
                c:=strtoint(copy(a,1,i-1));
                delete(a,1,i);

                for i:=1 to length(a) do if a[i]=' ' then break;
                l:=strtoint(copy(a,1,i-1));
                team[t].door_pos[l].used:=1;
                team[t].doorref[lvl,c].uni:=l;
                delete(a,1,i);

                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[0]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[1]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[2]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[3]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[4]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[5]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posb[6]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                team[t].door_pos[l].posb[7]:=FloatToHalf(strtofloat(a));

            end else

            if copy(a,1,10) = 'doorstart ' then begin
                delete(a,1,10);
                for i:=1 to length(a) do if a[i]=' ' then break;
                c:=strtoint(copy(a,1,i-1));
                delete(a,1,i);

                for i:=1 to length(a) do if a[i]=' ' then break;
                l:=strtoint(copy(a,1,i-1));
                team[t].door_pos[l].used:=1;
                team[t].doorref[lvl,c].uni:=l;
                delete(a,1,i);

                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[0]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[1]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[2]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[3]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[4]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[5]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                for i:=1 to length(a) do if a[i]=' ' then break;
                team[t].door_pos[l].posa[6]:=FloatToHalf(strtofloat(copy(a,1,i-1)));
                delete(a,1,i);
                team[t].door_pos[l].posa[7]:=FloatToHalf(strtofloat(a));

            end else
            if copy(a,1,9) = 'item_data' then begin
                if team[t].datacount > 63 then begin
                    debug(DBG_CRITICAL,'Switch limit reatched in file '+fn);
                end else begin
                    delete(a,1,10);
                    i:=team[t].datacount;
                    //inc(team[t].datacount);
                    b:='';
                    team[t].data[i].name:=a;
                    team[t].data[i].modcount:=1;
                    team[t].data[i].mods[0]:= 'isobject';
                    ind:=2;
                end;
            end else
            if copy(a,1,4) = 'data' then begin
                if team[t].datacount > 63 then begin
                    debug(DBG_CRITICAL,'Switch limit reatched in file '+fn);
                end else begin
                    delete(a,1,5);
                    i:=team[t].datacount;
                    //inc(team[t].datacount);
                    b:='';
                    team[t].data[i].name:=a;
                    team[t].data[i].modcount:=0;
                    ind:=1;
                end;
            end else
            if copy(a,1,11) = 'set_door_p ' then begin
                delete(a,1,11);
                for i:=1 to length(a) do if a[i]=' ' then break;
                l:=strtoint(copy(a,1,i-1));
                delete(a,1,i);
                team[t].door_entry_p[l]:=strtoint(a);
            end else
            if copy(a,1,8) = 'onclear ' then begin
                //on clear group event
                delete(a,1,8);
                for i:=0 to 63 do begin
                    if team[t].GroupInfo[lvl,i].id = grp then begin
                        team[t].GroupInfo[lvl,i].onClear:=a;
                        break;
                    end;
                end;
            end else
            if copy(a,1,12) = 'interaction ' then begin
                //on clear group event
                delete(a,1,12);
                for i:=0 to 15 do begin
                    if team[t].floor_inter[lvl,i].types=0 then begin
                        //free spot found fill it
                        b:=ReadWord(a);
                        if b = 'helicopter_drop' then team[t].floor_inter[lvl,i].types:=1;
                        if b = 'helicopter_support' then team[t].floor_inter[lvl,i].types:=2;
                        team[t].floor_inter[lvl,i].rot.x:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].rot.y:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].rot.z:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].rot.w:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].pos.x:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].pos.y:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].pos.z:=strtofloat(ReadWord(a));
                        team[t].floor_inter[lvl,i].pos.w:=strtofloat(ReadWord(a));
                        break;
                    end;
                end;
            end;



        end;
        except
            raise exception.Create('Error at line '+inttostr(x));
        end;

    end;

    //process onload
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].partyid = p then l:=i;
    for i:=0 to onload.Count-1 do
        ProcessQuestEvent(l,t,onload[i]);

    onload.Free;
    sl.Free;
end;


//this function will spawn the monster that should be apearing by default on the map
procedure spawnMonster(x,t,lvl:integer);
var i,l,c:integer;
    a:ansistring;
begin
    if player[x].questcompleted = 1 then exit; //nothing to spawn
    for i:=0 to team[t].MonstCount[lvl]-1 do
        if (team[t].Monst[lvl,i].wave = 1) and (team[t].Monst[lvl,i].status > 0) then begin
            //wave 1 = ready to display

            //do we have many monster type
            c:=pos(' ',team[t].Monst[lvl,i].atype);
            a:=team[t].Monst[lvl,i].atype;
            if c>0 then begin
                //yes select one
                while (c > 0) do
                    if random(10) >5 then begin
                        delete(a,1,c);
                        while length(a) > 0 do
                          if a[1] = ' ' then delete(a,1,1)
                          else break;
                        c:=pos(' ',a);
                    end else break;
                if c>0 then a:=copy(a,1,c-1);

                for c:=0 to monstercount-1 do
                if Monsters[c].mtype = a then begin
                    //apply lvl
                    team[t].Monst[lvl,i].hp:=round(monsters[c].life*((monsters[c].lvlup[0]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].mhp:=round(monsters[c].life*((monsters[c].lvlup[0]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].SATK:=round(monsters[c].SATK*((monsters[c].lvlup[1]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].RATK:=round(monsters[c].RATK*((monsters[c].lvlup[2]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].TATK:=round(monsters[c].TATK*((monsters[c].lvlup[3]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].SDEF:=round(monsters[c].SDEF*((monsters[c].lvlup[4]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].RDEF:=round(monsters[c].RDEF*((monsters[c].lvlup[5]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].TDEF:=round(monsters[c].TDEF*((monsters[c].lvlup[6]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].APT:=round(monsters[c].APT*((monsters[c].lvlup[7]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].exp:=monsters[c].exp+((monsters[c].exp * team[t].Monst[lvl,i].lvl)div 3);

                    break;
                end;
            end;

            //random rare or flaming rare
            team[t].Monst[lvl,i].rare:=0;
            if random(30) = 1 then begin
                team[t].Monst[lvl,i].hp:=round(team[t].Monst[lvl,i].hp*1.33);
                team[t].Monst[lvl,i].mhp:=round(team[t].Monst[lvl,i].mhp*1.33);
                team[t].Monst[lvl,i].SATK:=round(team[t].Monst[lvl,i].SATK*1.33);
                team[t].Monst[lvl,i].RATK:=round(team[t].Monst[lvl,i].RATK*1.33);
                team[t].Monst[lvl,i].TATK:=round(team[t].Monst[lvl,i].TATK*1.33);
                team[t].Monst[lvl,i].rare:=1;
                if random(4) = 1 then team[t].Monst[lvl,i].rare:=2;
                team[t].Monst[lvl,i].exp:=round(team[t].Monst[lvl,i].exp*1.4);
            end;

            a:='';
            //find the proper packet to send
            for l:=0 to MonsterCount-1 do
                if lowercase(Monsters[l].mtype) = lowercase(team[t].Monst[lvl,i].atype) then begin
                    a:=GenerateMonsterPacket(Monsters[l].packet,team[t].Monst[lvl,i].properties);
                    break;
                end;
            if a <> '' then begin
            //set the position
                for l:=0 to 3 do begin
                    pword(@a[$15+(l*2)])^ := FloatToHalf(team[t].Monst[lvl,i].rot[l]);
                    pword(@a[$1d+(l*2)])^ := FloatToHalf(team[t].Monst[lvl,i].pos[l]);
                end;
                //set the id
                safemove(team[t].Monst[lvl,i].id,a,9,4);
                safemove(team[t].Monst[lvl,i].hp,a,$49,4);
                //set area
                a[$57]:=ansichar(team[t].Monst[lvl,i].area);
                //set lvl
                a[$51]:=ansichar(team[t].Monst[lvl,i].lvl);
                if team[t].Monst[lvl,i].special = 1 then a[$5a]:=#5
                else a[$5a]:=#4;
                sendtoplayer(x,a);
            end;
        end;

end;

procedure UnhidePipe(x:integer);
var c,i,l:integer;
    s:ansistring;
begin
    //find and send the unhide packet for the end pipe
    l:=1;
    if player[x].Floor < 0 then exit;
    while l < length(team[player[x].TeamID].FloorObj[player[x].Floor]) do begin
      move(team[player[x].TeamID].FloorObj[player[x].Floor][l],c,4);
      if pansichar(@team[player[x].TeamID].FloorObj[player[x].Floor][l+$24]) = 'oa_telepipe_clear' then begin
        //found it unhide it
          s:=#$18#$00#$00#$00#$0B#$11#$04#$00#$39#$75#$00#$00#$AE#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
          safemove(team[player[x].TeamID].FloorObj[player[x].Floor][l+8],s,13,4);
          sendtoplayer(x,s);
          break;
      end;
      inc(l,c);
    end;
end;

//this is called to give the reward to all player in the party
procedure DistributeReward(t:integer);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].gid > 0) and (player[i].partyid = t) then begin
            //add mesetas
            inc(player[i].activeChar.Mesetas,team[player[i].TeamID].rewardmst);
            UpdateMesetas(i);

            //add exp
            AddPlayerExp(i,team[player[i].TeamID].rewardexp);

            //make player award of the completed stat
            player[i].questcompleted :=1;

            //tag quest as cleared
            player[i].activeChar.questflag[team[player[i].TeamID].questid]:=player[i].activeChar.questflag[team[player[i].TeamID].questid] or 1;

            //clean enemy for this team
            if player[i].Floor >= 0 then
            for l:=0 to team[player[i].TeamID].monstcount[player[i].Floor]-1 do
            if (team[player[i].TeamID].monst[player[i].Floor,l].status > 0) and
               (team[player[i].TeamID].monst[player[i].Floor,l].status <> 4) then  begin

            s:=#$48#$00#$00#$00#$04#$22#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	            +#$04#$00#$00#$00#$B4#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
	            +#$01#$00#$00#$00#$00#$00#$000#$00#$00#$00#$00#$00#$00#$00#$00#$00
	            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	            +#$00#$00#$00#$00#$00#$00#$00#$00;
            safemove(team[player[i].TeamID].monst[player[i].floor,l].id,s,$15,4);
            safemove(player[i].gid,s,$9,4);
            sendtoplayer(i,s);
            s:=#$20#$00#$00#$00#$04#$31#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$A0#$01#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
            safemove(team[player[i].TeamID].monst[player[i].floor,l].id,s,$15,4);
            safemove(player[i].gid,s,$9,4);
            sendtoplayer(i,s);
            end;

            //send the cleared packet
            s:=#$18#$00#$00#$00#$0B#$26#$00#$00#$A9#$11#$00#$00#$00#$00#$00#$00#$10#$00#$00#$00#$2E#$00#$00#$00;
            sendtoplayer(i,s);
            //make pipe apear
            UnhidePipe(i);

        end;
end;

procedure DistributeEventExp(x,m:integer);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].gid > 0) and (player[i].partyid = player[x].partyid) then begin
            //add mesetas
            AddPlayerExp(i,m);
        end;
end;

procedure DistributeEventMesetas(x,m:integer);
var i,l:integer;
    s:ansistring;
begin
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].gid > 0) and (player[i].partyid = player[x].partyid) then begin
            //add mesetas
            inc(player[i].activeChar.Mesetas,m);
            UpdateMesetas(i);
        end;
end;


function groupeisactive(t,lvl,group:integer):boolean;
var i:integer;
begin
    result:=false;
    for i:=0 to 64 do
        if team[t].GroupInfo[lvl,i].id = group then begin
            if team[t].GroupInfo[lvl,i].active = 1 then result:=true;
            break;
        end;

end;

function linkedgroupisalive(t,lvl,group:integer):boolean;
var i,l:integer;
begin
    result:=false;
    for i:=0 to 63 do
        if team[t].GroupInfo[lvl,i].id = group then begin
            if team[t].GroupInfo[lvl,i].linked > 0 then begin
                for l:=0 to team[t].monstcount[lvl]-1 do
                    if team[t].Monst[lvl,l].group = team[t].GroupInfo[lvl,i].linked then
                       if team[t].Monst[lvl,l].wave > 0 then begin result:=true; break; end;
            end;
            break;
        end;

end;

function PlayerNear(t,l:integer;x,y:single):boolean;
var i:integer;
begin
    result:=false;
    for i:=0 to srvcfg.MaxPlayer-1 do
      if playerok(i) then
        if (player[i].TeamID = t) and (player[i].Floor = l) then
          if sqrt(sqr(HalfPrecisionToFloat(player[i].CurPos.x)-x) + sqr(HalfPrecisionToFloat(player[i].CurPos.z)-y)) < 40 then begin
            result:=true;
            break;
          end;
end;

procedure TestWave(px,py:single;x,t,lvl:integer);
var i,l,c,k:integer;
    r,r2:single;
    s:ansistring;
begin
    //procedd tick event
    i:=0;
    while i < partys[player[x].partyid].onTick.Count do begin
        ProcessQuestEvent(x,player[x].TeamID,partys[player[x].partyid].onTick[i]);
        inc(i);
    end;

    //test if we should spawn any monster
    if (player[x].questcompleted = 0) then
    for i:=0 to team[t].monstcount[lvl]-1 do
        if (team[t].Monst[lvl,i].wave = 1) and (team[t].Monst[lvl,i].status = 0) and groupeisactive(t,lvl,team[t].Monst[lvl,i].group) then begin
            //un released monster test spawn distance
            r:=sqrt(sqr(px-team[t].Monst[lvl,i].pos[0])+sqr(py-team[t].Monst[lvl,i].pos[2]));
            if team[t].Monst[lvl,i].boss = 1 then r:=1;  //dont trigger it, the portal will
            if r< 35 then begin
                spawnMonsterGroup(t,lvl,team[t].Monst[lvl,i].group);
            end;
        end;

    //test if any item should attack
    i:=1;
    while i < length(team[t].FloorObj[lvl]) do begin

        if pansichar(@team[t].FloorObj[lvl][i+$24]) = 'ob_0530_0044' then begin
            //get x and y
            {3 = activate
            4 = agresive mode
            5 = attack lazer 180
            6 attack faster
            7 = sparkle
            9 = fire ball
            b = aspiration
            c = repousse}


            if team[t].FloorObj[lvl][i+$40] <> #255 then begin
                c:=0;
                r:=sqrt(sqr(px-HalfPrecisionToFloat(pword(@team[t].FloorObj[lvl][i+$1c])^))+sqr(py-HalfPrecisionToFloat(pword(@team[t].FloorObj[lvl][i+$20])^)));
                if r < 30 then begin
                    if team[t].FloorObj[lvl][i+$40] <> #5 then c:=5;
                end else if r < 50 then begin
                    if (team[t].FloorObj[lvl][i+$40] <> #5) and (team[t].FloorObj[lvl][i+$40] <> #9) then c:=9;
                end else if r < 55 then begin
                    if team[t].FloorObj[lvl][i+$40] = #3 then c:=4;
                end else if r < 60 then begin
                    if team[t].FloorObj[lvl][i+$40] = #0 then begin
                        c:=3;
                        l:=0;
                        for k:=0 to srvcfg.MaxPlayer-1 do
                            if playerok(k) and (player[k].room = player[x].room) then inc(l);
                        l:=12-l;
                        if l < 2 then l:=2;
                        if random(l) > 3 then begin
                            c:=0;
                            team[t].FloorObj[lvl][i+$40]:=#255;
                        end;
                    end;
                end;

                if c <> 0 then begin
                    team[t].FloorObj[lvl][i+$40]:=ansichar(c);
                    s:=#$24#$00#$00#$00#$04#$79#$40#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
                        +#$04#$00#$00#$00#$B9#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                        +#$03#$00#$00#$00;
                    safemove(player[x].GID,s,9,4);
                    safemove(team[t].FloorObj[lvl][i+8],s,$15,4);
                    s[$21]:=ansichar(c);
                    sendtolobby(player[x].room,-1,s);
                end;
            end;
        end;
        move(team[t].FloorObj[lvl][i],c,4);
        inc(i,c);
    end;


    //test if any monster should attack
    for i:=0 to team[t].monstcount[lvl]-1 do begin
        if (team[t].Monst[lvl,i].wave = 1) then begin
            //active monster
            if team[t].Monst[lvl,i].disapear > 0 then begin
                //if must disapear
                if team[t].Monst[lvl,i].DieTime <> 0 then begin
                    if gettickcount-team[t].Monst[lvl,i].DieTime > team[t].Monst[lvl,i].disapear then begin
                        //ok must make it die
                        team[t].Monst[lvl,i].wave:=0;
                        team[t].Monst[lvl,i].status:=Monster_Die;
                        team[t].Monst[lvl,i].DieTime2:=gettickcount;
                        UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,Monster_Die);
                        testnextwave(t,lvl,team[t].Monst[lvl,i].group);

                        s:=#$48#$00#$00#$00#$04#$22#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                          +#$04#$00#$00#$00#$B4#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                          +#$01#$00#$00#$00#$00#$00#$000#$00#$00#$00#$00#$00#$00#$00#$00#$00
                          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                          +#$00#$00#$00#$00#$00#$00#$00#$00;
                        safemove(team[t].Monst[lvl,i].id,s,$15,4);
                        sendlobbychar2(player[x].room,-1,s);
                        s:=#$20#$00#$00#$00#$04#$31#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                            +#$04#$00#$00#$00#$A0#$01#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
                        safemove(team[t].Monst[lvl,i].id,s,$15,4);
                        sendlobbychar2(player[x].room,-1,s);
                    end;
                end;
            end;
        end;
        if (team[t].Monst[lvl,i].wave = 0) then begin
            //test for rappy
            if team[t].Monst[lvl,i].atype = 'RagRappyA' then begin
                if team[t].Monst[lvl,i].DieTime <> 0 then
                    if gettickcount - team[t].Monst[lvl,i].DieTime > 5000 then begin
                        team[t].Monst[lvl,i].DieTime:=0;
                        team[t].Monst[lvl,i].once:=0;
                        s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                          +#$04#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                          +#$00#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                          +#$EE#$5C#$00#$00#$52#$61#$70#$70#$79#$45#$73#$63#$61#$70#$65#$00;
                        safemove(team[t].Monst[lvl,i].id,s,$15,4);
                        safemove(team[t].Monst[lvl,i].id,s,$25,4);
                        sendlobbychar2(player[x].room,-1,s);

                        s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                          +#$04#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                          +#$00#$00#$00#$00#$83#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                          +#$EB#$5C#$00#$00#$52#$61#$70#$70#$79#$45#$73#$63#$61#$70#$65#$45#$6E#$64#$00#$00;
                        safemove(team[t].Monst[lvl,i].id,s,$15,4);
                        safemove(team[t].Monst[lvl,i].id,s,$25,4);
                        sendlobbychar2(player[x].room,-1,s);
                    end;
            end;

            //respawn code here
            if team[t].Monst[lvl,i].respawn > 0 then begin
                //do any other monster on same group are alive?
                c:=1;
                if groupeisactive(t,lvl,team[t].Monst[lvl,i].group) then
                  c:=0;
                if linkedgroupisalive(t,lvl,team[t].Monst[lvl,i].group) then
                  c:=1;
                for l:=0 to team[t].Monstcount[lvl]-1 do
                    if team[t].Monst[lvl,i].group = team[t].Monst[lvl,l].group then
                        if team[t].Monst[lvl,l].wave>0 then
                          c:=1;
                //if player around no respawn
                //bugged mark as near always...
                //if team[t].Monst[lvl,i].passive = 0 then  //only unpassive enemy
                //    if PlayerNear(t,lvl,team[t].Monst[lvl,i].pos[0],team[t].Monst[lvl,i].pos[2]) then
                //      c := 1;
                if c = 1 then team[t].Monst[lvl,i].DieTime2:=gettickcount()
                else if gettickcount() - team[t].Monst[lvl,i].DieTime2 > team[t].Monst[lvl,i].respawn then begin
                    //reput monster to the wave 1
                    team[t].Monst[lvl,i].wave:=1;
                    team[t].Monst[lvl,i].status:=0; //ready to respawn
                    team[t].Monst[lvl,i].hp:=team[t].Monst[lvl,i].mhp;
                    team[t].Monst[lvl,i].DieTime2:=0;

                    //test if any other in the same group
                    for c:=0 to team[t].monstcount[lvl]-1 do
                    if (c <> i) and (team[t].Monst[lvl,i].group = team[t].Monst[lvl,c].group) and (random(2) = 1) then begin
                        //if yes random respawn
                        team[t].Monst[lvl,c].wave:=1;
                        team[t].Monst[lvl,c].status:=0; //ready to respawn
                        team[t].Monst[lvl,c].hp:=team[t].Monst[lvl,i].mhp;
                        team[t].Monst[lvl,c].DieTime2:=0;
                    end;
                end;
            end;
        end;
        if (team[t].Monst[lvl,i].wave = 1) and (team[t].Monst[lvl,i].status <> 0) and (team[t].Monst[lvl,i].status <> 10)  then begin
            if (team[t].Monst[lvl,i].status <> MONSTER_ATTACK) then begin
                //un released monster test spawn distance
                //is it confuse?
                if (team[t].Monst[lvl,i].statuseffect>=10036) and  (team[t].Monst[lvl,i].statuseffect<=10040) then begin
                    //find the nearest monster alive in the area
                    for l:=0 to team[t].Monstcount[lvl]-1 do
                        if (team[t].Monst[lvl,l].area = team[t].Monst[lvl,i].area) and (team[t].Monst[lvl,l].wave = 1) and
                            (team[t].Monst[lvl,l].status <> 0) and (team[t].Monst[lvl,l].status <> 10) then begin
                                team[t].Monst[lvl,i].target:=team[t].Monst[lvl,l].id;
                                team[t].Monst[lvl,i].status:=Monster_Attack;
                                UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_Attack);
                                if team[t].Monst[lvl,i].eventcount > 0 then begin
                                    //monster should trigger event
                                    CurTargId:=team[t].Monst[lvl,i].id;
                                    for c:=0 to team[t].Monst[lvl,i].eventcount-1 do
                                        ProcessQuestEvent(x,t,team[t].Monst[lvl,i].events[c]);
                                end;
                                break;
                            end;
                end else begin
                    r:=sqrt(sqr(px-team[t].Monst[lvl,i].pos[0])+sqr(py-team[t].Monst[lvl,i].pos[2]));
                    if team[t].Monst[lvl,i].boss = 1 then r:=r * team[t].Monst[lvl,i].bosszone; //need to be closer to trigger it
                    if team[t].Monst[lvl,i].passive = 1 then r:=r*1.5; //reduce the distance for run away animal
                    if r< 60 then begin
                        //in range make it attack
                        team[t].Monst[lvl,i].target:=player[x].GID;
                        team[t].Monst[lvl,i].status:=Monster_Attack;
                        if team[t].Monst[lvl,i].passive = 0 then
                          UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_Attack)
                        else begin
                          UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,3);
                          team[t].Monst[lvl,i].DieTime:=gettickcount;
                        end;


                        if team[t].Monst[lvl,i].eventcount > 0 then begin
                            //monster should trigger event
                            CurTargId:=team[t].Monst[lvl,i].id;
                            for l:=0 to team[t].Monst[lvl,i].eventcount-1 do
                                ProcessQuestEvent(x,t,team[t].Monst[lvl,i].events[l]);
                        end;

                    end;
                end;
            end else begin
                //already attacking;
                if (team[t].Monst[lvl,i].statuseffect>=10036) and  (team[t].Monst[lvl,i].statuseffect<=10040) then begin
                    //find the nearest monster alive in the area
                    for l:=0 to team[t].Monstcount[lvl]-1 do
                        if (team[t].Monst[lvl,l].area = team[t].Monst[lvl,i].area) and (team[t].Monst[lvl,l].wave = 1) and
                            (team[t].Monst[lvl,l].status <> 0) and (team[t].Monst[lvl,l].status <> 10) then begin
                                if team[t].Monst[lvl,i].target <> team[t].Monst[lvl,l].id then begin
                                    team[t].Monst[lvl,i].target:=team[t].Monst[lvl,l].id;
                                    team[t].Monst[lvl,i].status:=Monster_Attack;
                                    UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_Attack);
                                end;
                                break;
                            end;
                end else begin
                    r2:=9999;
                    for l:=0 to srvcfg.MaxPlayer-1 do
                        if playerok(l) then
                        if player[l].GID = team[t].Monst[lvl,i].target then begin
                        //if player dead stop attacking it
                        //if not on same floor stop attacking
                        if (player[l].hp <=0) or //player dead
                            (player[l].Floor <> lvl) or (player[l].questcompleted = 1) then begin

                            team[t].Monst[lvl,i].target:=0;
                             team[t].Monst[lvl,i].status:=MONSTER_MOVE;
                             UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_MOVE);
                        end else r2:=sqrt(sqr(player[l].CurPos.x-team[t].Monst[lvl,i].pos[0])
                            +sqr(player[l].CurPos.y-team[t].Monst[lvl,i].pos[2]));
                    end;

                    if team[t].Monst[lvl,i].target = player[x].GID then begin
                         //only if the target is moving
                         r:=sqrt(sqr(px-team[t].Monst[lvl,i].pos[0])+sqr(py-team[t].Monst[lvl,i].pos[2]));
                         if team[t].Monst[lvl,i].boss = 1 then r:=r / 1.5; //goes fither
                         if team[t].Monst[lvl,i].miniboss = 1 then r:=0; //cant lose focus
                         if r>80 then begin
                             //clear target if too far
                             team[t].Monst[lvl,i].target:=0;
                             team[t].Monst[lvl,i].status:=MONSTER_MOVE;
                             UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_MOVE);
                         end;
                    end else begin
                        //are we closer to the other target?
                        if (player[x].questcompleted = 0) then begin
                            r:=sqrt(sqr(px-team[t].Monst[lvl,i].pos[0])+sqr(py-team[t].Monst[lvl,i].pos[2]));
                            if (r2 < 80) and (r<r2) then begin
                                team[t].Monst[lvl,i].target:=player[x].GID;
                                 team[t].Monst[lvl,i].status:=MONSTER_Attack;
                                 UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_Attack);
                            end;
                        end;
                    end;
                end;
            end;
        end;
    end;


end;

procedure MakeMonsterTarget(x:integer;s:ansistring);
var t,lvl,i,l,id:integer;
    a:ansistring;
begin
    t:=player[x].TeamID;
    lvl:=player[x].Floor;
    move(s[$29],id,4);

    s:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$00#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
	+#$00#$00#$00#$00#$F9#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$E9#$5C#$00#$00#$54#$72#$61#$6E#$73#$66#$65#$72#$43#$6F#$6D#$70
	+#$6C#$65#$74#$65#$00#$00#$00#$00;

    safemove(player[x].gid,s,$9,4);
    safemove(player[x].gid,s,$15,4);
    safemove(team[player[x].TeamID].bosswarpid[player[x].Floor],s,$15,4);
    SendtoLobby(player[x].room,-1,s);

    for i:=0 to team[t].monstcount[lvl]-1 do
        if team[t].Monst[lvl,i].boss = 1 then begin
            team[t].Monst[lvl,i].bosszone:=0.6;
            {team[t].Monst[lvl,i].target:=player[x].GID;
            team[t].Monst[lvl,i].status:=Monster_Attack;
            UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_Attack);

            if team[t].Monst[lvl,i].eventcount > 0 then begin
                //monster should trigger event
                for l:=0 to team[t].Monst[lvl,i].eventcount-1 do
                    ProcessQuestEvent(x,t,team[t].Monst[lvl,i].events[l]);
            end;
            }
        end;
end;

function GroupShouldBeEvent(t,lvl,g:integer):boolean;
var i:integer;
begin
    result:=false;
    if team[t].FloorInEvent[lvl] = 1 then exit;
    for i:=0 to 63 do
        if team[t].GroupInfo[lvl,i].id = g then begin
            team[t].GroupInfo[lvl,i].linked:=0; //clear it
            if team[t].GroupInfo[lvl,i].doevent = 1 then
                if random(15 * (1 shl team[t].FloorEventCount[lvl])) = 1 then begin
                    result:=true;
                    team[t].FloorInEvent[lvl]:=1;
                    inc(team[t].FloorEventCount[lvl]);
                end;
        end;
end;

function TestForAltGroup(t,lvl,g:integer):integer;
var i:integer;
begin
    result:=g;
    for i:=0 to 63 do
        if team[t].GroupInfo[lvl,i].id = g then
            if team[t].GroupInfo[lvl,i].alt > 0 then
            if random(2)=1 then g:=team[t].GroupInfo[lvl,i].alt;

end;

function LinkEventGroup(t,lvl,g:integer):integer;
var i,c,l,pw,cw:integer;
begin
    //this function will find a group to spawn instead and put it as its linked one
    //it also return the group to procede and remap the monsters
    result:=g;

    //find an event
    c:=0;
    for i:=0 to 63 do
        if team[t].GroupInfo[lvl,i].id > 0 then
            if team[t].GroupInfo[lvl,i].isevent = 1 then inc(c);
    if c = 0 then exit; //no event

    c:=random(c);
    for i:=0 to 63 do
        if team[t].GroupInfo[lvl,i].id > 0 then
            if team[t].GroupInfo[lvl,i].isevent = 1 then begin
                if c = 0 then begin result:=team[t].GroupInfo[lvl,i].id; team[t].GroupInfo[lvl,i].id := 0; break; end;
                dec(c);
            end;


    i:=0;
    pw:=1;
    cw:=1;
    for c:=0 to team[t].monstcount[lvl]-1 do
        if team[t].Monst[lvl,c].group = result then begin
            //monster is in new group, find his position
            while team[t].Monst[lvl,i].group <> g do begin
                inc(i);
                if i >= team[t].monstcount[lvl] then i:=0;
            end;
            //copy the info
            //generate wave
            if team[t].monst[lvl,i].wave <> pw then begin
                pw:=team[t].monst[lvl,i].wave;
                inc(cw);
            end;
            //team[t].Monst[lvl,c].wave:=cw;
            team[t].Monst[lvl,c].area:=team[t].monst[lvl,i].area;
            team[t].Monst[lvl,c].rot:=team[t].monst[lvl,i].rot;
            team[t].Monst[lvl,c].pos:=team[t].monst[lvl,i].pos;
            team[t].Monst[lvl,c].apear:=team[t].monst[lvl,i].apear;

            inc(i);
            if i >= team[t].monstcount[lvl] then i:=0;
        end;

end;

procedure spawnMonsterGroup(t,lvl,g:integer);
var i,l,c:integer;
    a:ansistring;
    te:single;
begin
    //test if the group has to be pointed to some other grou
    //mabe the group can have an event random it
    //form1.Memo1.Lines.Add(inttostr(g));
    if GroupShouldBeEvent(t,lvl,g) then begin

        g:=LinkEventGroup(t,lvl,g);
    end else begin
        g:=TestForAltGroup(t,lvl,g);
    end;

    for i:=0 to team[t].MonstCount[lvl]-1 do
        if (team[t].Monst[lvl,i].wave = 1) and (team[t].Monst[lvl,i].group = g) then begin
            //wave 1 = ready to display

            //do we have many monster type
            c:=pos(' ',team[t].Monst[lvl,i].ctype);
            a:=team[t].Monst[lvl,i].ctype;
            if c>0 then begin
                //yes select one
                while (c > 0) do
                    if random(10) >5 then begin
                        delete(a,1,c);
                        while length(a) > 0 do
                          if a[1] = ' ' then delete(a,1,1)
                          else break;
                        c:=pos(' ',a);
                    end else break;
                if c>0 then a:=copy(a,1,c-1);

                for c:=0 to monstercount-1 do
                if Monsters[c].mtype = a then begin
                    //apply lvl
                    team[t].Monst[lvl,i].hp:=round(monsters[c].life*((monsters[c].lvlup[0]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].mhp:=round(monsters[c].life*((monsters[c].lvlup[0]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].SATK:=round(monsters[c].SATK*((monsters[c].lvlup[1]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].RATK:=round(monsters[c].RATK*((monsters[c].lvlup[2]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].TATK:=round(monsters[c].TATK*((monsters[c].lvlup[3]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].SDEF:=round(monsters[c].SDEF*((monsters[c].lvlup[4]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].RDEF:=round(monsters[c].RDEF*((monsters[c].lvlup[5]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].TDEF:=round(monsters[c].TDEF*((monsters[c].lvlup[6]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].APT:=round(monsters[c].APT*((monsters[c].lvlup[7]*team[t].Monst[lvl,i].lvl)+1));
                    team[t].Monst[lvl,i].exp:=monsters[c].exp+((monsters[c].exp * team[t].Monst[lvl,i].lvl)div 3);

                    break;
                end;
            end;

            //random rare or flaming rare
            team[t].Monst[lvl,i].rare:=0;
            if random(30) = 1 then begin
                team[t].Monst[lvl,i].hp:=round(team[t].Monst[lvl,i].hp*1.33);
                team[t].Monst[lvl,i].mhp:=round(team[t].Monst[lvl,i].mhp*1.33);
                team[t].Monst[lvl,i].SATK:=round(team[t].Monst[lvl,i].SATK*1.33);
                team[t].Monst[lvl,i].RATK:=round(team[t].Monst[lvl,i].RATK*1.33);
                team[t].Monst[lvl,i].TATK:=round(team[t].Monst[lvl,i].TATK*1.33);
                team[t].Monst[lvl,i].rare:=1;
                if random(4) = 1 then team[t].Monst[lvl,i].rare:=2;
                team[t].Monst[lvl,i].exp:=round(team[t].Monst[lvl,i].exp*1.4);
            end;
            team[t].Monst[lvl,i].atype:=a;
            team[t].Monst[lvl,i].statuseffect:=0;
            //find the proper packet to send
            for l:=0 to MonsterCount-1 do
                if lowercase(Monsters[l].mtype) = lowercase(a) then begin
                    a:=GenerateMonsterPacket(Monsters[l].packet,team[t].Monst[lvl,i].properties);
                    break;
                end;

            if length(a) < $20 then begin
                //anti crash
                debug(DBG_ERROR,'Unknown monster: '+a);
                a:=Monsters[0].packet;
            end;

            //set the position
            for l:=0 to 3 do begin
                pword(@a[$15+(l*2)])^ := FloatToHalf(team[t].Monst[lvl,i].rot[l]);
                pword(@a[$1d+(l*2)])^ := FloatToHalf(team[t].Monst[lvl,i].pos[l]);
                te:=HalfPrecisionToFloat(pword(@a[$1d+(l*2)])^);
                if te > 1 then te:=te;
            end;
            //set the id
            move(team[t].Monst[lvl,i].id,a[9],4);
            move(team[t].Monst[lvl,i].apear,a[$55],2);
            //set area
            a[$57]:=ansichar(team[t].Monst[lvl,i].area);
            //set lvl
            a[$51]:=ansichar(team[t].Monst[lvl,i].lvl);
            //hp
            move(team[t].Monst[lvl,i].hp,a[$49],4);
            //special or not?
            if team[t].Monst[lvl,i].special = 1 then a[$5a]:=#5
            else a[$5a]:=#4;
            a[$8d]:=ansichar(team[t].Monst[lvl,i].rare);
            move(a[$85],te,4);
            te:=te + ((random(5)-2)*0.05);
            if team[t].Monst[lvl,i].rare =1 then te:=te+0.4;
            team[t].Monst[lvl,i].boostflg:=0;
            if team[t].Monst[lvl,i].status = 0 then
                team[t].Monst[lvl,i].status:=10; //test
            if team[t].Monst[lvl,i].boss = 0 then move(te,a[$85],4);
            sendtofloor(room_team+t,lvl,a,0);
            if team[t].Monst[lvl,i].rare = 1 then begin
                a:=#$48#$00#$00#$00#$04#$24#$40#$00#$0#$0#$0#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$93#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$40#$0F#$03#$00
                +#$FF#$FF#$FF#$FF#$3D#$8D#$A1#$CA#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00;
                safemove(team[t].Monst[lvl,i].id,a,$15,4);
                sendtofloor(room_team+t,lvl,a,0);
            end;
            if team[t].Monst[lvl,i].rare = 2 then begin
                a:=#$48#$00#$00#$00#$04#$24#$40#$00#$0#$0#$0#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$93#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$42#$0F#$03#$00
                +#$FF#$FF#$FF#$FF#$09#$10#$f3#$CA#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00;
                safemove(team[t].Monst[lvl,i].id,a,$15,4);
                sendtofloor(room_team+t,lvl,a,0);
            end;
        end;

end;

procedure UpdateMonsterStatus(tid,lvl,entry,value:integer);
var s:ansistring;
begin
    s:=#$48#$00#$00#$00#$04#$22#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00#$B4#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
	+#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00#$00#$00#$00#$00;
    safemove(team[tid].monst[lvl,entry].id,s,$15,4);
    safemove(team[tid].monst[lvl,entry].owner,s,$9,4);
    safemove(team[tid].monst[lvl,entry].target,s,$25,4);
    if team[tid].monst[lvl,entry].target = 0 then s[$2d]:=#0;
    if (team[tid].monst[lvl,entry].statuseffect >= 10036) and (team[tid].monst[lvl,entry].statuseffect <= 10040) then
        safemove(s[$1d],s,$2d,4);
    safemove(value,s,$21,4);
    sendtofloor(Room_Team+tid,lvl,s,0);
end;


Procedure triggerMonster(x:integer;s:ansistring);
var id:^dword;
    i:integer;
begin
    if player[x].Floor < 0 then exit;
    id:=@s[$15];
    for i:=0 to team[player[x].TeamID].monstcount[player[x].Floor]-1 do
        if team[player[x].TeamID].monst[player[x].Floor,i].id = id^ then begin
            //id found make sure its alive
            if team[player[x].TeamID].monst[player[x].Floor,i].wave <> 1 then exit;
            if (team[player[x].TeamID].monst[player[x].Floor,i].status = 0)
                or (team[player[x].TeamID].monst[player[x].Floor,i].status = 10) then begin
                //send create and move
                if team[player[x].TeamID].monst[player[x].Floor,i].boss = 0 then UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_CREATE);
                if team[player[x].TeamID].monst[player[x].Floor,i].boss = 0 then UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,MONSTER_APEAR);
                team[player[x].TeamID].monst[player[x].Floor,i].status:=MONSTER_MOVE;
                team[player[x].TeamID].monst[player[x].Floor,i].owner:=player[x].gid;
            end;
            if team[player[x].TeamID].monst[player[x].Floor,i].boss = 0 then
                UpdateMonsterStatus(player[x].TeamID,player[x].Floor,i,team[player[x].TeamID].monst[player[x].Floor,i].status);
        end;

end;

procedure SendTriggerItem(n:TTriggerObj;t,l:integer);
var st:tstringlist;
    s:ansistring;
    id:integer;
begin
    st:=tstringlist.Create;
    id:=GetNextTeamItemID(t);
    if copy(n.name,1,13) = 'oa_wave_laser' then begin
        id:=n.id; //prevent it to reapear
        st.Add('ver 4');
        st.Add('define OPT_FLOAT_1 = 1');
        st.Add('define OPT_FLOAT_0 = 0');
        st.Add('define OPT_INT_52 = 52');
        st.Add('define OPT_INT_49 = 49');
        st.Add('define OPT_INT_50 = 50');
        st.Add('define OPT_INT_48 = 48');
        st.Add('define OPT_INT_51 = 51');

        st.Add('object oa_wave_laser');
        st.Add('id '+inttostr(id));
        st.Add('rotation 0,000 0,000 0,000 1,000');
        st.Add('position 37,906 0,170 -34,406 0,000');
        st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 00 00 00');
        st.Add('byte_flag 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 90 00 C0 33 68');
        st.Add('OPT_INT_51 -1');
        st.Add('OPT_INT_48 1');
        st.Add('OPT_INT_50 2');
        st.Add('OPT_INT_49 0');
        st.Add('OPT_INT_52 13');
        st.Add('OPT_FLOAT_0 0');
        st.Add('OPT_FLOAT_1 0');
    end;
    if n.name = 'oa_blazing_colum_set' then begin
        st.Add('ver 4');
        st.Add('define OPT_FLOAT_1 = 1');
        st.Add('define OPT_FLOAT_0 = 0');
        st.Add('define OPT_FLOAT_58 = 58');
        st.Add('define OPT_INT_50 = 50');
        st.Add('define OPT_INT_48 = 48');
        st.Add('define OPT_INT_51 = 51');

        st.Add('object oa_blazing_colum_set');
        st.Add('id '+inttostr(id));
        st.Add('rotation 0,000 0,729 0,000 0,683');
        st.Add('position 136,750 15,664 49,000 0,000');
        st.Add('item_flag 1C 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 00 00 00');
        st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 00 90 00 00 29 00');
        st.Add('OPT_INT_51 -1');
        st.Add('OPT_INT_48 1');
        st.Add('OPT_INT_50 2');
        st.Add('OPT_FLOAT_58 35,780029296875');
        st.Add('OPT_FLOAT_0 5');
        st.Add('OPT_FLOAT_1 5');
    end;
    if n.name='oa_stalactite' then begin
        st.Add('ver 4');
        st.Add('define OPT_FLOAT_1 = 1');
        st.Add('define OPT_FLOAT_0 = 0');
        st.Add('define OPT_INT_52 = 52');
        st.Add('define OPT_INT_49 = 49');
        st.Add('define OPT_INT_50 = 50');
        st.Add('define OPT_INT_48 = 48');
        st.Add('define OPT_INT_51 = 51');
        st.Add('define OPT_FLOAT_58 = 58');

        st.Add('object oa_stalactite_sign');
        st.Add('id '+inttostr(id));
        st.Add('rotation 0,000 0,000 0,000 1,000');
        st.Add('position -146,750 -0,136 -66,938 0,000');
        st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
        st.Add('byte_flag 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 90 00 00 00 00');
        st.Add('OPT_INT_51 -1');
        st.Add('OPT_INT_48 1');
        st.Add('OPT_INT_50 0');
        st.Add('OPT_INT_49 0');
        st.Add('OPT_INT_52 11');
        st.Add('OPT_FLOAT_0 0');
        st.Add('OPT_FLOAT_1 0');

        id:=GetNextTeamItemID(t);
        st.Add('object oa_stalactite');
        st.Add('id '+inttostr(id));
        st.Add('rotation 0,000 0,000 0,000 1,000');
        st.Add(format('position %.3f %.3f %.3f 0,000',[HalfPrecisionToFloat(n.pos[4]),HalfPrecisionToFloat(n.pos[5])
                ,HalfPrecisionToFloat(n.pos[6]) ]));
        st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 00 00 00');
        st.Add('byte_flag 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 90 00 00 00 00');
        st.Add('OPT_INT_51 -1');
        st.Add('OPT_INT_48 1');
        st.Add('OPT_INT_50 2');
        st.Add('OPT_INT_49 0');
        st.Add('OPT_INT_52 11');
        st.Add('OPT_FLOAT_58 0');
        st.Add('OPT_FLOAT_0 0');
        st.Add('OPT_FLOAT_1 0');

        id:=GetNextTeamItemID(t);
        st.Add('object oa_stalactite');
        st.Add('id '+inttostr(id));
        st.Add('rotation 0,000 0,000 0,000 1,000');
        st.Add(format('position %.3f %.3f %.3f 0,000',[HalfPrecisionToFloat(n.pos[4])-4,HalfPrecisionToFloat(n.pos[5])
                ,HalfPrecisionToFloat(n.pos[6])-4 ]));
        st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 00 00 00');
        st.Add('byte_flag 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 90 00 00 00 00');
        st.Add('OPT_INT_51 -1');
        st.Add('OPT_INT_48 1');
        st.Add('OPT_INT_50 2');
        st.Add('OPT_INT_49 0');
        st.Add('OPT_INT_52 11');
        st.Add('OPT_FLOAT_58 0');
        st.Add('OPT_FLOAT_0 0');
        st.Add('OPT_FLOAT_1 0');

        id:=GetNextTeamItemID(t);
        st.Add('object oa_stalactite');
        st.Add('id '+inttostr(id));
        st.Add('rotation 0,000 0,000 0,000 1,000');
        st.Add(format('position %.3f %.3f %.3f 0,000',[HalfPrecisionToFloat(n.pos[4]),HalfPrecisionToFloat(n.pos[5])
                ,HalfPrecisionToFloat(n.pos[6])-2 ]));
        st.Add('item_flag 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 05 00 00 00');
        st.Add('byte_flag 03 00 00 00 00 00 00 00 00 00 00 00 00 00 00 90 00 00 00 00');
        st.Add('OPT_INT_51 -1');
        st.Add('OPT_INT_48 1');
        st.Add('OPT_INT_50 2');
        st.Add('OPT_INT_49 0');
        st.Add('OPT_INT_52 11');
        st.Add('OPT_FLOAT_58 0');
        st.Add('OPT_FLOAT_0 0');
        st.Add('OPT_FLOAT_1 0');
    end;
    s:=GenerateObjData(st,'\quest\',-1,-1);
    //move(n.id,s[9],4);
    safemove(n.pos[0],s,$15,16);
    sendtofloor(room_team+t,l,s);

    if n.name = 'oa_wave_laser' then begin  //break it
      s:=#$24#$00#$00#$00#$04#$79#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$0A#$03#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
      +#$02#$00#$00#$00;
      safemove(id,s,$15,4);
      sendtofloor(room_team+t,l,s);
    end;

    st.Free;
end;


procedure TriggerObject(x:integer;s:ansistring);
var itm,from,tag:dword;
    a:ansistring;
    i,l,t:integer;
begin
    move(s[$15],itm,4);
    move(s[$21],from,4);
    move(s[$2d],tag,4);
    //handle special code for lobby
    if player[x].room = Room_Casino then begin
        if itm = $43a then begin
            //send the unlock text
            {a:=#$5C#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$3A#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
              +#$00#$00#$00#$00#$3A#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$E5#$5C#$00#$00#$53#$74#$61#$72#$74#$53#$6B#$69#$74#$28#$63#$6F
              +#$5F#$30#$33#$31#$33#$39#$36#$29#$00#$00#$00#$00; }//this make apear the how to
            a:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$2B#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
              +#$00#$00#$00#$00#$2B#$04#$00#$00#$00#$00#$00#$00#$06#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$EA#$5C#$00#$00#$41#$63#$63#$65#$73#$73#$53#$68#$6F#$70#$28#$31
              +#$2C#$32#$29#$00;        //here we just allow the shop
            safemove(player[x].gid,a,9,4);
            sendtoplayer(x,a);
        end;
    end;

    //should code something to handle in quest
    if (player[x].room >= Room_Team) and (player[x].Floor>=0) then begin

        //should we trigger a monster group
        t:=player[x].TeamID;
        l:=player[x].Floor;
        for i:=0 to 63 do begin
            if team[t].GroupInfo[l,i].id <> 0 then begin
                if team[t].GroupInfo[l,i].trigger = itm then begin
                    team[t].GroupInfo[l,i].trigger:=0; //disable for next time
                    spawnMonsterGroup(t,l,team[t].GroupInfo[l,i].id);
                end;
            end;
        end;

        //item trigger?
        for i:=0 to 63 do
          if team[t].ObjTrigger[l,i].id = itm then begin
              if team[t].ObjTrigger[l,i].action = 1 then SendTriggerItem(team[t].ObjTrigger[l,i],t,l); //send item
          end;

    end;

end;



//procedure TestPartyWarp(p:integer);
procedure WarpParty(p:integer);
var i,c,l,x:integer;
    s,a:ansistring;

begin

        //warp them baby!

        partys[p].bosswarp:=0;
        x:=-1;
        for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].partyid = p) then
        if player[i].inboss = 1 then begin
            if x = -1 then x:=i;
            for l:=0 to team[player[x].TeamID].warpcount-1 do
                if team[player[i].TeamID].warps[l].id = team[player[i].TeamID].bosswarpid[player[i].Floor] then begin
                    s:=#$30#$00#$00#$00#$04#$02#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$13#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                    +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
                    safemove(team[player[i].TeamID].warps[l].pos[0],s,$21,16);
                    safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
                    sendtoplayer(i,s);
                end;


            s:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$13#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
              +#$00#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$D0#$5C#$00#$00#$46#$6F#$72#$77#$61#$72#$64#$65#$64#$00#$00#$00;

            safemove(player[i].gid,s,$25,4);
            safemove(player[i].gid,s,$9,4);
            safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
            SendtoLobby(player[i].room,-1,s);



            s:=#$54#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$10#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$EB#$5C#$00#$00#$4F#$62#$6A#$65#$63#$74#$54#$72#$61#$6E#$73#$66
            +#$65#$72#$00#$00;

            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$15,4);
            safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$25,4);
            SendtoLobby(player[i].room,-1,s);
        end else if player[i].inboss = 2 then begin
            //special quest transporter
             if x = -1 then x:=i;
            sendtoparty(i,#$0C#$00#$00#$00#$0E#$51#$00#$00#$01#$00#$00#$00);

            s:=#$58#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$E9#$5C#$00#$00#$54#$72#$61#$6E#$73#$66#$65#$72#$43#$6F#$6D#$70
            +#$6C#$65#$74#$65#$00#$00#$00#$00;
            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$25,4);
            safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
            SendtoLobby(player[i].room,-1,s);

            for l:=0 to team[player[x].TeamID].warpcount-1 do
                if team[player[x].TeamID].warps[l].id = team[player[i].TeamID].bosswarpid[player[i].Floor] then begin
                    s:=#$30#$00#$00#$00#$04#$02#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$13#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                    +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
                    safemove(team[player[i].TeamID].warps[l].pos[0],s,$21,16);
                    safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
                    sendtoplayer(i,s);
                end;


            s:=#$50#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$D0#$5C#$00#$00#$46#$6F#$72#$77#$61#$72#$64#$65#$64#$00#$00#$00;
            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$25,4);
            safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
            SendtoLobby(player[i].room,-1,s);

            s:=#$54#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$EB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$EB#$5C#$00#$00#$4F#$62#$6A#$65#$63#$74#$54#$72#$61#$6E#$73#$66
            +#$65#$72#$00#$00;
            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$15,4);
            c:=team[player[i].TeamID].bosswarpid[player[i].Floor]+1;
            safemove(c,s,$25,4);
            SendtoLobby(player[i].room,-1,s);

        end else if player[i].inboss = 3 then begin
            if x = -1 then x:=i;
            sendtoparty(i,#$0C#$00#$00#$00#$0E#$51#$00#$00#$01#$00#$00#$00);

            s:=#$58#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$E9#$5C#$00#$00#$54#$72#$61#$6E#$73#$66#$65#$72#$43#$6F#$6D#$70
            +#$6C#$65#$74#$65#$00#$00#$00#$00;
            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$25,4);
            safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
            SendtoLobby(player[i].room,-1,s);

            s:=#$50#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$EA#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$D0#$5C#$00#$00#$46#$6F#$72#$77#$61#$72#$64#$65#$64#$00#$00#$00;
            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$25,4);
            safemove(team[player[i].TeamID].bosswarpid[player[i].Floor],s,$15,4);
            SendtoLobby(player[i].room,-1,s);

            s:=#$54#$00#$00#$00#$04#$15#$44#$00#$7C#$79#$9B#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$7C#$79#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00#$EB#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$EB#$5C#$00#$00#$4F#$62#$6A#$65#$63#$74#$54#$72#$61#$6E#$73#$66
            +#$65#$72#$00#$00;
            safemove(player[i].gid,s,$9,4);
            safemove(player[i].gid,s,$15,4);
            c:=team[player[i].TeamID].bosswarpid[player[i].Floor]+1;
            safemove(c,s,$25,4);
            SendtoLobby(player[i].room,-1,s);

            s:=team[player[i].TeamID].bosswarpdata[player[i].Floor];
            delete(s,1,5);
            s:=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
              +#0#0#0#0#0#0#0#0+ansichar(strtoint(s))+#0#0#0#0#0#0#0#0#0;
            changefloor(i,s);

        end;


        for l:=0 to 127 do
            if team[player[x].TeamID].CallBacks[l].name = team[player[x].TeamID].bosswarpdata[player[x].Floor] then begin
              for c:=0 to team[player[x].TeamID].CallBacks[l].cmd.Count-1 do
                ProcessQuestEvent(x,player[x].TeamID,team[player[x].TeamID].CallBacks[l].cmd[c]);
            end;
        for l:=0 to team[player[x].TeamID].monstcount[player[x].Floor]-1 do
        if team[player[x].TeamID].monst[player[x].Floor,l].boss = 1 then begin
            //reset boss hp
            team[player[x].TeamID].monst[player[x].Floor,l].hp:=team[player[x].TeamID].monst[player[x].Floor,l].mhp;
            //unleash it
            spawnMonsterGroup(player[x].TeamID,player[x].Floor,team[player[x].TeamID].monst[player[x].Floor,l].group);
            team[player[x].TeamID].monst[player[x].Floor,l].bosszone:=2;
        end;

    //end;
end;


procedure ReloadFloor(x:integer);
var id,i,lvl:integer;
begin
    id:= team[player[x].TeamID].questid;
    i:=0;
    lvl:=player[x].Floor;
    setquestid(x,@id,@i);
    changefloor(x,#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0,lvl);
end;


end.
