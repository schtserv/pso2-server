ID: 20010
Floor: 5
folder: Exploration

//reward
exp: 162
mesetas: 258

weather Fine
weather Cloudy
weather HeavyRain
weather ThunderyRain


//multi team setting
team1 3
team2 4
team3 5
splitfloor 2

//set floor 2 to have 2 layout
floorvar 0 2
floorvar 1 3

//icon
time 3
team 2

dificulty 1
req_lvl 1
monst_lvl 9
item1 01 00 10 00 00 00 D3 00
item2 01 00 0C 00 00 00 56 00
monst1 172014870 2
monst2 704551092 2
monst3 492573407 2

dificulty 2
req_lvl 20
monst_lvl 31
item1 01 00 0D 00 00 00 26 01
item2 01 00 0B 00 00 00 27 00
monst1 172014870 2
monst2 704551092 2
monst3 492573407 2

dificulty 3
req_lvl 40
monst_lvl 41
item1 01 00 03 00 00 00 6A 00
item2 01 00 0C 00 00 00 89 00
monst1 172014870 2
monst2 704551092 2
monst3 492573407 2

dificulty 4
req_lvl 50
monst_lvl 63
item1 01 00 05 00 00 00 FB 00
item2 01 00 0D 00 00 00 2B 01
monst1 172014870 2
monst2 704551092 2
monst3 492573407 2
