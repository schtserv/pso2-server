ID: 120010
Floor: 7
folder: arks
planet: 28

//reward
exp: 0
mesetas: 0

weather Fine

//multi team setting
#team1 2
#team2 2
#team3 2
#splitfloor 0

//icon
time 3
team 2

dificulty 1
req_lvl 1
monst_lvl 1
item1 01 00 04 00 00 00 04 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 7
monst2 4294967295 3
monst3 4294967295 3

dificulty 2
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 7
monst2 4294967295 3
monst3 4294967295 3

dificulty 3
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 7
monst2 4294967295 3
monst3 4294967295 3

dificulty 4
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 7
monst2 4294967295 3
monst3 4294967295 3

dificulty 5
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 0
monst2 4294967295 0
monst3 4294967295 0

dificulty 6
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 0
monst2 4294967295 0
monst3 4294967295 0

dificulty 7
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 0
monst2 4294967295 0
monst3 4294967295 0

dificulty 8
req_lvl 1
monst_lvl 1
item1 00 00 00 00 00 00 00 00
item2 00 00 00 00 00 00 00 00
monst1 4294967295 0
monst2 4294967295 0
monst3 4294967295 0

