unit UTeamFnc;

interface
uses classes,types,sysutils,windows;

type
    TTeamMember = record
      id:dword;
      flag:integer;
      joindate:dword;
      AccName:array[0..31] of widechar;
    end;
    TteamEntry = record
      id,date:dword;
      name:array[0..31] of widechar;
      welcome:array[0..63] of widechar;
      website:array[0..63] of widechar;
      comment:array[0..63] of widechar;
      LeaderName,LeaderAcc:array[0..31] of widechar;
      reserved:array[0..95] of dword;
      members:array[0..199] of TTeamMember;
    end;
    TMasterTeam = record
      count:dword;

    end;


procedure TeamCreate(x:integer;name:widestring);
procedure GetMyTeam(x:integer);
procedure GetTeamInfo(x,t:integer);
procedure SetTeamComment(x:integer;s:ansistring);
procedure SetTeamWelcome(x:integer;s:ansistring);
procedure TeamDisband(x:integer);
procedure RemovePlayerFromTeam(x:integer;s:ansistring);
procedure ChangePlayerRightinTeam(x:integer;s:ansistring);
procedure SendTeamMemberInfo(x:integer);
procedure TeamSomething1(x:integer;s:ansistring);
procedure RetrivePlayerInfo(x:integer;s:ansistring);
procedure RemoveMyselfFromTeam(x:integer);
procedure InvitPlayerToTeam(x:integer;s:ansistring);
procedure SendTeamRequestList(x:integer);
procedure AwnserTeamOffer(x:integer;s:ansistring);

implementation

uses UPlayer, fmain, UPsoUtil;

function LoadTeamData(t:integer; var te:tteamentry):boolean;
var f:integer;
begin
    result:=false;
    if fileexists(path+'team\'+inttostr(t)) then begin
        f:=fileopen(path+'team\'+inttostr(t),$40);
        fillchar(te,sizeof(te),0);
        fileread(f,te,sizeof(te));
        fileclose(f);
        result:=true;
    end;
end;

procedure SaveTeamData(t:integer; var te:tteamentry);
var f:integer;
begin
    f:=filecreate(path+'team\'+inttostr(t));
    filewrite(f,te,sizeof(te));
    fileclose(f);
end;


procedure UpdatePlayerTeamData(x,st:integer; dte:dword; tn:array of widechar);
var s:ansistring;
    i:integer;
begin
    s:=#$D8#$00#$00#$00#$1C#$20#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
        +#$50#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$66#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$01#$04#$01#$00#$00#$00#$00#$00
        +#$FF#$FF#$FF#$FF#$00#$00#$00#$00#$00#$00#$00#$00#$85#$03#$00#$00
        +#$00#$00#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].GID,s[5],4);
    for i:=0 to length(player[x].aname)-1 do
        s[$11+(i*2)]:=player[x].aname[1+i];

    move(player[x].activeChar.base.name[0],s[$55],17*2);
    if player[x].save.TeamID <> 0 then begin
        if st = 1 then st:=2;
        s[$93]:=ansichar(st);
        move(player[x].save.TeamID,s[$91],4);
        move(tn[0],s[$95],36);
        move(dte,s[$b9],4);
    end;
    sendtolobby(player[x].room,-1,s);
end;

procedure TeamCreate(x:integer;name:widestring);
var f:integer;
    i:integer;
    mt:TMasterTeam;
    te:TteamEntry;
    s:ansistring;
    v:dword;
begin
    fillchar(te,sizeof(te),0);
    mt.count:=$1000;
    if fileexists(path+'team\team.dat') then begin
        f:=fileopen(path+'team\team.dat',$40);
        fileread(f,mt,sizeof(mt));
        fileclose(f);
    end;

    te.date:=Round((now() - 25569) * 86400);
    te.members[0].joindate:=te.date;

    te.id:=mt.count;
    move(name[1],te.name[0],length(name)*2);
    te.members[0].id:=player[x].gid;
    te.members[0].flag:=$101; //leader flag

    SaveTeamData(mt.count,te);

    inc(mt.count);
    f:=filecreate(path+'team\team.dat');
    filewrite(f,mt,sizeof(mt));
    fileclose(f);

    player[x].save.teamid:=te.id;
    //confirm creation
    UpdatePlayerTeamData(x,1,te.date, te.name);

    //copy leader to team entry
    for i:=0 to length(player[x].aname)-1 do
        te.LeaderAcc[i]:=widechar(player[x].aname[1+i]);
    for i:=0 to length(player[x].aname)-1 do
        te.members[0].AccName[i]:=widechar(player[x].aname[1+i]);
    move(player[x].activeChar.base.name[0],te.LeaderName[0],18*2);



    s:=#$38#$00#$00#$00#$1C#$01#$04#$00#$01#$00#$00#$00#$B2#$EE#$00#$00 //    4...........��..
    +#$00#$00#$00#$00#$FC#$B0#$00#$00#0#0#0#0#0#0#0#0
    +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
    +#0#0#0#0#0#0#0#0;

    move(name[1],s[$19],length(name)*2);
    move(te.id,s[13],4);
    sendtoplayer(x,s);


end;

procedure TeamDisband(x:integer);
var f:integer;
    i,l,t:integer;
    te:TteamEntry;
    s:ansistring;
    v:dword;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin

        //make sure the player is allowed to do it
        i:=0;
        for f:=0 to 199 do
            if player[x].GID = te.members[f].id then
                if te.members[f].flag and 1 = 1 then i:=1;
        if i = 0 then exit; //cant do it

        //confirm all active player using it
        for l:=0 to srvcfg.MaxPlayer-1 do
          if playerok(l) then
              if player[l].save.teamid = te.id then begin
                    player[l].save.teamid:=0;
                  UpdatePlayerTeamData(l,4,te.date,te.name);
              end;


        //UpdatePlayerTeamData(x,4,te.date, te.name);


        sendtoplayer(x,#$0C#$00#$00#$00#$1C#$03#$00#$00#$01#$00#$00#$00);//     ............


    end;
end;

procedure GetMyTeam(x:integer);
var f:integer;
    te:TteamEntry;
    a,s:ansistring;
    b:widestring;
begin
    if LoadTeamData(player[x].save.teamid,te) then begin

        //prepare the packet

        s:=#$8C#$00#$00#$00#$1C#$47#$04#$00#$00#$00#$00#$00#$46#$43#$00#$00//     �....G......FC..
          +#$53#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     S.p.a.c.e. .P.i.
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;//     r.a.t.e.!.!.!...
        setlength(a,length(te.welcome)*2);
        move(te.name[0],s[$11],length(te.name)*2);
        move(te.welcome[0],a[1],length(te.welcome)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        s:=s+getmagic(length(a) div 2,$43D2,$84)+a;
        b:=te.LeaderName;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(b)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        s:=s+getmagic(length(a) div 2,$43D2,$84)+a;
        b:=te.LeaderAcc;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(b)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        s:=s+getmagic(length(a) div 2,$43D2,$84)+a;
        a:=#0#0#0#0;
        move(te.date,a[1],4);
        s:=s+a; //this is the creation date??

        f:=length(s);
        move(f,s[1],4);
        sendtoplayer(x,s);
    end;
end;

procedure GetTeamInfo(x,t:integer);
var f,c:integer;
    te:TteamEntry;
    a,s:ansistring;
    b:widestring;
begin
    if LoadTeamData(t,te) then begin


        //prepare the packet
        {Xor: C86C  Sub: A7
Xor: C8EC  Sub: 27
000000: 58 01 00 00 1C 05 04 00 01 00 00 00 FA F5 00 00     X...........��..
000010: DB C8 00 00 53 00 70 00 61 00 63 00 65 00 20 00     ��..S.p.a.c.e. .
000020: 50 00 69 00 72 00 61 00 74 00 65 00 21 00 21 00     P.i.r.a.t.e.!.!.
000030: 21 00 00 00 56 5F 9C 00 00 00 00 00 C3 C8 00 00     !...V_�.....��..
000040: 50 00 69 00 6B 00 61 00 73 00 6F 00 6F 00 00 00     P.i.k.a.s.o.o...
000050: C7 C8 00 00 66 00 66 00 66 00 00 00 01 00 00 00     ��..f.f.f.......
000060: 00 00 01 01 AE C8 00 00 74 00 68 00 69 00 73 00     ....��..t.h.i.s.
000070: 20 00 69 00 73 00 20 00 74 00 68 00 65 00 20 00      .i.s. .t.h.e. .
000080: 6D 00 61 00 73 00 74 00 65 00 72 00 20 00 63 00     m.a.s.t.e.r. .c.
000090: 6F 00 6D 00 6D 00 65 00 6E 00 74 00 00 00 00 00     o.m.m.e.n.t.....
0000A0: D0 C8 00 00 74 00 68 00 69 00 73 00 20 00 69 00     ��..t.h.i.s. .i.
0000B0: 73 00 20 00 6D 00 79 00 20 00 74 00 65 00 61 00     s. .m.y. .t.e.a.
0000C0: 6D 00 20 00 73 00 69 00 74 00 65 00 00 00 00 00     m. .s.i.t.e.....
0000D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000E0: DD C0 24 59 01 00 00 00 00 00 00 00 00 00 00 00     ��$Y............
0000F0: 00 00 00 00 01 00 00 00 00 00 00 00 00 00 00 00     ................
000100: CB C8 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ��..............
000110: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000120: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000130: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000140: 00 00 00 00 00 00 00 00 00 00 00 00 94 11 00 00     ............�...
000150: 00 00 00 00 0F 00 00 00     ........      }
        s:=#$8C#$00#$00#$00#$1C#$05#$04#$00#$01#$00#$00#$00#0#0#0#0#$DB#$C8#$00#$00//     �....G......FC..
          +#$53#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     S.p.a.c.e. .P.i.
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#0#0#0#0#0#0#0#0;//     r.a.t.e.!.!.!...


        //find leader
        c:=0;
        for f:=0 to 199 do begin
            if te.members[f].flag and 1 = 1 then
                move(te.members[f].id,s[$35],4);
            if te.members[f].id <> 0 then inc(c);
        end;
        move(te.name[0],s[$15],length(te.name)*2);
        move(te.ID,s[13],4);
        b:=te.LeaderAcc;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(b)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        s:=s+getmagic(length(a) div 2,$C8EC,$27)+a;

        b:=te.LeaderName;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(b)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;

        s:=s+getmagic(length(a) div 2,$C8EC,$27)+a+ansichar(c)+#0#0#0#0#0#1#1;



        b:=te.comment;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(b)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        s:=s+getmagic(length(a) div 2,$C8EC,$27)+a;

        b:=te.website;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(b)*2);
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        s:=s+getmagic(length(a) div 2,$C8EC,$27)+a;

        a:=#0#0#0#0;
        move(te.date,a[1],4);
        s:=s+a; //this is the creation date??

        s:=s+#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     ��$Y............
          +#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     ................
          +#$CB#$C8#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     ��..............
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     ................
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     ................
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00//     ................
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$94#$11#$00#$00//     ............�...
          +#$00#$00#$00#$00#$0F#$00#$00#$00;

        f:=length(s);
        move(f,s[1],4);
        sendtoplayer(x,s);
    end;
end;

procedure SetTeamComment(x:integer;s:ansistring);
var f,i,t,l:integer;
    te:TteamEntry;
    a,re:ansistring;
    b:widestring;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin

        //make sure the player is allowed to do it
        i:=0;
        for f:=0 to 199 do
            if player[x].GID = te.members[f].id then
                if te.members[f].flag and 1 = 1 then i:=1;
        if i = 0 then exit; //cant do it

        i:=$21;
        l:=0;
        //copy comment
        while pword(@s[i])^ <> 0 do begin
            te.comment[l]:=pwidechar(@s[i])^ ;
            inc(l);
            inc(i,2);
        end;
        te.comment[l]:=#0;
        inc(i,2);
        if i and 3 <> 1 then inc(i,2);
        inc(i,4); //skip encoded count
        l:=0;
        //copy website
        while pword(@s[i])^ <> 0 do begin
            te.website[l]:=pwidechar(@s[i])^ ;
            inc(l);
            inc(i,2);
        end;
        te.website[l]:=#0;
        inc(i,2);

        //reply to it
        re:=#$90#$00#$00#$00#$1C#$07#$04#$00#$01#$00#$00#$00#$00#$00#$00#$00
           +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00;
        b:=te.comment;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(a));
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        re:=re+getmagic(length(a) div 2,$D468,$bd)+a;

        b:=te.website;
        setlength(a,length(b)*2);
        move(b[1],a[1],length(a));
        a:=a+#0#0;
        while length(a) and 3 <> 0 do a:=a+#0#0;
        re:=re+getmagic(length(a) div 2,$D468,$bd)+a;

        re:=re+#1#0#0#0;
        f:=length(re);
        move(f,re[1],4);
        sendtoplayer(x,re);

        SaveTeamData(te.id,te)
    end;
end;

procedure SetTeamWelcome(x:integer;s:ansistring);
var f,i,t,l:integer;
    te:TteamEntry;
    a,re:ansistring;
    b:widestring;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin
        //make sure the player is allowed to do it
        i:=0;
        for f:=0 to 199 do
            if player[x].GID = te.members[f].id then
                if te.members[f].flag and 1 = 1 then i:=1;
        if i = 0 then exit; //cant do it

        i:=$19;
        l:=0;
        //copy comment
        while pword(@s[i])^ <> 0 do begin
            te.welcome[l]:=pwidechar(@s[i])^ ;
            inc(l);
            inc(i,2);
        end;
        te.welcome[l]:=#0;


        SaveTeamData(t,te)
    end;
end;

procedure RemovePlayerFromTeam(x:integer;s:ansistring);
var f,i,t,l:integer;
    te:TteamEntry;
    a,re:ansistring;
    b:widestring;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin
               //make sure the player is allowed to do it
        i:=0;
        for f:=0 to 199 do
            if player[x].GID = te.members[f].id then
                if te.members[f].flag and 1 = 1 then i:=1;
        if i = 0 then exit; //cant do it

        for f:=0 to 199 do
            if te.members[f].id = pdword(@s[9])^ then begin
                //if player is online send his free status
                //asdadasd
                //found it
                te.members[f].id:=0;
                te.members[f].flag:=0;
                te.members[f].AccName[0]:=#0;

                for l:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(l) then
                        if player[l].GID = te.members[f].id then begin
                            player[l].save.teamid:=0;
                            UpdatePlayerTeamData(l,4,te.date,te.name);
                        end;

                break;
            end;

        SaveTeamData(t,te);
        sendtoplayer(x,#$14#$00#$00#$00#$1C#$15#$00#$00#$01#$00#$00#$00+copy(s,9,8));
    end;
end;


procedure ChangePlayerRightinTeam(x:integer;s:ansistring);
var f,i,t,l:integer;
    te:TteamEntry;
    a,re:ansistring;
    b:widestring;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin

        //make sure the player is allowed to do it
        i:=0;
        for f:=0 to 199 do
            if player[x].GID = te.members[f].id then
                if te.members[f].flag and 1 = 1 then i:=1;
        if i = 0 then exit; //cant do it

        for f:=0 to 199 do
            if te.members[f].id = pdword(@s[9])^ then begin
                //found it
                te.members[f].flag:=byte(s[$11]) shl 8;
                if byte(s[$11]) = 2 then te.members[f].flag:=te.members[f].flag or 1;
                for l:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(l) then
                        if player[l].GID = te.members[f].id then begin
                            UpdatePlayerTeamData(l,byte(s[$11]),te.date,te.name);
                        end;
                break;
            end;

        //update member status in lobby

        SaveTeamData(t,te);
        sendtoplayer(x,#$14#$00#$00#$00#$1C#$17#$00#$00#$01#$00#$00#$00+copy(s,9,12));
    end;
end;


{
XOR: 0DD0   SUB: AC
//reply //this is mainly the list of player in team
000000: D4 01 00 00 1C 11 04 00 01 00 00 00 6C 0D 00 00     �...........l...
000010: 53 00 70 00 61 00 63 00 65 00 20 00 50 00 69 00     S.p.a.c.e. .P.i.
000020: 72 00 61 00 74 00 65 00 21 00 21 00 21 00 00 00     r.a.t.e.!.!.!...
000030: 7E 0D 00 00 DC A6 9B 00 00 00 00 00 4D 00 49 00     ~...ܦ�.....M.I.
000040: 55 00 52 00 45 00 54 00 48 00 00 00 00 00 00 00     U.R.E.T.H.......
000050: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000060: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000080: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000090: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000B0: 00 00 00 00 00 00 00 00 00 00 00 00 FF FF 03 00     ............��..
0000C0: FA F5 00 00 53 00 70 00 61 00 63 00 65 00 20 00     ��..S.p.a.c.e. .
0000D0: 50 00 69 00 72 00 61 00 74 00 65 00 21 00 21 00     P.i.r.a.t.e.!.!.
0000E0: 21 00 00 00 00 00 00 00 19 4F 37 59 00 00 00 00     !........O7Y....
0000F0: 00 00 00 00 00 00 00 00 00 00 00 00 A9 79 F0 57     ............�y�W
000100: 00 00 00 00 56 5F 9C 00 00 00 00 00 50 00 69 00     ....V_�.....P.i.
000110: 6B 00 61 00 73 00 6F 00 6F 00 00 00 00 00 00 00     k.a.s.o.o.......
000120: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000130: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000140: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000150: 66 00 66 00 66 00 00 00 00 00 00 00 00 00 00 00     f.f.f...........
000160: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000170: 00 00 00 00 01 04 01 00 00 00 00 00 FF FF FF FF     ............����
000180: 00 00 00 00 00 00 00 00 86 03 00 00 00 00 01 00     ........�.......
000190: FA F5 00 00 53 00 70 00 61 00 63 00 65 00 20 00     ��..S.p.a.c.e. .
0001A0: 50 00 69 00 72 00 61 00 74 00 65 00 21 00 21 00     P.i.r.a.t.e.!.!.
0001B0: 21 00 00 00 00 00 00 00 DD C0 24 59 00 00 00 00     !.......��$Y....
0001C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0001D0: 00 00 00 00     ....
}

procedure SendTeamMemberInfo(x:integer);
var a,s:ansistring;
    l,i,c,t,f:integer;
    te:TteamEntry;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin

        s:='';
        c:=0;
        for l:=0 to 199 do
        if te.members[l].id <> 0 then begin
            setlength(a, 13*16);
            fillchar(a[1],13*16,0);
            move(te.members[l].id,a[1],4);
            move(te.members[l].AccName[0],a[9],64);
            //if online tell where
            for i:=0 to srvcfg.MaxPlayer-1 do
                if playerok(i) then
                    if  player[i].GID = te.members[l].id then begin
                        //the player is online fill the info
                        move(player[i].activeChar.base.name[0],a[$4d],32);
                        a[$71]:=#1;
                        a[$72]:=#4;
                        a[$73]:=#1;
                        a[$79]:=#$ff;
                        a[$7a]:=#$ff;
                        a[$7b]:=#$ff;
                        a[$7c]:=#$ff;

                        a[$85]:=#$86;
                        a[$86]:=#3;
                        a[$8b]:=#$1;
                    end;

            //test for now this should be 00 00 or ff ff
            //ff ff = location inconue?
            //00 00 = main lobby
            a[$89]:=#$00;
            a[$8a]:=#$00;
            if (te.members[l].flag shr 8) and $ff = 1 then a[$8b]:=#$01;
            if (te.members[l].flag shr 8) and $ff = 2 then a[$8b]:=#$02;
            if (te.members[l].flag shr 8) and $ff = 0 then a[$8b]:=#$03;


            move(te.id,a[$8d],4);
            move(te.name[0],a[$91],32);
            move(te.members[l].joindate,a[$ad],4);

            s:=s+a;
            inc(c);
        end;

        s:=#$D4#$01#$00#$00#$1C#$11#$04#$00#$01#$00#$00#$00#$6C#$0D#$00#$00
        +#$53#$00#$70#$00#$61#$00#$63#$00#$65#$00#$20#$00#$50#$00#$69#$00
        +#$72#$00#$61#$00#$74#$00#$65#$00#$21#$00#$21#$00#$21#$00#$00#$00
        +getmagic(c,$0DD0,$ac)+s;
        c:=length(s);
        move(c,s[1],4);
        sendtoplayer(x,s);
    end else sendtoplayer(x,#$14#$00#$00#$00#$1C#$11#$04#$00#$02#$00#$00#$00#$7C#$0D#$00#$00
	+#$7C#$0D#$00#$00);
end;

procedure TeamSomething1(x:integer;s:ansistring);
var a:ansistring;
    t,f:integer;
    te:TteamEntry;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin

        a:=#$74#$00#$00#$00#$1C#$32#$00#$00#$01#$00#$00#$00#$FA#$F5#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$9D#$AA#$62#$59
            +#$00#$00#$00#$00#$0E#$00#$00#$00#$0D#$00#$00#$00#$08#$00#$00#$00
            +#$FF#$FF#$FF#$FF;
        move(player[x].save.teamid,a[13],4);

        move(te.date,a[$5d],4);
        sendtoplayer(x,a);
    end;
end;


procedure RetrivePlayerInfo(x:integer;s:ansistring);
var a:ansistring;
    t,f:integer;
    te:TteamEntry;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin
        for f:=0 to 199 do
            if te.members[f].id = player[x].GID then
                UpdatePlayerTeamData(x,(te.members[f].flag shr 8) and $ff,te.date,te.name);
    end else begin
        player[x].save.teamid:=0;
        UpdatePlayerTeamData(x,4,0,te.name);
    end;
end;


procedure RemoveMyselfFromTeam(x:integer);
var f,i,t,l:integer;
    te:TteamEntry;
    a,re:ansistring;
    b:widestring;
begin
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin
               //make sure the player is allowed to do it

        for f:=0 to 199 do
            if te.members[f].id = player[x].GID then begin
                //if player is online send his free status
                //asdadasd
                //found it
                te.members[f].id:=0;
                te.members[f].flag:=0;
                te.members[f].AccName[0]:=#0;


                player[x].save.teamid:=0;
                UpdatePlayerTeamData(x,4,te.date,te.name);


                break;
            end;

        SaveTeamData(t,te);
        sendtoplayer(x,#$14#$00#$00#$00#$1C#$13#$00#$00#$01#$00#$00#$00#0#0#0#0#0#0#0#0);
    end;
end;



procedure InvitPlayerToTeam(x:integer;s:ansistring);
var a:ansistring;
    i,t,l:integer;
    te:TteamEntry;
begin
    sendtoplayer(x,#$0C#$00#$00#$00#$1C#$09#$00#$00#$01#$00#$00#$00);

    //find the plsyer
    t:=player[x].save.teamid;
    if LoadTeamData(t,te) then begin
        for i:=0 to srvcfg.MaxPlayer-1 do
        if playerok(i) and (player[i].GID=pdword(@s[9])^) then begin
            setlength(a,$1dc);
            fillchar(a[1],$1dc,0);
            a:=#$E4#$01#$00#$00#$1C#$0C#$00#$00+a;
            move(player[i].GID,a[9],4); //target id
            move(player[x].GID,a[$11],4); //sender id
            move(player[x].activeChar.charID,a[$15],4); //sender char id
            move(te.id,a[$1d],4); //team id
            move(te.date,a[$21],4); //team creation date or invitation date?
            for l:=0 to 199 do
                if te.members[l].id = player[x].GID then
                    a[$27]:=ansichar((te.members[l].flag shr 8) and 255);

            //copy sender account name
            for l:=0 to length(player[x].aname)-1 do
                a[$39+(l*2)]:=player[x].aname[l];

            //copy target account name
            for l:=0 to length(player[x].aname)-1 do
                a[$7d+(l*2)]:=player[x].aname[l];

            //copy invit text
            move(s[$15],a[$c1],$100);

            //copy team name
            move(te.name[0],a[$1c1],32);

            player[i].teaminvitation:=copy(a,9,$1dc);

            sendtoplayer(i,a);
        end;
    end;
end;

procedure SendTeamRequestList(x:integer);
var s:ansistring;
begin
    //send only the last request since the magic is unknown
    s:=#$EC#$01#$00#$00#$1C#$0F#$04#$00#$01#$00#$00#$00#$42#$02#$00#$00+player[x].TeamInvitation;

    if player[x].TeamInvitation <> '' then sendtoplayer(x,s);
end;


procedure AwnserTeamOffer(x:integer;s:ansistring);
var i,t,l,c:integer;
    a:ansistring;
    te:TteamEntry;
begin
    //go trought all the entry and find the right one
    i:=1;
    while i < length(player[x].TeamInvitation) do begin
        if pdword(@s[9])^ = pdword(@player[x].TeamInvitation[i+$14])^ then begin
            //is it accepted or no?
            if s[$11] = #2 then begin
                //refused
                a:=#$24#$00#$00#$00#$1C#$0B#$04#$00#$02#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$B4#$EB#$00#$00#$04#$00#$00#$00#$56#$5F#$9C#$00
                +#$00#$00#$00#$00;
                t:=pdword(@player[x].TeamInvitation[i+$8])^;
                move(t,a[$1d],4);
                sendtoplayer(x,a);

                //make sender awnser
                a:=#$E8#$01#$00#$00#$1C#$0D#$00#$00#$03#$00#$00#$00+copy(player[x].TeamInvitation,i,$1dc);
                for l:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(l) then
                        if player[l].GID = t then sendtoplayer(l,a);
            end else begin
                //accepted
                if LoadTeamData(pdword(@s[9])^,te) then begin
                    player[x].save.teamid:=te.id;
                    for l:=0 to 199 do
                        if te.members[l].id = 0 then begin
                            te.members[l].id:=player[x].GID;
                            te.members[l].flag:=$300;
                            te.members[l].joindate :=Round((now() - 25569) * 86400);
                            fillchar(te.members[l].AccName[0],64,0);
                            for c:=0 to length(player[x].aname)-1 do
                                te.members[l].AccName[c]:=widechar(player[x].aname[c+1]);

                            break;
                        end;
                    UpdatePlayerTeamData(x,3,te.date,te.name);

                    a:=#$44#$00#$00#$00#$1C#$0B#$04#$00#$01#$00#$00#$00#$FA#$F5#$00#$00
                    +#$00#$00#$00#$00#$A4#$EB#$00#$00#$53#$00#$70#$00#$61#$00#$63#$00
                    +#$65#$00#$20#$00#$50#$00#$69#$00#$72#$00#$61#$00#$74#$00#$65#$00
                    +#$21#$00#$21#$00#$21#$00#$00#$00#$03#$00#$00#$00#$56#$5F#$9C#$00
                    +#$00#$00#$00#$00 ;

                    move(te.id,a[$1d],4);
                    t:=pdword(@player[x].TeamInvitation[i+$8])^;
                    move(t,a[$3d],4);
                    move(te.name[0],a[$19],32);
                    sendtoplayer(x,a);

                    //make sender awnser
                    a:=#$E8#$01#$00#$00#$1C#$0D#$00#$00#$02#$00#$00#$00+copy(player[x].TeamInvitation,i,$1dc);
                    for l:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(l) then
                        if player[l].GID = t then sendtoplayer(l,a);
                end;
            end;


            delete(player[x].TeamInvitation,i,$1dc); //remove it
            break;
        end;
        inc(i,$1dc);
    end;


end;

end.
