unit UScriptEngine;

interface
uses StrUtils,sysutils,types,windows;


procedure ProcessQuestEvent(x,t:integer;s:ansistring);

implementation

uses UDB, UInventory, UMovement, UQuestBoard, fmain, UMonster,
  UPsoUtil, casino, UDrop, UParty, UDebug, ULobby, GameEvent, ItemGenerator,
  UTimerHelper, UForest, UPlayer, GameInteraction;

procedure fnc_giveexp(x,t:integer;s:ansistring);
var a:ansistring;
begin
    a:=ReadWord(s);
    DistributeEventExp(x,strtoint(a));
end;

procedure fnc_createitem(x,t:integer;s:ansistring);
var a:ansistring;
    i:integer;
begin
    a:=ReadWord(s);
    for i:=0 to team[t].datacount-1 do
        if team[t].data[i].name = a then begin
            team[t].FloorObj[player[x].Floor]:= team[t].FloorObj[player[x].Floor] + team[t].data[i].data;
            sendtofloor(player[x].room,player[x].Floor,team[t].data[i].data);
        end;
end;

procedure fnc_give_mesetas(x,t:integer;s:ansistring);
var a:ansistring;
begin
    a:=ReadWord(s);
    DistributeEventMesetas(x,strtoint(a));
end;

procedure fnc_ExplorationEventCompleted(x,t:integer;s:ansistring);
begin
    team[t].FloorInEvent[player[x].Floor]:=0;
end;

procedure fnc_clear_count(x,t:integer;s:ansistring);
begin
    team[t].missioncount:=0;
end;

procedure fnc_inc_count(x,t:integer;s:ansistring);
var a:ansistring;
    i,c,l:integer;
begin
    a:=ReadWord(s);
    for i:=0 to 7 do
        if (team[t].ActiveCode[i].used = 1) and (team[t].ActiveCode[i].name = a)
          and (team[t].ActiveCode[i].floor = player[x].Floor) then begin
            inc(team[t].ActiveCode[i].count);
            a:=#$24#$00#$00#$00#$15#$05#$00#$00#$64#$00#$00#$00#$00#$00#$00#$00
            +#$06#$00#$a9#$11#$00#$00#$00#$00#$00#$00#$00#$00#$04#$00#$00#$00
            +#$00#$00#$00#$00;
            move(team[t].ActiveCode[i].id3,a[9],4);
            //a[$19]:=ansichar(team[t].countid shr 24);
            if team[t].ActiveCode[i].ty='ii_joint_struggle_ssn_apc' then
                a[$19]:=#1;
            move(team[t].ActiveCode[i].count,a[$1d],4);
            sendtofloor(player[x].room,player[x].Floor,a);

            //are we done?
            if team[t].ActiveCode[i].count >= team[t].ActiveCode[i].max then begin
                team[t].ActiveCode[i].used:=0; //clear it
                if team[t].ActiveCode[i].ty='ii_areaboss' then begin
                  a:=#$24#$00#$00#$00#$15#$10#$00#$00#$F9#$02#$00#$00#$00#$00#$00#$00
                    +#$06#$00#$a9#$11#$02#$03#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
                    +#$00#$00#$01#$00;
                  move(team[t].ActiveCode[i].id3,a[$9],4);
                  move(CurTargId,a[$15],4);
                  sendtofloor(player[x].room,player[x].Floor,a);
                end;

                //send the closing packets  XOR: 8DC9   SUB: C2
                a:=#$88#$00#$00#$00#$15#$03#$04#$00#$69#$00#$00#$00#$00#$00#$00#$00
                +#$06#$00#$a9#$11#$02#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$0E#$00#$00#$00#$0E#$00#$00#$00#$00#$00#$00#$00    //e e = 14 exp 14 mesetas
                +#$00#$00#$00#$00#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$0B#$8D#$00#$00#$0B#$8D#$00#$00#$1B#$8D#$00#$00
                +#$4E#$70#$63#$43#$6F#$6D#$4F#$6E#$53#$75#$63#$63
                +#$65#$73#$73#$00#$0A#$8D#$00#$00#$11#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$02#$0C#$00#$00#$00;
                if team[t].ActiveCode[i].ty='ii_joint_struggle_ssn_apc' then
                a:=#$54#$00#$00#$00#$15#$03#$04#$00#$2A#$07#$00#$00#$00#$00#$00#$00
                  +#$06#$00#$A9#$11#$02#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                  +#$00#$00#$00#$00#$37#$01#$00#$00#$F6#$01#$00#$00#$05#$00#$00#$00#$03#$00#$07#$00
                  +#$00#$00#$01#$00#$05#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                  +#$0B#$8D#$00#$00#$0B#$8D#$00#$00#$0B#$8D#$00#$00
                  +#$0B#$8D#$00#$00;
                move(team[t].ActiveCode[i].id3,a[9],4);
                if team[t].ActiveCode[i].exp > 0 then move(team[t].ActiveCode[i].exp,a[$25],4);
                if team[t].ActiveCode[i].mesetas > 0 then move(team[t].ActiveCode[i].mesetas,a[$29],4);
                if team[t].ActiveCode[i].ty<>'ii_joint_struggle_ssn_apc' then
                    move(team[t].ActiveCode[i].name[1],a[$65],length(team[t].ActiveCode[i].name));
                sendtofloor(player[x].room,player[x].Floor,a);

                if team[t].ActiveCode[i].ty='ii_areaboss' then begin
                  a:=#$28#$00#$00#$00#$15#$17#$04#$00#$F9#$02#$00#$00#$00#$00#$00#$00
                      +#$06#$00#$A9#$11#$01#$00#$00#$00#$38#$00#$00#$00#$02#$03#$00#$00
                      +#$00#$00#$00#$00#$06#$00#$A9#$11;
                  move(team[t].ActiveCode[i].id3,a[$9],4);
                  move(CurTargId,a[$19],4);
                  sendtofloor(player[x].room,player[x].Floor,a);
                end;

                a:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$B9#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$E8#$5C#$00#$00#$49#$6E#$63#$69#$64#$65#$6E#$74#$4D#$61#$72#$6B
                +#$65#$72#$28#$30#$29#$00#$00#$00;
                move(team[t].ActiveCode[i].id2,a[$15],4);
                move(player[x].GID,a[9],4);
                sendtofloor(player[x].room,player[x].Floor,a);

                //run the linked scrypt
                for c:=0 to 127 do
                    if team[t].CallBacks[c].name = team[t].ActiveCode[i].obj then begin
                        for l:=0 to team[t].CallBacks[c].cmd.Count-1 do
                            processquestevent(x,t,team[t].CallBacks[c].cmd[l]);
                        break;
                    end;
            end;
        end;
end;

procedure fnc_newcode(x,t:integer;s:ansistring);
var px,py:single;
    a,b,d,mu:ansistring;
    i,l,c,k,h1,h2:integer;
begin

    d:=ReadWord(s);  //type
    a:=ReadWord(s);  //name
    px:=strtofloat(ReadWord(s));  //px
    py:=strtofloat(ReadWord(s));  //py
    c:=strtoint(ReadWord(s));
    if d = 'ii_pit_ahl' then begin
        h1:=strtoint(ReadWord(s));
        CreateCodeEvent(x,t,player[x].Floor,c,px,py,d,a,h1,0,'',ReadWord(s),CurTargId);
    end else
    if d = 'ii_joint_struggle_ssn_apc' then begin
        //helpers id
        h1:=strtoint(ReadWord(s));
        h2:=strtoint(ReadWord(s));
        mu:=ReadWord(s);
        CreateCodeEvent(x,t,player[x].Floor,c,px,py,d,a,h1,h2,mu,ReadWord(s),CurTargId);
    end else begin
        CreateCodeEvent(x,t,player[x].Floor,c,px,py,d,a,0,0,'',ReadWord(s),CurTargId);
    end;
end;

procedure fnc_watch_id(x,t:integer;s:ansistring);
var a:ansistring;
begin
    a:=ReadWord(s);
    CurTargId:=strtoint(a);
end;

procedure fnc_set_reward(x,t:integer;s:ansistring);
var b:ansistring;
    i:integer;
begin
    b:=ReadWord(s);
    for i:=0 to 7 do
        if (team[t].ActiveCode[i].used = 1) and (team[t].ActiveCode[i].name = b) then begin
            team[t].ActiveCode[i].exp:=strtoint(ReadWord(s));
            team[t].ActiveCode[i].mesetas:=strtoint(ReadWord(s));
        end;
end;

procedure fnc_data(x,t:integer;s:ansistring);
var a,b:ansistring;
    i,l,c,k:integer;
begin
  a:=ReadWord(s);
  for i:=0 to team[t].datacount-1 do
      if team[t].data[i].name = a then begin
          a:=team[t].data[i].data;
          //apply write and other cmd
          for l:=0 to team[t].data[i].modcount-1 do begin
              b:=team[t].data[i].mods[l];
              if copy(b,1,6) = 'write ' then begin
                  delete(b,1,6);
                  c:=pos(' ',b);
                  k:=strtoint(copy(b,1,c-1));
                  delete(b,1,c);
                  if b = 'partyid' then begin
                     move(player[x].partyid,a[k+1],4);
                  end else
                  if b = 'playerid' then begin
                     move(player[x].gid,a[k+1],4);
                  end else begin
                      //look for var
                      for c:=0 to partys[player[x].partyid].varcount-1 do
                          if b = partys[player[x].partyid].vars[c].name then begin
                              move(partys[player[x].partyid].vars[c].val,a[k+1],4);
                              break;
                          end;
                  end;
              end;
              if b = 'isobject' then begin
                  //must add to floor
                  team[t].FloorObj[player[x].Floor]:=team[t].FloorObj[player[x].Floor]+a;
              end;
          end;
          sendtoparty(x,a);
          break;
      end;
end;

procedure fnc_add(x,t:integer;s:ansistring);
var b:ansistring;
    c:integer;
begin
    b:=ReadWord(s);
    for c:=0 to partys[player[x].partyid].varcount-1 do
        if b = partys[player[x].partyid].vars[c].name then begin
            inc(partys[player[x].partyid].vars[c].val,strtoint(ReadWord(s)));
            break;
        end;
end;

procedure fnc_set_count_id(x,t:integer;s:ansistring);
begin
    team[t].countid:=strtoint(ReadWord(s));
end;

procedure fnc_disable(x,t:integer;s:ansistring);
var b:ansistring;
begin
    b:=ReadWord(s);
    if b = 'ontick' then partys[player[x].partyid].onTick.Clear;
end;

procedure fnc_disable_group(x,t:integer;s:ansistring);
var b:ansistring;
    i,c,l:integer;
begin
    b:=ReadWord(s);
    l:=strtoint(b)-1;
    c:=strtoint(ReadWord(s));
    for i:=0 to 63 do
        if team[t].GroupInfo[l,i].id = c then begin
            //disable it
            team[t].GroupInfo[l,i].active:=0;
        end;
end;

procedure fnc_enable_group(x,t:integer;s:ansistring);
var b:ansistring;
    i,c,l:integer;
begin
    b:=ReadWord(s);
    l:=strtoint(b)-1;
    c:=strtoint(ReadWord(s));
    for i:=0 to 63 do
        if team[t].GroupInfo[l,i].id = c then begin
            //disable it
            team[t].GroupInfo[l,i].active:=1;
        end;
end;

procedure fnc_random(x,t:integer;s:ansistring);
var b,a:ansistring;
    i,c,l:integer;
begin
    b:=ReadWord(s);
    a:=ReadWord(s);
    l:=strtoint(b);
    for i:=0 to partys[player[x].partyid].varcount-1 do
        if partys[player[x].partyid].vars[i].name = a then begin
            partys[player[x].partyid].vars[i].val:=random(l);
        end;
end;

procedure fnc_free_npc(x,t:integer;s:ansistring);
var b:ansistring;
    c:integer;
begin
    b:=ReadWord(s);
    for c:=0 to team[t].npccount-1 do
        if team[t].npcs[c].id = strtoint(b) then begin

            team[t].npcs[c].DieTime:=gettickcount();
            break;
        end;
end;

procedure fnc_npc(x,t:integer;s:ansistring);
var b,a:ansistring;
    c,i,l:integer;
begin
    b:=ReadWord(s);
    team[t].npcs[team[t].npccount].id:=strtoint(b);

    b:=ReadWord(s);
    team[t].npcs[team[t].npccount].ctype:=b;

    for i:=0 to NPCCount-1 do
        if b = npcs[i].mtype then begin
            //npc is valid
            l:=strtoint(ReadWord(s)); //lvl
            team[t].npcs[team[t].npccount].DieTime:=0;
            team[t].npcs[team[t].npccount].hp:=npcs[i].life+round(l*(npcs[i].life*npcs[i].lvlup[0]));
            team[t].npcs[team[t].npccount].mhp:=team[t].npcs[team[t].npccount].hp;
            team[t].npcs[team[t].npccount].SATK:=npcs[i].SATK+round(l*(npcs[i].SATK*npcs[i].lvlup[1]));
            team[t].npcs[team[t].npccount].RATK:=npcs[i].RATK+round(l*(npcs[i].RATK*npcs[i].lvlup[2]));
            team[t].npcs[team[t].npccount].TATK:=npcs[i].TATK+round(l*(npcs[i].TATK*npcs[i].lvlup[3]));
            team[t].npcs[team[t].npccount].SDEF:=npcs[i].SDEF+round(l*(npcs[i].SDEF*npcs[i].lvlup[4]));
            team[t].npcs[team[t].npccount].RDEF:=npcs[i].RDEF+round(l*(npcs[i].RDEF*npcs[i].lvlup[5]));
            team[t].npcs[team[t].npccount].TDEF:=npcs[i].TDEF+round(l*(npcs[i].TDEF*npcs[i].lvlup[6]));
            team[t].npcs[team[t].npccount].APT:=npcs[i].APT+round(l*(npcs[i].APT*npcs[i].lvlup[7]));
            team[t].npcs[team[t].npccount].boostflg:=0;

            team[t].npcs[team[t].npccount].lvl:=l;

            team[t].npcs[team[t].npccount].wave:=strtoint(ReadWord(s))-1; //floor
            team[t].npcs[team[t].npccount].rot[0]:=strtofloat(ReadWord(s)); //rot
            team[t].npcs[team[t].npccount].rot[1]:=strtofloat(ReadWord(s));
            team[t].npcs[team[t].npccount].rot[2]:=strtofloat(ReadWord(s));
            team[t].npcs[team[t].npccount].rot[3]:=strtofloat(ReadWord(s));

            team[t].npcs[team[t].npccount].pos[0]:=strtofloat(ReadWord(s)); //pos
            team[t].npcs[team[t].npccount].pos[1]:=strtofloat(ReadWord(s));
            team[t].npcs[team[t].npccount].pos[2]:=strtofloat(ReadWord(s));
            team[t].npcs[team[t].npccount].pos[3]:=strtofloat(ReadWord(s));

            //send packet with lvl info
            a:=npcs[i].packet;
            //set lvl and hp and id
            move(team[t].npcs[team[t].npccount].id,a[9],4);
            move(team[t].npcs[team[t].npccount].hp,a[$49],4);
            move(team[t].npcs[team[t].npccount].lvl,a[$51],4);
            a[529+(byte(a[521])*8)]:=ansichar(team[t].npcs[team[t].npccount].lvl);
            a[529+(byte(a[522])*8)]:=ansichar(team[t].npcs[team[t].npccount].lvl);

            //set position
            pword(@a[$1d])^:= floattohalf(team[t].npcs[team[t].npccount].pos[0]);
            pword(@a[$1f])^:= floattohalf(team[t].npcs[team[t].npccount].pos[1]);
            pword(@a[$21])^:= floattohalf(team[t].npcs[team[t].npccount].pos[2]);
            pword(@a[$23])^:= floattohalf(team[t].npcs[team[t].npccount].pos[3]);

            //send to all
            sendtofloor(player[x].room,team[t].npcs[team[t].npccount].wave,a);

            //send the equip packet
            for c:=0 to team[t].datacount-1 do
                if team[t].data[c].name = s then begin
                    sendtofloor(player[x].room,team[t].npcs[team[t].npccount].wave,team[t].data[c].data);
                end;

            //make it apear
            a:=#$18#$00#$00#$00#$03#$30#$00#$00#$4B#$2F#$00#$00#$00#$00#$00#$00#$16#$00#$8A#$03#$01#$00#$00#$00;
            move(team[t].npcs[team[t].npccount].id,a[9],4);
            sendtofloor(player[x].room,team[t].npcs[team[t].npccount].wave,a);

            inc(team[t].npccount);
        end;
end;

procedure fnc_set_result(x,t:integer;s:ansistring);
var b:ansistring;
    c:integer;
begin
    b:=ReadWord(s);
    c:=0;
    if (b = '3') or (b = 's') or (b = 'S') then c:=3;
    if (b = '2') or (b = 'a') or (b = 'A') then c:=2;
    if (b = '1') or (b = 'b') or (b = 'B') then c:=1;
    if (b = '0') or (b = 'c') or (b = 'C') then c:=0;
    partys[player[x].partyid].result:=c;
end;

procedure fnc_obj_action(x,t:integer;s:ansistring);
var b,a:ansistring;
    i:integer;
begin
    b:=ReadWord(s);
    a:=ReadWord(s);
    i:=strtoint(b);
    {Xor: 5C0F  Sub: D5
    Xor: 5C4F  Sub: 95
    Xor: 5C8F  Sub: 55
    Xor: 5CCF  Sub: 15}
    s:=#$4C#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$B0#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
      +#$00#$00#$00#$00#$B0#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +getmagic(length(a)+1,$5C0F,$d5)+a+#0;
    while length(s) and 3 <> 0 do s:=s+#0;
    move(i,s[$15],4);
    move(i,s[$25],4);
    i:=length(s);
    move(i,s[1],4);
    sendtolobby(player[x].room,-1,s);
end;

procedure fnc_monst_effect(x,t:integer;s:ansistring);
var b,a:ansistring;
    i:integer;
    f:single;
begin
    b:=ReadWord(s);
    i:=strtoint(b);
    {Xor: 5C0F  Sub: D5
    Xor: 5C4F  Sub: 95
    Xor: 5C8F  Sub: 55
    Xor: 5CCF  Sub: 15}
    a:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$04#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
      +#$04#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$59#$0D#$03#$00
      +#$FF#$FF#$FF#$FF#$88#$E2#$D5#$CA#$00#$00#$00#$00#$04#$00#$00#$00
      +#$00#$00#$00#$08#$00#$00#$00#$00;

    move(i,a[$15],4);
    move(i,a[$21],4);

    b:=ReadWord(s);
    i:=strtoint(b);
    move(i,a[$2d],4);

    b:=ReadWord(s);
    if b<>'' then begin
        f:=strtofloat(b);
        move(f,a[$35],4);
    end;

    sendtolobby(player[x].room,-1,a);
end;

procedure fnc_else(x,t:integer;s:ansistring);
begin
    if partys[player[x].partyid].ifresult = 0 then ProcessQuestEvent(x,t,s);
end;

procedure fnc_if(x,t:integer;s:ansistring);
var a,b:ansistring;
    i,c,l:integer;
begin
    b:=ReadWord(s);

    //test if is a var
    i:=0;
    for c:=0 to partys[player[x].partyid].varcount-1 do
        if b = partys[player[x].partyid].vars[c].name then begin i:=1; break; end;
    partys[player[x].partyid].ifresult:=0;
    if i = 1 then begin
        //is a var
        i:=0;
        a:=ReadWord(s);
        if a = '=' then i:=1;
        if a = '!' then i:=2;
        if a = '<' then i:=3;
        if a = '>' then i:=4;

        b:=ReadWord(s);
        l:=strtoint(b);
        if i = 1 then if partys[player[x].partyid].vars[c].val = l then begin ProcessQuestEvent(x,t,s); partys[player[x].partyid].ifresult:=1; end;
        if i = 2 then if partys[player[x].partyid].vars[c].val <> l then begin ProcessQuestEvent(x,t,s); partys[player[x].partyid].ifresult:=1; end;
        if i = 3 then if partys[player[x].partyid].vars[c].val < l then begin ProcessQuestEvent(x,t,s); partys[player[x].partyid].ifresult:=1; end;
        if i = 4 then if partys[player[x].partyid].vars[c].val > l then begin ProcessQuestEvent(x,t,s); partys[player[x].partyid].ifresult:=1; end;

    end else begin
        //is a test

    end;
end;

procedure fnc_fence(x,t:integer;s:ansistring);
var a:ansistring;
    l:integer;
begin
    a:=ReadWord(s);
    if a = 'OPEN' then begin
        a:=#$4c#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
          +#$04#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$00#$00#$00#$00#$16#$02#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
          +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
          +#$D5#$5C#$00#$00#$4F#$50#$45#$4E#$00#$00#$00#$00;
        move(player[x].gid,a[9],4);
        l:=strtoint(ReadWord(s));
        move(l,a[$15],4); //copy the id
        move(l,a[$25],4); //copy the id
        SendtoFloor(player[x].room,player[x].Floor,a);
        setfencestatus(t,player[x].floor,l,1);
    end;
end;

procedure fnc_remove_item(x,t:integer;s:ansistring);
var a:ansistring;
    l:integer;
begin
    a:=ReadWord(s);
    l:=strtoint(a);
    RemoveObjFromFloor(x,t,player[x].Floor,l);
    a:=#$20#$00#$00#$00#$04#$06#$40#$00#$7D#$FF#$CF#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$4C#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11;
    move(l,a[$15],4);
    SendtoFloor(player[x].room,player[x].Floor,a);
end;

procedure fnc_set_timer(x,t:integer;s:ansistring);
var b:ansistring;
    c:integer;
begin
    b:=ReadWord(s);

    //find a free timer
    for c:=0 to 300 do
      if timers[c].teamid = -1 then begin
          timers[c].teamid:=t;
          timers[c].BeginTime:=gettickcount;
          timers[c].floor:=player[x].Floor;
          timers[c].Time:=strtoint(b);
          timers[c].CallBack:=ReadWord(s);
          break;
      end;
end;

procedure fnc_chalenge_explain(x,t:integer;s:ansistring);
var a:ansistring;
begin
    a:=#$1C#$00#$00#$00#$0B#$69#$04#$00#$04#$00#$00#$00#$01#$00#$00#$00
        +#$00#$00#$00#$00#$3A#$6E#$00#$00#$3A#$6E#$00#$00;
    a[$d]:=ansichar(strtoint(ReadWord(s)));
    SendtoFloor(player[x].room,player[x].Floor,a);
end;

procedure fnc_chalenge_start(x,t:integer;s:ansistring);
var a,b:ansistring;
begin;
    b:=ReadWord(s);
    a:=#$28#$00#$00#$00#$0B#$63#$00#$00#$42#$A9#$B0#$B6#$F6#$0B#$00#$00
    +#$00#$40#$1C#$46#$D6#$E6#$B6#$B6#$F6#$0B#$00#$00#$00#$40#$1C#$46
    +#$00#$00#$0C#$C2#$00#$00#$00#$00;     //-35
    pint64(@a[9])^:=team[player[x].TeamID].ChalengeTicks;
    pint64(@a[$15])^:=player[x].ping+(gettickcount()-player[x].pingticks);
    psingle(@a[$11])^:=strtoint(b);
    psingle(@a[$1d])^:=strtoint(b);
    SendtoFloor(player[x].room,player[x].Floor,a);

    team[player[x].TeamID].ChalengeStartTicks:=gettickcount();
    team[player[x].TeamID].ChalengeMax:=psingle(@a[$1d])^;
    team[player[x].TeamID].ChalengeCurrent:=psingle(@a[$1d])^;

    a:=getmissionpacket(x);
    SendtoFloor(player[x].room,player[x].Floor,a);
end;

procedure fnc_fail_event(x,t:integer;s:ansistring);
var a,b:ansistring;
    i,p:integer;
begin
    b:=ReadWord(s);  //name of the event
    a:=ReadWord(s); //when
    //find the event
    for i:=0 to 7 do
      if team[t].ActiveCode[i].used = 1 then
        if team[t].ActiveCode[i].floor = player[x].Floor then
           if team[t].ActiveCode[i].name = b then begin
               if a = 'now' then begin

               end else if a = 'campship' then begin
                   //set it so if you reatch campship its over
                   for p:=0 to srvcfg.MaxPlayer-1 do
                    if playerok(p) and (player[p].partyid = player[x].partyid) then
                      if player[p].inboss >= 1 then begin  //any special warp
                           player[p].failfrom:=b;
                           player[p].failtype:=team[t].ActiveCode[i].ty;
                      end;

               end;

               break;
           end;
end;

procedure fnc_create_interaction(x,t:integer;s:ansistring);
var a,b:ansistring;
    i,p:integer;
    pos,rot:TPSO2Vector;
begin
    b:=ReadWord(s);  //name of the event
    rot.x:=strtofloat(ReadWord(s));
    rot.y:=strtofloat(ReadWord(s));
    rot.z:=strtofloat(ReadWord(s));
    rot.w:=strtofloat(ReadWord(s));
    pos.x:=strtofloat(ReadWord(s));
    pos.y:=strtofloat(ReadWord(s));
    pos.z:=strtofloat(ReadWord(s));
    pos.w:=strtofloat(ReadWord(s));

    if b = 'helicopter_drop' then begin
        CreateHeliDrop(t,player[x].Floor,pos,rot);
    end;
end;

procedure fnc_random_once(x,t:integer;s:ansistring);
var n:ansistring;
begin
    //random_once <name> <rate> <code or function name>
    n:=readword(s);
    if team[t].floor_once[player[x].Floor].IndexOf(n) = -1 then begin
        //only f never done
        if random(strtoint(readword(s))) = 0 then begin
            //ok the random successed
            team[t].floor_once[player[x].Floor].Add(n); //add to dont run list
            ProcessQuestEvent(x,t,s); //run code
        end;
    end;

end;

procedure ProcessQuestEvent(x,t:integer;s:ansistring);
var a:ansistring;
    i,l:integer;
begin
    a:=ReadWord(s);
    if a = 'give_exp' then fnc_giveexp(x,t,s)
    else if a = 'createitem' then fnc_createitem(x,t,s)
    else if a = 'give_mesetas' then fnc_give_mesetas(x,t,s)
    else if a = 'ExplorationEventCompleted' then fnc_ExplorationEventCompleted(x,t,s)
    else if a = 'clear_count' then fnc_clear_count(x,t,s)
    else if a = 'questcompleted' then DistributeReward(player[x].partyid)
    else if a = 'newcode' then fnc_newcode(x,t,s)
    else if a = 'inc_count' then fnc_inc_count(x,t,s)
    else if a = 'watch_id' then fnc_watch_id(x,t,s)
    else if a = 'set_reward' then fnc_set_reward(x,t,s)
    else if a = 'data' then fnc_data(x,t,s)
    else if a = 'add' then fnc_add(x,t,s)
    else if a = 'set_count_id' then fnc_set_count_id(x,t,s)
    else if a = 'disable' then fnc_disable(x,t,s)
    else if a = 'disable_group' then fnc_disable_group(x,t,s)
    else if a = 'enable_group' then fnc_enable_group(x,t,s)
    else if a = 'random' then fnc_random(x,t,s)
    else if a = 'free_npc' then fnc_free_npc(x,t,s)
    else if a = 'npc' then fnc_npc(x,t,s)
    else if a = 'set_result' then fnc_set_result(x,t,s)
    else if a = 'obj_action' then fnc_obj_action(x,t,s)
    else if a = 'monst_effect' then fnc_monst_effect(x,t,s)
    else if a = 'else' then fnc_else(x,t,s)
    else if a = 'if' then fnc_if(x,t,s)
    else if a = 'fence' then fnc_fence(x,t,s)
    else if a = 'remove_item' then fnc_remove_item(x,t,s)
    else if a = 'set_timer' then fnc_set_timer(x,t,s)
    else if a = 'chalenge_explain' then fnc_chalenge_explain(x,t,s)
    else if a = 'chalenge_start' then fnc_chalenge_start(x,t,s)
    else if a = 'fail_event' then fnc_fail_event(x,t,s)
    else if a = 'create_interaction' then fnc_create_interaction(x,t,s)
    else if a = 'random_once' then fnc_random_once(x,t,s)


    else begin
        //is it a function if yes
        if a <> '' then
        for i:=0 to 127 do
          if team[t].CallBacks[i].name = a then begin  //run it
              for l:=0 to team[t].CallBacks[i].cmd.Count-1 do
                  ProcessQuestEvent(x,t,team[t].CallBacks[i].cmd[l]);
          end;
    end;

end;

end.
