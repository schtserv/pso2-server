unit UMyRoom;

interface
uses windows,classes,types,sysutils;
type
    troomitem = record
        id:dword;
        skin:dword;
        position:array[0..7] of word;
        reserved:array[0..15] of dword;
    end;
    TPersonnalRoom = record
        ownerid:dword;
        ownername:array[0..31] of widechar;
        roomstyle:dword;
        itemcount:integer;
        items:array[0..63] of troomitem;
        lock:dword;
    end;

var PersonnalRooms:array[0..200] of TPersonnalRoom;
    ItemsChilds:tstringlist;

procedure initRooms();
procedure LoadRoom(id,slot:integer);
procedure unlockroom(x:integer);
procedure SendMyRoomObject(x,rid:integer);
procedure PlaceObjectInRoom(x:integer; s:ansistring);
procedure MoveItemInRoom(x:integer;s:ansistring);
procedure EditRoomLock(x:integer; s:ansistring);
procedure RemoveAllItemInRoom(x:integer);
procedure RemoveItemInRoom(x:integer;s:ansistring);
procedure DoorMoveTo(x,id:integer);

implementation

uses fmain, UPlayer, UPsoUtil, ItemGenerator, UInventory, UDB, UDrop, ULobby;

procedure initRooms();
var i:integer;
begin
    for i:=0 to 199 do PersonnalRooms[i].ownerid:=$ffffffff;
    ItemsChilds:=tstringlist.Create;
    ItemsChilds.LoadFromFile(path+'data\roomobjlink.txt');
end;

procedure LoadRoom(id,slot:integer);
var f,i:integer;
begin
    if fileexists(path+'rooms\'+inttostr(id)) then begin
        f:=fileopen( path+'rooms\'+inttostr(id), $40);
        fileread(f,PersonnalRooms[slot],sizeof(PersonnalRooms[slot]));
        fileclose(f);

    end else begin
        PersonnalRooms[slot].ownerid:=id;
        PersonnalRooms[slot].roomstyle:=$FFFFFFF8;
        PersonnalRooms[slot].itemcount:=0;
        fillchar(PersonnalRooms[slot].ownername[0],64,0);
        //find the player
        for f:=0 to srvcfg.MaxPlayer-1 do
            if player[f].GID = id then begin
                for i:=0 to length(player[f].aname)-1 do
                    PersonnalRooms[slot].ownername[i]:=widechar(player[f].aname[i+1]);
            end;
    end;
end;

procedure unlockroom(x:integer);
var s:ansistring;
    r,l:integer;
begin
    r:=player[x].room-Room_PersonnalRoom;
    s:=#$34#$00#$00#$00#$1D#$0D#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$56#$5F#$9C#$00#$00#$00#$00#$00#$14#$00#$00#$00#$01#$00#$00#$00
    +#$00#$00#$00#$00;
    move(player[x].GID,s[9],4);
    move(PersonnalRooms[r].ownerid,s[$21],4);
    sendtoplayer(x,s);

    s:=#$C0#$00#$00#$00#$1D#$1F#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
    +#$00#$00#$00#$00#$19#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].GID,s[$9],4);
    sendtoplayer(x,s);

    s:=#$2C#$00#$00#$00#$1D#$20#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].GID,s[$9],4);
    sendtoplayer(x,s);

    for l:=0 to PersonnalRooms[r].itemcount-1 do begin
      s:=#$40#$00#$00#$00#$1D#$0F#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
      +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
      +#$31#$6D#$87#$0D#$F2#$49#$3D#$0D#$03#$00#$05#$00#$00#$00#$67#$00
      +#$04#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$01#$00#$00#$00;
      move(player[x].GID,s[$9],4);
      move(l,s[$21],1);
      pword(@s[$2f])^:=PersonnalRooms[r].items[l].skin-1;
      pdword(@s[$31])^:=PersonnalRooms[r].items[l].id;
      sendtoplayer(x,s);
    end;

    s:=#$40#$00#$00#$00#$1D#$0F#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$02#$00#$00#$00;
    move(player[x].GID,s[$9],4);
    sendtoplayer(x,s);

    s:=#$24#$00#$00#$00#$1D#$41#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$D4#$DF#$00#$00;
    move(player[x].GID,s[$9],4);
    sendtoplayer(x,s);

    s:=#$0C#$00#$00#$00#$1D#$19#$00#$00#$01#$00#$00#$04;
    sendtoplayer(x,s);
end;



procedure CleanChildObject(r,id:integer);
var a,b:ansistring;
    i,l,c,k:integer;
begin
    for i:=0 to ItemsChilds.Count-1 do begin
        a:=ItemsChilds[i];
        if ReadInt(a) = PersonnalRooms[r].items[id].skin and $ffff then begin
            while a <> '' do begin
                readword(a);
                c:=readint(a);
                for l:=1 to c do begin
                    b:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                        +#$04#$00#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
                    k:=PersonnalRooms[r].items[id].id+(l*100);
                    move(k,b[$15],4);
                    sendtolobby(Room_PersonnalRoom+r,-1,b);
                end;
            end;
            break; //done
        end;

    end;
end;

function GetChildObject(r,id:integer):ansistring;
var a,b:ansistring;
    i,l,c:integer;
    st:tstringlist;
begin
    result:='';
    for i:=0 to ItemsChilds.Count-1 do begin
        a:=ItemsChilds[i];
        if ReadInt(a) = PersonnalRooms[r].items[id].skin and $ffff then begin
            st:=tstringlist.Create;
            st.Add('ver 2');
            st.Add('define OPT_INT_51 = 51');
            st.Add('define OPT_INT_48 = 48');
            st.Add('define OPT_INT_58 = 58');
            st.Add('define OPT_INT_60 = 60');
            st.Add('define OPT_INT_49 = 49');
            st.Add('define OPT_INT_47 = 47');
            st.Add('define OPT_INT_50 = 50');
            st.Add('define OPT_ID_02 = 2');
            st.Add('define OPT_ID_09 = 9');
            st.Add('define OPT_ID_00 = 0');
            while a <> '' do begin
                b:=readword(a);
                if b = 'set' then begin
                    //set point
                    c:=readint(a);
                    for l:=0 to c-1 do begin
                        st.Add(format('object point_set%.2d',[l]));
                        st.Add(format('id %d',[PersonnalRooms[r].items[id].id+((l+1)*100)]));
                        st.Add(format('rotation %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[0]),
                            HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[1]),HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[2])
                            ,HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[3])]));
                        st.Add(format('position %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[4]),
                            HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[5]),HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[6])
                            ,HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[7])]));
                        st.Add('item_flag 3D 00 00 00 '+inttohex(PersonnalRooms[r].items[id].id,2)+' 00 00 00 00 00 00 00 06 00 A9 11 00 00 00 00 04 00 00 00');
                        st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 80 00 00 9E 54 E2');
                        st.Add('OPT_INT_50 -1');
                        st.Add('OPT_INT_47 1');
                        st.Add('OPT_INT_49 1');
                        st.Add('OPT_INT_48 0');
                        st.Add('OPT_INT_51 1');
                        st.Add('OPT_ID_02 0');
                    end;
                end;
                if b = 'sit' then begin
                    //sit/action point
                    c:=readint(a);
                    for l:=0 to c-1 do begin
                        st.Add('object oa_sit_point');
                        st.Add(format('id %d',[PersonnalRooms[r].items[id].id+((l+1)*100)]));
                        st.Add(format('rotation %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[0]),
                            HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[1]),HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[2])
                            ,HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[3])]));
                        st.Add(format('position %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[4]),
                            HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[5]),HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[6])
                            ,HalfPrecisionToFloat(PersonnalRooms[r].items[id].position[7])]));
                        st.Add('item_flag 2A 00 00 00 '+inttohex(PersonnalRooms[r].items[id].id,2)+' 00 00 00 00 00 00 00 06 00 A9 11 00 00 00 00 04 00 00 00');
                        st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 80 00 00 00 00 00');
                        st.Add('OPT_INT_50 -1');
                        st.Add('OPT_INT_47 1');
                        st.Add('OPT_INT_49 1');
                        st.Add('OPT_INT_58 0');
                        st.Add('OPT_INT_60 '+inttostr(l+1));
                        st.Add('OPT_ID_09 0');
                        st.Add('OPT_ID_00 '+inttostr(PersonnalRooms[r].items[id].id));
                    end;
                end;
            end;

            result:=GenerateObjData(st,'\quest\');
            st.free;
            break; //done
        end;

    end;

end;

procedure SendMyRoomObject(x,rid:integer);
var st:tstringlist;
    i:integer;
begin
    st:=tstringlist.Create;
    st.LoadFromFile(path+'map\myroom1.obj');
    for i:=0 to PersonnalRooms[rid].itemcount-1 do begin
        st.Add(format('object ob_%.4d_%.4d',[PersonnalRooms[rid].items[i].skin shr 16,PersonnalRooms[rid].items[i].skin and $ffff]));
        st.Add(format('id %d',[PersonnalRooms[rid].items[i].id]));
        st.Add(format('rotation %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[0]),
            HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[1]),HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[2])
            ,HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[3])]));
        st.Add(format('position %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[4]),
            HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[5]),HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[6])
            ,HalfPrecisionToFloat(PersonnalRooms[rid].items[i].position[7])]));
        st.Add('item_flag 3E 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
        st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 80 02 00 50 45 41');
        st.Add('OPT_INT_50 -1');
        st.Add('OPT_INT_47 1');
        st.Add('OPT_INT_49 6');
        st.Add('OPT_INT_48 0');
        st.Add('OPT_INT_51 1');
        sendtoplayer(x,GetChildObject(rid,i));
    end;

    //convert and send
    sendtoplayer(x,GenerateObjData(st,'\quest\'));
    st.Free;
end;


procedure PlaceObjectInRoom(x:integer; s:ansistring);
var i,r,m,l,k,f:integer;
    a:ansistring;
    st:tstringlist;
begin
    //find the room
    r:=player[x].room-Room_PersonnalRoom;
    if r < 0 then exit; //safety
    if r> 199 then exit;

    if player[x].GID <> PersonnalRooms[r].ownerid then exit; //not his room

    //find it in inventory , must test all inventory
    //test player inventory
    i:=-1;
    for m:=0 to player[x].activechar.BankCount-1 do
      if (pdword(@s[$35])^ = player[x].activeChar.PersonalBank[m].id1)
          and (pdword(@s[$39])^ = player[x].activeChar.PersonalBank[m].id2) then begin
              //found remove it
              a:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
              +#$B9#$28#$A6#$04#$00#$00#$01#$00#$01#$00#$00#$00;
              move(player[x].activeChar.PersonalBank[m].id1,a[13],4);
              move(player[x].activeChar.PersonalBank[m].id2,a[17],4);
              i:=player[x].activeChar.PersonalBank[m].ItemEntry; //this is the skin id
              if player[x].activeChar.PersonalBank[m].ItemType = 3 then begin
                  a[$15]:=ansichar(player[x].activeChar.PersonalBank[m].count-1);
                  if a[$15] <> #0 then a[$19]:=#2;
              end;

              removeFromStorage(x,player[x].activeChar.PersonalBank[m].id1,player[x].activeChar.PersonalBank[m].id2,1
                  ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
              sendtoplayer(x,a);
              break;
          end;

    //find the item in bank
        for m:=0 to player[x].activechar.CharBankCount-1 do
            if (pdword(@s[i])^ = player[x].activeChar.CharBank[m].id1)
                and (pdword(@s[i+4])^ = player[x].activeChar.CharBank[m].id2) then begin
                    a:=#$30#$00#$00#$00#$0F#$22#$04#$00#$E8#$4D#$00#$00#$E9#$4D#$00#$00
                      +#$AE#$CA#$6C#$D5#$78#$86#$2B#$0D#$00#$00#$01#$00#$00#$00#$00#$00
                      +#$E8#$4D#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                    move(player[x].activeChar.CharBank[m].id1,a[17],4);
                    move(player[x].activeChar.CharBank[m].id2,a[21],4);
                    i:=player[x].activeChar.CharBank[m].ItemEntry; //this is the skin id
                    if player[x].activeChar.CharBank[m].ItemType = 3 then begin
                        a[$19]:=ansichar(player[x].activeChar.CharBank[m].count-1);
                        a[$1b]:=ansichar(1);
                    end;

                    removeFromStorage(x,player[x].activeChar.CharBank[m].id1,player[x].activeChar.CharBank[m].id2,1
                        ,player[x].activeChar.CharBank,@player[x].activechar.CharBankCount,200);
                    sendtoplayer(x,a);
                    break;
                end;

        //find the item in bank
        for m:=0 to player[x].save.BasicStorageCount -1 do
            if (pdword(@s[i])^ = player[x].save.BasicStorage[m].id1)
                and (pdword(@s[i+4])^ = player[x].save.BasicStorage[m].id2) then begin
                    a:=#$30#$00#$00#$00#$0F#$22#$04#$00#$E8#$4D#$00#$00#$E9#$4D#$00#$00
                      +#$AE#$CA#$6C#$D5#$78#$86#$2B#$0D#$00#$00#$01#$00#$0e#$00#$00#$00
                      +#$E8#$4D#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                    move(player[x].save.BasicStorage[m].id1,a[17],4);
                    move(player[x].save.BasicStorage[m].id2,a[21],4);
                    i:=player[x].save.BasicStorage[m].ItemEntry; //this is the skin id
                    if player[x].save.BasicStorage[m].ItemType = 3 then begin
                        a[$19]:=ansichar(player[x].save.BasicStorage[m].count-1);
                        a[$1b]:=ansichar(1);
                    end;

                    removeFromStorage(x,player[x].save.BasicStorage[m].id1,player[x].save.BasicStorage[m].id2,1
                        ,player[x].save.BasicStorage,@player[x].save.BasicStorageCount,200);
                    sendtoplayer(x,a);
                    break;
                end;



    //find an id
    l:= 0;
    k:=10;

    while l < PersonnalRooms[r].itemcount do begin
        if PersonnalRooms[r].items[l].id = k then begin
            inc(k);
            l:=0;
        end else inc(l);
    end;

    PersonnalRooms[r].items[PersonnalRooms[r].itemcount].id:=k;
    PersonnalRooms[r].items[PersonnalRooms[r].itemcount].skin:=i+1+(1000 shl 16);
    move(s[$21],PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[0],16);
    //drop it on the floor
    st:=tstringlist.Create;
    st.Add('ver 2');
    st.Add('define OPT_INT_51 = 51');
    st.Add('define OPT_INT_48 = 48');
    st.Add('define OPT_INT_49 = 49');
    st.Add('define OPT_INT_47 = 47');
    st.Add('define OPT_INT_50 = 50');

    st.Add(format('object ob_%.4d_%.4d',[PersonnalRooms[r].items[PersonnalRooms[r].itemcount].skin shr 16,
            PersonnalRooms[r].items[PersonnalRooms[r].itemcount].skin and $ffff]));
    st.Add('id '+inttostr(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].id));
    st.Add(format('rotation %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[0]),
            HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[1]),HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[2])
            ,HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[3])]));
    st.Add(format('position %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[4]),
            HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[5]),HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[6])
            ,HalfPrecisionToFloat(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].position[7])]));
    st.Add('item_flag 3E 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
    st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 80 02 00 50 45 41');
    st.Add('OPT_INT_50 -1');
    st.Add('OPT_INT_47 1');
    st.Add('OPT_INT_49 6');
    st.Add('OPT_INT_48 0');
    st.Add('OPT_INT_51 1');

    sendtolobby(player[x].room,-1,GetChildObject(r,PersonnalRooms[r].itemcount));


    a:=#$44#$00#$00#$00#$1D#$11#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$CA#$62#$71#$39#$C1#$2F#$BD#$0D#$03#$00#$05#$00#$00#$00#$29#$01 //item id, item code
    +#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11#$04#$00#$01#$00 //new item id on floor, 04 00 01 00 = 04 as 4 action remain??
    +#$00#$00#$00#$00;
    move(player[x].GID,a[9],4);
    move(s[$35],a[$21],16);
    move(PersonnalRooms[r].items[PersonnalRooms[r].itemcount].id,a[$31],4);

    inc(PersonnalRooms[r].itemcount);

    sendtolobby(player[x].room,-1,GenerateObjData(st,'\quest\'));

    sendtoplayer(x,a);

    st.Free;


    //resave it
    f:=filecreate( path+'rooms\'+inttostr(PersonnalRooms[r].ownerid));
        filewrite(f,PersonnalRooms[r],sizeof(PersonnalRooms[r]));
        fileclose(f);
end;


procedure MoveItemInRoom(x:integer;s:ansistring);
var i,r,f:integer;
    st:tstringlist;
    a:ansistring;
begin
  //find the room
    r:=player[x].room-Room_PersonnalRoom;
    if r < 0 then exit; //safety
    if r> 199 then exit;

    if player[x].GID <> PersonnalRooms[r].ownerid then exit; //not his room

    //find the item
    for i:=0 to PersonnalRooms[r].itemcount-1 do
        if PersonnalRooms[r].items[i].id = pdword(@s[$21])^ then begin
            move(s[$2d],PersonnalRooms[r].items[i].position[0],16);
            //drop it on the floor
            st:=tstringlist.Create;
            st.Add('ver 2');
            st.Add('define OPT_INT_51 = 51');
            st.Add('define OPT_INT_48 = 48');
            st.Add('define OPT_INT_49 = 49');
            st.Add('define OPT_INT_47 = 47');
            st.Add('define OPT_INT_50 = 50');

            st.Add(format('object ob_%.4d_%.4d',[PersonnalRooms[r].items[i].skin shr 16,
            PersonnalRooms[r].items[i].skin and $ffff]));
            st.Add('id '+inttostr(PersonnalRooms[r].items[i].id));
            st.Add(format('rotation %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[0]),
                    HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[1]),HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[2])
                    ,HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[3])]));
            st.Add(format('position %.4f %.4f %.4f %.4f',[HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[4]),
                    HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[5]),HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[6])
                    ,HalfPrecisionToFloat(PersonnalRooms[r].items[i].position[7])]));
            st.Add('item_flag 3E 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 04 00 00 00');
            st.Add('byte_flag 02 00 00 00 00 00 00 00 00 00 00 00 00 00 80 02 00 50 45 41');
            st.Add('OPT_INT_50 -1');
            st.Add('OPT_INT_47 1');
            st.Add('OPT_INT_49 6');
            st.Add('OPT_INT_48 0');
            st.Add('OPT_INT_51 1');
            sendtolobby(player[x].room,-1,GetChildObject(r,i));
            sendtolobby(player[x].room,-1,GenerateObjData(st,'\quest\'));
            st.Free;

            //move confirmed
            a:=#$58#$00#$00#$00#$1D#$0B#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$07#$00#$00#$00
            +#$00#$00#$00#$00#$06#$00#$91#$43#$0D#$00#$00#$00#$00#$00#$00#$00
            +#$06#$00#$a9#$11#$01#$00#$00#$00;
            move(player[x].GID,a[9],4);
            move(s[$21],a[$3d],$18);
            sendtoplayer(x,a);

            //resave it
        f:=filecreate( path+'rooms\'+inttostr(PersonnalRooms[r].ownerid));
        filewrite(f,PersonnalRooms[r],sizeof(PersonnalRooms[r]));
        fileclose(f);
        end;
end;


procedure EditRoomLock(x:integer; s:ansistring);
var r,f:integer;
    a:ansistring;
begin
    //find the room
    r:=player[x].room-Room_PersonnalRoom;
    if r < 0 then exit; //safety
    if r> 199 then exit;

    if player[x].GID <> PersonnalRooms[r].ownerid then exit; //not his room

    PersonnalRooms[r].lock:=byte(s[$21]);

    a:=#$0C#$00#$00#$00#$1D#$19#$00#$00#$01#$00#$00+s[$21];

    sendtolobby(player[x].room,-1,a);

    a:=#$58#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
    +#$00#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$E9#$5C#$00#$00#$52#$6F#$6F#$6D#$4C#$6F#$63#$6B#$49#$6E#$64#$65
    +#$78#$28#$34#$29#$00#$00#$00#$00;
    move(player[x].GID,a[9],4);
    a[$53]:=ansichar(byte(s[$21])+$30);
    sendtolobby(player[x].room,-1,a);

    //resave it
    f:=filecreate( path+'rooms\'+inttostr(PersonnalRooms[r].ownerid));
        filewrite(f,PersonnalRooms[r],sizeof(PersonnalRooms[r]));
        fileclose(f);
end;


procedure RemoveAllItemInRoom(x:integer);
var r,f,i,l:integer;
    a:ansistring;
    d:dword;
begin
    //find the room
    r:=player[x].room-Room_PersonnalRoom;
    if r < 0 then exit; //safety
    if r> 199 then exit;

    if player[x].GID <> PersonnalRooms[r].ownerid then exit; //not his room

    i:=PersonnalRooms[r].itemcount-1;
    while i > -1 do begin
        //try to add it
        a:=#$54#$00#$00#$00#$0F#$53#$04#$00#$9B#$E7#$00#$00#$9C#$E7#$00#$00
        +#$B6#$FD#$EC#$59#$DC#$21#$76#$0D#$03#$00#$05#$00#$00#$00#$E7#$04
        +#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00;
        d:=CreateItemID(x);
        move(d,a[$11],4);
        move(player[x].activechar.charid,a[$15],4);
        f:=PersonnalRooms[r].items[i].skin and $ffff;
        dec(f);
        move(f,a[$1f],2);
        l:=AddToStorage(x,@a[$11],player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);
        if l = -1 then begin
            //warning and stop
            a:=#$8C#$00#$00#$00#$19#$01#$04#$00+TextToPSO('Your inventory is full.')+#3#0#0#0;
            f:=length(a);
            move(f,a[1],4);
            sendtoplayer(x,a);
            break;
        end else begin
            //if success remove on floor and notify player
            move(player[x].activechar.personalbank[l],a[$11],$8);
            sendtoplayer(x,a);
            a:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
            move(player[x].GID,a[9],4);
            move(PersonnalRooms[r].items[i].id,a[$15],4);
            sendtolobby(player[x].room,-1,a);
            CleanChildObject(r,i);
            dec(PersonnalRooms[r].itemcount);
            dec(i);
        end;

    end;

    a:=#$38#$00#$00#$00#$1D#$13#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$14#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].GID,a[9],4);
    sendtoplayer(x,a);

    //confirmed
    a:=#$0C#$00#$00#$00#$1D#$38#$00#$00#$00#$00#$01#$00;//     .....8......
    sendtoplayer(x,a);

    //resave it
    f:=filecreate( path+'rooms\'+inttostr(PersonnalRooms[r].ownerid));
        filewrite(f,PersonnalRooms[r],sizeof(PersonnalRooms[r]));
        fileclose(f);
end;

procedure RemoveItemInRoom(x:integer;s:ansistring);
var r,f,i,l:integer;
    a:ansistring;
    d:dword;
begin
    //find the room
    r:=player[x].room-Room_PersonnalRoom;
    if r < 0 then exit; //safety
    if r> 199 then exit;

    if player[x].GID <> PersonnalRooms[r].ownerid then exit; //not his room

    for i:=0 to PersonnalRooms[r].itemcount-1 do
    if PersonnalRooms[r].items[i].id = pdword(@s[$21])^ then begin
        //try to add it
        a:=#$54#$00#$00#$00#$0F#$53#$04#$00#$9B#$E7#$00#$00#$9C#$E7#$00#$00
        +#$B6#$FD#$EC#$59#$DC#$21#$76#$0D#$03#$00#$05#$00#$00#$00#$E7#$04
        +#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
        +#$00#$00#$00#$00;
        d:=CreateItemID(x);
        move(d,a[$11],4);
        move(player[x].activechar.charid,a[$15],4);
        f:=PersonnalRooms[r].items[i].skin and $ffff;
        dec(f);
        move(f,a[$1f],2);
        l:=AddToStorage(x,@a[$11],player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);
        if l = -1 then begin
            //warning and stop
            a:=#$8C#$00#$00#$00#$19#$01#$04#$00+TextToPSO('Your inventory is full.')+#3#0#0#0;
            f:=length(a);
            move(f,a[1],4);
            sendtoplayer(x,a);

        end else begin
            //if success remove on floor and notify player
            move(player[x].activechar.personalbank[l],s[$11],$8);
            sendtoplayer(x,a);
            a:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$07#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11;
            move(player[x].GID,a[9],4);
            move(PersonnalRooms[r].items[i].id,a[$15],4);
            sendtolobby(player[x].room,-1,a);
            CleanChildObject(r,i);
            move(PersonnalRooms[r].items[i+1],PersonnalRooms[r].items[i],sizeof(troomitem)*(PersonnalRooms[r].itemcount-i));
            dec(PersonnalRooms[r].itemcount);
        end;
        break;
    end;


    a:=#$38#$00#$00#$00#$1D#$13#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$01#$00#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$06#$00#$a9#$11
    +#$00#$00#$00#$00#$00#$00#$00#$00;
    move(player[x].GID,a[9],4);
    move(s[$21],a[$25],4);
    sendtoplayer(x,a);

    //resave it
    f:=filecreate( path+'rooms\'+inttostr(PersonnalRooms[r].ownerid));
        filewrite(f,PersonnalRooms[r],sizeof(PersonnalRooms[r]));
        fileclose(f);
end;


procedure DoorMoveTo(x,id:integer);
begin
    if id = 0 then golobby(x,1)
    else if id = 1 then golobby(x,0)
    else if id = 2 then gocasino(x,0)
    else if id = 3 then gobridge(x)
    else if id = 4 then gocafe(x)
    else golobby(x,0);
end;

end.
