unit UParty;

interface
uses windows,types,sysutils,Classes;

type

    tquestvar = record
        name:ansistring;
        val:dword;
    end;
    TPartyEntry = record
        used:integer;
        name,pass,comment,questname:ansistring;
        lvlmin,lvlmax,limitation:integer;
        options:ansistring;
        leader:integer;
        time:dword;

        queststart:ansistring;
        //var
        varcount:integer;
        vars:array[0..64] of tquestvar;

        onTick:tstringlist;
        ifresult:integer;
        bosswarp:dword;

        //door changes
        door_entry_p:array[0..15] of dword;

        death,result:integer;
    end;

procedure SendParty(x:integer);
procedure GetPartyID(x:integer;s:ansistring);
function GetNextPartyID(x:integer):dword;
procedure invitePlayerToParty(x:integer;s:ansistring);
procedure PartyQuerryInfo1(x:integer;s:ansistring);
procedure PartyQuerryInfo2(x:integer;s:ansistring);
procedure AcceptParty(x:integer; s:ansistring);
procedure LeaveParty(x:integer);
procedure KickPlayerFromParty(x:integer;s:ansistring);
procedure SetPartyLeader(x:integer;id:dword);
procedure DisbandParty(x:integer);
function partycount(id:integer):integer;
procedure GetPartyList(x:integer;s:ansistring);
procedure JoinGame(x:integer;s:ansistring);


var Partys:array[1..200] of TPartyEntry;

implementation

uses UPlayer, fmain, UPsoUtil, UQuestBoard, ULobby, UDebug;

function GetNextPartyID(x:integer):dword;
var i:integer;
    s:widestring;
begin
    if player[x].partyid <> 0 then
        if partys[player[x].partyid].leader = x then partys[player[x].partyid].used:=0;
    for i:=1 to 200 do
        if partys[i].used = 0 then begin
            partys[i].used:=1;
            s:=player[x].activechar.base.name;
            setlength(partys[i].name,length(s)*2);
            move(s[1],partys[i].name[1],length(s)*2);
            partys[i].pass:='';
            partys[i].comment:='';
            partys[i].questname:='';
            partys[i].lvlmin:=1;
            partys[i].lvlmax:=75;
            partys[i].options:=#0#0#0#0#0#0#0#0#0#0#0#0;
            partys[i].leader:=x;
            result:=i;
            break;
        end;
end;


//client request this to get the party name
procedure PartyQuerryInfo1(x:integer;s:ansistring);
var a,b:ansistring;
    i,c,p:integer;
    id,k:integer;
begin
    //XOR: E7E8   SUB: FF
    c:=0;
    if length(s) > 12 then begin
    a:=#$D4#$01#$00#$00#$0E#$1B#$04#$00#$01#$00#$00#$00;
    while (c*12)+13 < length(s) do
    for i:=0 to 9 do begin
        if (c*12)+13 < length(s) then begin
            move(s[(c*12)+13],id,4);
            if id > 200 then exit;
            if partys[id].used = 1 then begin
                b:=#0#0#0#0#0#0#0#0#$00#$00#$00#$00;

                if player[partys[id].leader].teamid <> -1 then begin
                    b[9]:=#$10;
                    move(player[partys[id].leader].teamid,b[1],4);
                end;

                a:=a+b;
                b:=#0#0#0#0#0#0#0#0#$0D#$00#$53#$01;     //id
                move(id,b[1],4);
                if player[partys[id].leader].teamid <> -1 then begin
                    a:=a+b+ UniToUniMagic(partys[id].name ,$E7E8,$FF)+#$04#$01#$04#$01#$00;
                    if partys[id].limitation and 8 = 8 then a:=a+#0
                    else a:=a+#1;
                    a:=a+chr(c+1)+#$00;
                    b:=#0; //normal game
                    if partys[id].pass <> #0#0#0#0 then b[1]:=#1;
                    if partys[id].limitation and 1 = 1 then b[1]:=ansichar(byte(b[1]) or 4);
                    if partys[id].limitation and 2 = 2 then b[1]:=ansichar(byte(b[1]) or 8);
                    a:=a+b+#$00#$00#$00;
                    k:=(gettickcount-partys[id].time) div 1000;
                    b:=#0#0#0#0#0#0#0#0;
                    move(k,b[1],4);
                    move(team[player[partys[id].leader].teamid].questid2,b[5],4);
                    a:=a+b;
                end else                                            //max min              s.p r#           flag              time
                  a:=a+b+ UniToUniMagic(partys[id].name ,$E7E8,$FF)+#$00#$00#$04#$01#$00#$00#$00#$00#$8#$00#$00#$00#$00#$00#00#$00+#$FA#$89#$F0#$57;
            //s.p 0 = solo, 1 = 4 player, 2 = 12 player
            //r# = room number
            //flag 1 = pass, 4 = friend, 8 = team member,
            inc(c);
            end else a:=a+#$00#$00#$00#$00#$00#$00#$00#$00
	        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	        +#$17#$E7#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	        +#$00#$00#$00#$00#$00#$00#$00#$00;
        end else begin
            a:=a+#$00#$00#$00#$00#$00#$00#$00#$00
	        +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	        +#$17#$E7#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	        +#$00#$00#$00#$00#$00#$00#$00#$00;
        end;
    end;

    move(c,a[9],2);
    c:=length(a);
    move(c,a[1],4);
    sendtoplayer(x,a);
    end;

    sendtoplayer(x,#$0C#$00#$00#$00#$0E#$1C#$00#$00#$00#$00#$00#$00);
end;


procedure PartyQuerryInfo2(x:integer;s:ansistring);
var a,b:ansistring;
    l,i,c,k,p:integer;
    id:dword;
begin
{XOR: 7921   SUB: E0
XOR: 7921   SUB: E0
XOR: 7921   SUB: E0
XOR: 7945   SUB: CF
XOR: 7945   SUB: CF
XOR: 793A   SUB: 65
XOR: 7900   SUB: 00
XOR: 7900   SUB: 03
XOR: 79A3   SUB: 5E     }

    //comment (minimum 1)
    //partyid (12 byte);
    //unknown $20 bytes
    //team option 12 bytes
    //char name
    //player name
    //player GID (12 byte)
    //class,sub:byte
    //lvl:word
    //unk 00 00 01 00 = leader? (00 00 00 FF = empty)

    if length(s) > 12 then begin
    c:=0;

    a:=#$40#$08#$00#$00#$0E#$1E#$04#$00#$01#$00#$00#$00;
    while (c*12)+13 < length(s) do
    for i:=0 to 11 do begin
        if (c*12)+13 < length(s) then begin
            move(s[(c*12)+13],id,4);
            if id > 200 then exit;
            if partys[id].used = 1 then begin
            b:=#0#0#0#0#0#0#0#0#$0D#$00#$53#$01;     //id
            move(id,b[1],4);
            a:=a+UniToUniMagic(partys[id].comment ,$7921,$E0)         //party config
            +b;
            
            if player[partys[id].leader].teamid <> -1 then begin
                b:=#0#0#0#0;
                move(team[player[partys[id].leader].teamid].questid2,b[1],4);
                a:=a+#$af#$01#$00#$00#$00#$00#$00#$00#$0b#$00#$00#$00#$00#$00#$e8#$03
                +#$00#$00#$00#$00#$00#$00#$00#$00+b+#$09#$00#$00#$00   //quest id
                +#$1#$00#$00#$00#$3#$4+chr(partys[id].lvlmin)+chr(partys[id].lvlmax)+#$c#1#$00#$ff#0#0#0#0;
            end else begin
                a:=a+#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$FF#$FF#$FF#$FF#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$0#$0+chr(partys[id].lvlmin)+chr(partys[id].lvlmax)+#$0#0#$ff#0#0#0#0#0;
            end;

            b:=#0#0#0#0#0#0#0#0#4#0#0#0;
            move(player[partys[id].leader].gid,b[1],4);
            l:=player[partys[id].leader].activechar.base.job;
            a:=a+GetCharNameWithMagic(partys[id].leader,$7921,$E0)+
                TextToUni(player[partys[id].leader].aname+#0,$7921,$E0)+b
                +chr(l)
                +chr(player[partys[id].leader].activechar.base.subjob)
                +chr(player[partys[id].leader].activechar.base.jobstat[l].lvl)+#0#0#0#1#0;

            //do the 3 remaining that arent leader
            k:=1;
            for p:=0 to srvcfg.MaxPlayer-1 do
            if (playerok(p)) and
                (player[p].partyid = id) and (p <> partys[id].leader) then begin
                inc(k);
                move(player[p].gid,b[1],4);
                l:=player[p].activechar.base.job;
                a:=a+GetCharNameWithMagic(p,$7921,$E0)+
                TextToUni(player[p].aname,$7921,$E0)+b
                +chr(l)
                +chr(player[p].activechar.base.subjob)
                +chr(player[p].activechar.base.jobstat[l].lvl)+#0#0#0#1#0;
            end;

            for p:=k to 3 do begin //empty slot
                a:=a+#$C1#$79#$00#$00#$C1#$79#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF;
            end;

            inc(c);
            end else a:=a+#$C1#$79#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$00#$00#$00#$00
                    +#$C1#$79#$00#$00#$C1#$79#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$C1#$79#$00#$00
                    +#$C1#$79#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$FF#$C1#$79#$00#$00#$C1#$79#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$FF#$C1#$79#$00#$00#$C1#$79#$00#$00#$00#$00#$00#$00
                    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF;
        end else begin
            a:=a+#$C1#$79#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$FF#$FF#$FF
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$00#$00#$00#$00
              +#$C1#$79#$00#$00#$C1#$79#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF#$C1#$79#$00#$00
              +#$C1#$79#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$FF#$C1#$79#$00#$00#$C1#$79#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$FF#$C1#$79#$00#$00#$C1#$79#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$FF;
        end;
    end;

    move(c,a[9],2);
    c:=length(a);
    move(c,a[1],4);
    sendtoplayer(x,a);
    end;

    sendtoplayer(x,#$08#$00#$00#$00#$0E#$1F#$00#$00);
end;

procedure invitePlayerToParty(x:integer;s:ansistring);
var id:dword;
    i,l:integer;
    a:ansistring;
begin
    move(s[9],id,4); //copy the id
    for i:=0 to srvcfg.MaxPlayer-1 do
        if player[i].GID = id then begin
            //this is the target player send the invitation
            a:=#$48#$00#$00#$00#$0E#$06#$04#$00#$AB#$2E#$00#$00#$00#$00#$00#$00
              +#$0D#$00#$85#$03#$DC#$A6#$9B#$00#$00#$00#$00#$00#$04#$00#$00#$00
              +GetCharNameWithMagic(x,$EF59,$d5)+GetCharNameWithMagic(x,$EF59,$d5);
            move(player[x].partyid,a[9],4);
            move(player[x].gid,a[$15],4);
            l:=length(a);
            move(l,a[1],4);
            //must put the name
            sendtoplayer(i,a);
        end;
end;

procedure GetPartyID(x:integer;s:ansistring);
var i:integer;
    id:dword;
    a:ansistring;
begin
    move(s[1],id,4);
    if byte(s[5]) > 0 then begin //item belong to the player id
        for i:=0 to srvcfg.MaxPlayer-1 do
            if player[i].GID = id then begin
                a:=#$50#$00#$00#$00#$04#$15#$44#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$B8#$00#$00#$00#$02#$00#$00#$00#$06#$00#$a9#$11
                +#$00#$00#$00#$00#$f8#$0c#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$EF#$5C#$00#$00#$53#$65#$74#$50#$61#$72#$74#$79#$49#$64#$00#$00;
                move(player[x].gid,a[$9],4);
                move(s[1],a[$15],12);
                move(player[i].partyid,a[$25],4);
                sendtoplayer(x,a);
                break;
            end;
    end;
end;

procedure SendParty(x:integer);
var s,a:ansistring;
    i,c,k:integer;
begin                                 //party id here
    s:=#$54#$01#$00#$00#$0E#$02#$04#$00#$F8#$0C#$00#$00#$00#$00#$00#$00#$0D#$00#$53#$01#0#0#0#0#0#0#0#0#4#0#0#0#0#0#0#0;
    move(player[x].partyid,s[9],4);
    if (player[x].partyid < 1) and (player[x].partyid > 200) then
        debug(2,'SendParty: error party id '+inttostr(player[x].partyid));
    c:=partys[player[x].partyid].leader;
    //receiving player data
    move(player[c].gid,s[$15],4);
    //max 4 player entry
    c:=0;
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (playerok(i)) and (player[x].partyid = player[i].partyid) then begin
        //for now always 1 player

        //fill in entry
        inc(c);
        a:=#0#0#0#0#0#0#0#0#4#0#0#0;
        move(player[i].gid,a[$1],4);
        s:=s+a;
        a:=player[i].aname+#0;
        s:=s+TextToUni(a,$D863,$a9);
        s:=s+GetCharNameWithMagic(i,$D863,$a9);
        a:=#0#0#0#0;
        move(player[i].maxhp,a[1],4);
        k:=player[i].activechar.base.job;
        k:=player[i].activechar.base.jobstat[k].lvl;
        s:=s+chr(k)+#0    //02 = leader? 01 = normal? or mabe lvl?
            +chr(player[i].activeChar.base.job)+chr(player[i].activeChar.base.subjob) //job + sub
            +chr(c-1)+#0#$fb#$01#$ff#0#0#2+a+a+a
            +#0#0#0#0#$6a#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
        if c = 4 then break;
    end;
    for i:=c to 3 do
        //empty entry
            s:=s+#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$0#$00#$CA#$D8#$00#$00#$CA#$D8#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$FF#$FF#$FF#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#0#0#0#0;

    s[$21]:=ansichar(c);
    c:=length(s);
    move(c,s[1],4);
    sendtoplayer(x,s);
end;

procedure AcceptParty(x:integer; s:ansistring);
var id:dword;
    i:integer;
    a,b:ansistring;
begin
    move(s[9],id,4);
    if id > 200 then exit;
    if partys[id].used =1 then begin
        leaveparty(x);
        partys[player[x].partyid].used:=0;
        player[x].partyid := id;

        for i:=0 to srvcfg.MaxPlayer-1 do
        if (playerok(i)) and (player[i].partyid = player[x].partyid) then begin
            SendParty(i);

            {sega send this to all party player instead
            000000: 84 00 00 00 0E 00 04 00 DC A6 9B 00 00 00 00 00     �.......ܦ�.....
            000010: 04 00 00 00 01 00 00 00 02 00 00 00 00 00 00 00     ................
            000020: 06 00 00 00 FF FF FF FF FC CC 00 00 4D 00 49 00     ....������..M.I.
            000030: 55 00 52 00 45 00 54 00 48 00 00 00 FD CC 00 00     U.R.E.T.H...��..
            000040: 43 00 65 00 72 00 65 00 7A 00 61 00 00 00 00 00     C.e.r.e.z.a.....
            000050: 00 00 00 00 00 00 00 00 00 00 00 00 FB 00 00 00     ............�...
            000060: CA 00 00 00 CA 00 00 00 CA 00 00 00 6A 00 01 FF     �...�...�...j..�
            000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
            000080: 00 00 00 00     ....   }

        end;

        a:=#$58#$00#$00#$00#$0E#$0D#$04#$00+UniToUniMagic(partys[id].name,$9789,$e3)
          +UniToUniMagic(partys[id].pass,$9789,$e3)+UniToUniMagic(partys[id].comment,$9789,$e3)
          +partys[id].options;

        i:=length(a);
        move(i,a[1],4);

        //should send to party
        sendtoplayer(x,a);

        i:=partys[id].leader;
        player[x].TeamID:=player[i].TeamID;
        if player[i].TeamID > -1 then begin
            a:=#$3C#$00#$00#$00#$0E#$25#$00#$00#$3F#$75#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$03#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
              +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
              +#$00#$00#$00#$00#$00#$00#$00#$00#$03#$00#$00#$00;
            move(player[x].gid,a[$19],4);
            move(team[player[x].TeamID].QuestId2,a[$9],4);
            sendtoplayer(x,a);

            sendtoplayer(x,partys[player[i].partyid].queststart);

            for i:=0 to 15 do begin
                player[x].FloorArea[i] :=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0;
                player[x].FloorObj[i]:='';
                player[x].FloorItems[i]:='';
            end;
        end;

        b:=#$24#$00#$00#$00#$0E#$4F#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
            +#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
            +#$01#$00#$00#$00;
        for i:=0 to srvcfg.MaxPlayer-1 do
        if (playerok(i)) and (player[i].partyid = player[x].partyid) then begin
            move(player[i].gid,b[9],4);
            sendtoparty(x,b);
        end;
        RefreshInTeamCount(player[x].teamid);
    end;
end;

{party player left
000000: 20 00 00 00 0E 01 00 00 DC A6 9B 00 00 00 00 00      .......ܦ�.....
000010: 04 00 00 00 56 5F 9C 00 00 00 00 00 04 00 00 00     ....V_�.........
 }

procedure LeaveParty(x:integer);
var p,i:integer;
    s:ansistring;
begin
    p:=player[x].partyid;

    if partys[p].leader = x then begin
        //the leader is leaving
        //find the next leader
        for i:=0 to srvcfg.MaxPlayer-1 do
            if playerok(i) and (player[i].partyid = player[x].partyid) and (i <> x) then begin
                //new leader
                partys[p].leader := i;
                break;
            end;
    end;

    if partys[p].leader = x then begin
        //no new leader destroy the party
        partys[p].used:=0;
        i:=player[x].TeamID;
        if i>-1 then begin
        if team[i].p1 = p then team[i].p1:=0;
        if team[i].p2 = p then team[i].p2:=0;
        if team[i].p3 = p then team[i].p3:=0;
        end;
    end else begin

    //remove player
    s:=#$20#$00#$00#$00#$0E#$01#$00#$00#$DC#$A6#$9B#$00#$00#$00#$00#$00
        +#$04#$00#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#$04#$00#$00#$00;
    move(player[x].gid,s[9],4);
    move(player[partys[p].leader].gid,s[$15],4);
    sendtoparty(x,s);

    //set new party for leaving player
    player[x].partyid:=GetNextPartyID(x);
    SendParty(x);
    end;

    if player[x].room >= Room_Team then begin
        //go to lobby
    sendtoplayer(x,#$0C#$00#$00#$00#$0E#$13#$00#$00#$00#$00#$00#$00);
    GoLobby(x,2);

    sendtoplayer(x,#$14#$00#$00#$00#$0E#$1A#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$00#$00#$00#$00);
    end;

    i:=player[x].TeamID;
    player[x].TeamID:=-1;

    TestTeam(i); //clean it if empty
    RefreshInTeamCount(i);
end;

procedure KickPlayerFromParty(x:integer;s:ansistring);
var id:dword;
    p,i:integer;
begin
    p:=player[x].partyid;
    move(s[9],id,4);
    if x <> partys[p].leader then exit; //not leader
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].GID = id) and playerok(i) then
        begin
            if player[i].partyid = p then LeaveParty(i); //make it leave
        end;

end;

function partycount(id:integer):integer;
var i:integer;
begin
    result:=0;
    for i:=0 to srvcfg.MaxPlayer-1 do
    if playerok(i) and (player[i].partyid = id) then inc(result);
end;

procedure SetPartyLeader(x:integer;id:dword);
var p,i:integer;
    s:ansistring;
begin
    if partys[player[x].partyid].leader <> x then exit; //not the leader
    p:=player[x].partyid;
    //find the player and set it as leader
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].GID = id) and playerok(i) then
        begin
            partys[p].leader:=i;
            break;
        end;
    //notify the party
    s:=#$14#$00#$00#$00#$0E#$0F#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00;
    move(player[partys[p].leader].gid,s[9],4);
    sendtoparty(x,s);
end;

procedure DisbandParty(x:integer);
var p,i:integer;
begin
    //the client send his party id but not needed since he can only disband his party
    if partys[player[x].partyid].leader <> x then exit; //not the leader

    //clean party
    p:=player[x].partyid;


    //set a new party for eatch player
    for i:=0 to srvcfg.MaxPlayer-1 do
        if (player[i].partyid = p) and playerok(i) then begin
            player[i].partyid:=GetNextPartyID(i);
            SendParty(i);
            player[x].TeamID:=-1;
        end;

    partys[p].used:=0;

end;

procedure GetPartyList(x:integer;s:ansistring);
var i:integer;
    a:ansistring;
begin
    a:=#0#0#0#0#0#0#0#0#0#0#0#0;
    {partys[2].used:=1;
    partys[2].name:='a'#0'b'#0;
    partys[2].pass:=#0#0;
    partys[2].options:=#1#44#0#1#0#0#0#0#0#0#0#0;    }
    for i:=1 to 200 do
        if partys[i].used = 1 then
        if player[partys[i].leader].TeamID <> -1 then
        // also discard your own team
        if player[x].partyid <> i then
         begin
            a:=a+chr(i)+#0#0#0#0#0#0#0#0#0#0#0;

        end;
    PartyQuerryInfo1(x,a);
end;

function WideansistringCompare(p1,p2:pwidechar;si:integer):boolean;
var i:integer;
begin
    result:=true;
    i:=0;
    while i < si do begin
        if p1[i] <> p2[i] then begin
            result:=false;
            break;
        end;
        if p1[i] = #0 then break;
        inc(i);
    end;
end;

procedure JoinGame(x:integer;s:ansistring);
var id:integer;
begin
    move(s[$15],id,4);
    if id > 200 then exit;
    if partys[id].used = 0 then exit;
    if WideansistringCompare(@s[$21],@partys[id].pass[1],length(partys[id].pass) div 2) then begin
        //good
        //leave party if needed
        if player[x].partyid <> 0 then LeaveParty(x);

        //join party
        AcceptParty(x,copy(s,13,16));

    end else begin
        //bad pass
        sendtoplayer(x,#$0C#$00#$00#$00#$0B#$0F#$00#$00#$07#$00#$00#$00);
    end;


end;




end.
