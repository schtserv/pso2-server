unit UInventory;

interface

uses windows,types,sysutils,UPlayer;
type
    TItemVSID = record
        id:integer;
        item:string[8];
    end;
    TStorageInfo = record
        max,count:dword;
        id,tipe,unk,aviable:byte;
    end;

procedure SendPalette(x:integer);
procedure AddItemToInv(x:integer;it:ansistring);
procedure SetQuickPalette(x:integer;s:ansistring);
procedure LoadItemsDesc();
procedure SendInvetory(x:integer);
procedure GetItemDescription(x:integer;s:ansistring);
procedure SetWeaponPalette(x:integer;s:ansistring);
procedure SendFullPalette(x:integer);
procedure SavePalette2(x:integer;s:ansistring);
procedure UseWeaponPalette(x,i:integer);
procedure UseSubPalette(x,i:integer);
procedure sendArmor(x:integer);
procedure SendItemsName(x,c,si:integer;p:pansichar);
procedure PickupItem(x:integer;id:pdword);
procedure UpdateMesetas(x:integer);
Function AddToStorage(x:integer;itm:pinventoryitem;var bank:array of tinventoryitem;count:psmallint;max:integer):integer;
function FindItem(t,s,u,e:word):integer;
function removeFromStorage(x:integer;id1,id2,qty:dword;var bank:array of tinventoryitem;count:psmallint;max:integer):integer;
procedure EquipSomething(x:integer; id1,id2,slot:pdword);
procedure unEquipSomething(x:integer; id1,id2,slot,slot2:pdword);
function GetPlayerEquiped(x:integer):ansistring;
procedure SendStorage(x:integer);
procedure DepositeToBank(x:integer;s:ansistring);
procedure WidrawFromBank(x:integer;s:ansistring);
procedure DepositeFromBank(x:integer;s:ansistring);
procedure bankmesetas(x:integer;s:ansistring);
procedure FashionEdit(x:integer;s:ansistring);
procedure UnlockFashionMenu(x:integer);
procedure SetAccessoryFashion(x:integer;s:ansistring);
procedure TrashStuff(x:integer;s:ansistring);
procedure TrashStuffBank(x:integer;s:ansistring);
procedure LoadOutfit();


const
    ItemVsIdCount = 28;
    ItemsVSID:array[0..28] of TItemVSID = (
     (id:$06c4;item:#$02#$00#$02#$00#$00#$00#$7B#$03),
     (id:$06ce;item:#$02#$00#$02#$00#$00#$00#$0D#$00 ),
     (id:$000a;item:#$02#$00#$01#$00#$00#$00#$07#$00),  //ok
     (id:$0009;item:#$02#$00#$01#$00#$00#$00#$06#$00),  //done
     (id:$000d;item:#$02#$00#$01#$00#$00#$00#$0A#$00),
     (id:$000f;item:#$02#$00#$01#$00#$00#$00#$0C#$00),
     (id:$0010;item:#$02#$00#$01#$00#$00#$00#$0d#$00),
     (id:$001c;item:#$02#$00#$01#$00#$00#$00#$19#$00),
     (id:$0212;item:#$02#$00#$01#$00#$00#$00#$0A#$01),
     (id:$0218;item:#$02#$00#$01#$00#$00#$00#$44#$04),
     (id:$0217;item:#$02#$00#$01#$00#$00#$00#$24#$02),
     (id:$005e;item:#$02#$00#$01#$00#$00#$00#$42#$04),
     (id:$0212;item:#$02#$00#$01#$00#$00#$00#$0A#$01),
     (id:$0054;item:#$02#$00#$01#$00#$00#$00#$23#$02),
     (id:$002c;item:#$02#$00#$01#$00#$00#$00#$22#$02),
     (id:$0036;item:#$02#$00#$01#$00#$00#$00#$43#$04),

     (id:$2719;item:#$02#$00#$02#$00#$00#$00#$07#$00),
     (id:$271d;item:#$02#$00#$02#$00#$00#$00#$0b#$00),
     (id:$271e;item:#$02#$00#$02#$00#$00#$00#$0c#$00),
     (id:$271f;item:#$02#$00#$02#$00#$00#$00#$0d#$00),
     (id:$2722;item:#$02#$00#$02#$00#$00#$00#$10#$00),
     (id:$2730;item:#$02#$00#$02#$00#$00#$00#$1e#$00),
     (id:$273c;item:#$02#$00#$02#$00#$00#$00#$79#$03),
     (id:$2750;item:#$02#$00#$02#$00#$00#$00#$17#$07),
     (id:$275a;item:#$02#$00#$02#$00#$00#$00#$7a#$03),
     (id:$276f;item:#$02#$00#$02#$00#$00#$00#$16#$07),
     (id:$2a12;item:#$02#$00#$02#$00#$00#$00#$9c#$01),
     (id:$2a17;item:#$02#$00#$02#$00#$00#$00#$7b#$03),
     (id:$2a18;item:#$02#$00#$02#$00#$00#$00#$18#$07)
    )  ;

    CastIDCount = 35;
    CastID:array[0..35] of TItemVSID = (
    (id:$c383;item:#$06#$00#$02#$00#$00#$00#$41#$00), //femal body
    (id:$c383;item:#$06#$00#$04#$00#$00#$00#$41#$00), //femal arm
    (id:$c383;item:#$06#$00#$06#$00#$00#$00#$41#$00), //femal leg

    (id:$9c6b;item:#$06#$00#$01#$00#$00#$00#$3b#$00), //male body
    (id:$9c6b;item:#$06#$00#$03#$00#$00#$00#$3b#$00), //male arm
    (id:$9c6b;item:#$06#$00#$05#$00#$00#$00#$3b#$00), //male leg

    (id:$c371;item:#$06#$00#$02#$00#$00#$00#$3F#$00), //femal body
    (id:$c371;item:#$06#$00#$04#$00#$00#$00#$3F#$00), //femal arm
    (id:$c371;item:#$06#$00#$06#$00#$00#$00#$3F#$00), //femal leg

    (id:$9c43;item:#$06#$00#$01#$00#$00#$00#$02#$00), //male body
    (id:$9c43;item:#$06#$00#$03#$00#$00#$00#$02#$00), //male arm
    (id:$9c43;item:#$06#$00#$05#$00#$00#$00#$02#$00),  //male leg

    (id:$c37d;item:#$06#$00#$02#$00#$00#$00#$40#$00), //femal body
    (id:$c37d;item:#$06#$00#$04#$00#$00#$00#$40#$00), //femal arm
    (id:$c37d;item:#$06#$00#$06#$00#$00#$00#$40#$00),  //femal leg

    (id:$c352;item:#$06#$00#$02#$00#$00#$00#$01#$00), //femal body
    (id:$c352;item:#$06#$00#$04#$00#$00#$00#$01#$00), //femal arm
    (id:$c352;item:#$06#$00#$06#$00#$00#$00#$01#$00),  //femal leg

    (id:$c353;item:#$06#$00#$02#$00#$00#$00#$02#$00), //femal body
    (id:$c353;item:#$06#$00#$04#$00#$00#$00#$02#$00), //femal arm
    (id:$c353;item:#$06#$00#$06#$00#$00#$00#$02#$00),  //femal leg

    (id:$c354;item:#$06#$00#$02#$00#$00#$00#$03#$00), //femal body
    (id:$c354;item:#$06#$00#$04#$00#$00#$00#$03#$00), //femal arm
    (id:$c354;item:#$06#$00#$06#$00#$00#$00#$03#$00),  //femal leg

    (id:$9c41;item:#$06#$00#$01#$00#$00#$00#$00#$00), //male body
    (id:$9c41;item:#$06#$00#$03#$00#$00#$00#$00#$00), //male arm
    (id:$9c41;item:#$06#$00#$05#$00#$00#$00#$00#$00),  //male leg

    (id:$9c42;item:#$06#$00#$01#$00#$00#$00#$01#$00), //male body
    (id:$9c42;item:#$06#$00#$03#$00#$00#$00#$01#$00), //male arm
    (id:$9c42;item:#$06#$00#$05#$00#$00#$00#$01#$00),  //male leg

    (id:$9c57;item:#$06#$00#$01#$00#$00#$00#$39#$00), //male body
    (id:$9c57;item:#$06#$00#$03#$00#$00#$00#$39#$00), //male arm
    (id:$9c57;item:#$06#$00#$05#$00#$00#$00#$39#$00),  //male leg

    (id:$9c63;item:#$06#$00#$01#$00#$00#$00#$3a#$00), //male body
    (id:$9c63;item:#$06#$00#$03#$00#$00#$00#$3a#$00), //male arm
    (id:$9c63;item:#$06#$00#$05#$00#$00#$00#$3a#$00)  //male leg

    );

var ExtraID:array of TItemVSID;
    ExtraIDCount:integer;


implementation

uses fmain, UPsoUtil, UQuestBoard, UDB, UMonster, UForest, UMag,
  clientorder;

procedure LoadOutfit();
var f,i:integer;
begin
    f:=fileopen('data\outfits.dat',$40);
    ExtraIDCount := fileseek(f,0,2) div $10;
    fileseek(f,0,0);
    setlength(ExtraID,ExtraIDcount);
    for i:=0 to ExtraIDCount-1 do begin
        fileread(f,ExtraID[i].item[1],8);
        fileread(f,ExtraID[i].id,4);
        fileseek(f,4,1);
    end;

    fileclose(f);
end;

function GetPlayerEquiped(x:integer):ansistring;
var s:ansistring;
    l,i:integer;
    id1,id2:dword;
begin
    result:='';
    i:=player[x].activeChar.weppal;
    move(player[x].activeChar.palette[i*$40],id1,4);
    move(player[x].activeChar.palette[4+(i*$40)],id2,4);
    //find item in inventory
    for l:=0 to player[x].activeChar.BankCount-1 do
        if (player[x].activeChar.PersonalBank[l].id1 = id1) and (player[x].activeChar.PersonalBank[l].id2 = id2) then begin
            //send to all the new equiped weapon
            s:=#$4C#$00#$00#$00#$0F#$21#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#4#0#0#0;
            setlength(s,$4c);
            safemove(player[x].gid,s,9,4); //player id
            safemove(player[x].activeChar.PersonalBank[l],s,$15,$38);
            result:=s;
        end;
end;

procedure UseWeaponPalette(x,i:integer);
var id1,id2,pet:dword;
    l:integer;
    s:ansistring;
begin
    player[x].activeChar.weppal:=i;

    //get item id from palette data
    move(player[x].activeChar.palette[i*$40],id1,4);
    move(player[x].activeChar.palette[4+(i*$40)],id2,4);
    move(player[x].activeChar.palette[$24+(i*$40)],pet,4);


     //if pet test to clean it
    if player[x].petloaded = 1 then begin
    s:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$B2#$00#$00#$00#$01#$00#$00#$00#$06#$00#$a9#$11;
    move(player[x].gid,s[$9],4);
    move(player[x].gid,s[$15],4);
    sendtolobby(player[x].room,-1,s);
    player[x].petloaded:=0;
    end;

    //find item in inventory
    for l:=0 to player[x].activeChar.BankCount-1 do
        if (player[x].activeChar.PersonalBank[l].id1 = id1) and (player[x].activeChar.PersonalBank[l].id2 = id2) then begin
            //send to all the new equiped weapon
            s:=#$4C#$00#$00#$00#$0F#$21#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#4#0#0#0;
            setlength(s,$4c);
            safemove(player[x].gid,s,9,4); //player id
            safemove(player[x].activeChar.PersonalBank[l],s,$15,$38);
            SendToLobby(player[x].room,-1,s);



            //if must creat pet do it here
            if player[x].activechar.base.job = Job_Summoner then
            begin
                s:='';
                if (player[x].activeChar.PersonalBank[l].itemtype = $01)
                and (player[x].activeChar.PersonalBank[l].ItemSub = $12) then begin
                    if (player[x].activeChar.PersonalBank[l].ItemEntry = $00) then s:=Pets[0].packet;
                end;
                if s <> '' then begin
                safemove(player[x].gid,s,$9,4);
                safemove(player[x].gid,s,$ad,4);
                safemove(pet,s,$bd,4);
                safemove(player[x].gid,s,$c1,4);
                safemove(player[x].gid,s,$cd,4);
                sendtolobby(player[x].room,-1,s);

                //trigger
                s:=#$20#$00#$00#$00#$04#$2B#$40#$00#$00#$00#$00#$00#$00#$00#$00#$00
	            +#$00#$00#$00#$00#$B2#$00#$00#$00#$01#$00#$00#$00#$06#$00#$a9#$11;
                safemove(player[x].gid,s,$15,4);
                sendtolobby(player[x].room,-1,s);

                //activate
                s:=#$48#$00#$00#$00#$04#$22#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$B2#$00#$00#$00#$01#$00#$00#$00#$06#$00#$a9#$11
                +#$07#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
                +#$00#$00#$00#$00#$00#$00#$00#$00;

                safemove(player[x].gid,s,$9,4);
                safemove(player[x].gid,s,$15,4);
                sendtolobby(player[x].room,-1,s);
                player[x].petloaded:=1;

                if player[x].pet_stunned[0] <> 0 then begin
                    //only 1 pet for now so test only first
                    s:=#$48#$00#$00#$00#$04#$24#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                      +#$04#$00#$00#$00#$19#$01#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11
                      +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$49#$E6#$02#$00
                      +#$FF#$FF#$FF#$FF#$8D#$3A#$3F#$CB#$00#$00#$00#$00#$04#$00#$00#$00
                      +#$00#$00#$00#$00#$00#$00#$00#$00;
                    safemove(player[x].gid,s,9,4);
                    safemove(player[x].gid,s,$15,4);
                    sendtolobby(player[x].room,-1,s);
                end;
                end;
            end;
            exit;
        end;
    //else send empty
    s:=#$4C#$00#$00#$00#$0F#$21#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#4#0#0#0;
    setlength(s,$4c);
    safemove(player[x].gid,s,9,4); //player id
    fillchar(s[$15],$38,0);
    SendToLobby(player[x].room,-1,s);
end;

procedure ChangeOutfit(x:integer);
var i:integer;
    s:ansistring;
begin
    //this is called to change the outfit in the basic char data
    if (player[x].activeChar.outfit.id1 = 0) and (player[x].activeChar.outfit.id2 = 0) then begin
        player[x].activeChar.base.outfitid:=$7530;

    end else begin
        //apply the proper id
        s:=#0#0#0#0#0#0#0#0;
        safemove(player[x].activeChar.outfit.itemtype,s,1,8);
        if player[x].activeChar.base.race <> Race_Cast then begin
        for i:=0 to ExtraIDCount-1 do begin
            if ExtraID[i].item = s then begin
                player[x].activeChar.base.outfitid:=ExtraID[i].id;
                //do color
                move(player[x].activeChar.outfit.count,player[x].activeChar.base.outfitcolor,6);
            end;
        end;
        for i:=0 to ItemVSIDCount do begin
            if ItemsVSId[i].item = s then begin
                player[x].activeChar.base.outfitid:=ItemsVSId[i].id;
                //do color
                move(player[x].activeChar.outfit.count,player[x].activeChar.base.outfitcolor,6);
            end;
        end;
        end else begin
        for i:=0 to ExtraIDCount-1 do begin
            if ExtraID[i].item = s then begin
                player[x].activeChar.base.outfitid:=ExtraID[i].id;
                //do color
                move(player[x].activeChar.outfit.count,player[x].activeChar.base.bodycolor,6);
            end;
        end;
        for i:=0 to CastIDCount do begin
            if CastID[i].item = s then begin
                player[x].activeChar.base.outfitid:=CastID[i].id;
                //do color
                move(player[x].activeChar.outfit.count,player[x].activeChar.base.bodycolor,6);
            end;
        end;
        move(player[x].activeChar.cast_arm.itemtype,s[1],8);
        for i:=0 to ExtraIDCount-1 do begin
            if ExtraID[i].item = s then begin
                player[x].activeChar.base.armid:=ExtraID[i].id;
                //do color
                move(player[x].activeChar.cast_arm.count,player[x].activeChar.base.armcolor,6);
            end;
        end;
        for i:=0 to CastIDCount do begin
            if CastID[i].item = s then begin
                player[x].activeChar.base.armid:=CastID[i].id;
                //do color
                move(player[x].activeChar.cast_arm.count,player[x].activeChar.base.armcolor,6);
            end;
        end;
        move(player[x].activeChar.cast_leg.itemtype,s[1],8);
        for i:=0 to ExtraIDCount-1 do begin
            if ExtraID[i].item = s then begin
                player[x].activeChar.base.legid:=ExtraID[i].id;
                //do color
                move(player[x].activeChar.cast_leg.count,player[x].activeChar.base.legcolor,6);
            end;
        end;
        for i:=0 to CastIDCount do begin
            if CastID[i].item = s then begin
                player[x].activeChar.base.legid:=CastID[i].id;
                //do color
                move(player[x].activeChar.cast_leg.count,player[x].activeChar.base.legcolor,6);
            end;
        end;

        end;

    end;
    player[x].maxhp:=GetPlayerMaxHp(x); //refresh max hp in case an armor change it
end;


procedure EquipSomething(x:integer; id1,id2,slot:pdword);
var i:integer;
    s:ansistring;
begin
    //when pso2 equip something the id is sent outside the normal inventory
    for i:=0 to player[x].activechar.BankCount-1 do
    if (player[x].activeChar.PersonalBank[i].id1 = id1^) and
        (player[x].activeChar.PersonalBank[i].id2 = id2^) then begin
        if slot^ = 3 then begin
            move(player[x].activeChar.PersonalBank[i],player[x].activeChar.outfit,$38);
            ChangeOutfit(x);
        end;
        if slot^ = 4 then move(player[x].activeChar.PersonalBank[i],player[x].activeChar.cast_arm,$38);
        if slot^ = 5 then move(player[x].activeChar.PersonalBank[i],player[x].activeChar.cast_leg,$38);
        if slot^ = 0 then move(player[x].activeChar.PersonalBank[i],player[x].activeChar.units[0],$38);
        if slot^ = 1 then move(player[x].activeChar.PersonalBank[i],player[x].activeChar.units[1],$38);
        if slot^ = 2 then move(player[x].activeChar.PersonalBank[i],player[x].activeChar.units[2],$38);



        //confirm it
        s:=#$5C#0#0#0#$f#9#4#0#0#0#0#0#0#0#0#0
            +#4#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 //items
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0
            +#0#0#0#0 //slot
            +#$e9#$3e#0#0#0#0#0#0#0#0#0#0;
        safemove(player[x].gid,s,9,4);
        safemove(player[x].activeChar.PersonalBank[i],s,$15,$38);
        s[$4d]:=ansichar(slot^);
        sendtolobby(player[x].room,-1,s);

        //remove it from the inv
        move(player[x].activeChar.PersonalBank[i+1],player[x].activeChar.PersonalBank[i],(player[x].activechar.BankCount-i-1)*$38);
        dec(player[x].activechar.BankCount);

        player[x].maxhp:=GetPlayerMaxHp(x); //refresh max hp in case an armor change it
        exit;
    end;

end;

procedure unEquipSomething(x:integer; id1,id2,slot,slot2:pdword);
var i:integer;
    s:ansistring;
begin
    //when pso2 unequip something the id is sent in the normal inventory
    i:=player[x].activechar.BankCount;
    //confirm it
        s:=#$54#0#0#0#$f#$b#0#0#0#0#0#0#0#0#0#0
            +#4#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0 //items
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
            +#0#0#0#0#0#0#0#0
            +#0#0#0#0 //slot
            +#0#0#0#0; //unknown
        safemove(player[x].gid,s,9,4);

        s[$4d]:=ansichar(slot^);
        s[$51]:=ansichar(slot2^);

    if slot^ = 3 then begin
        if (player[x].activeChar.outfit.id1 = 0) and (player[x].activeChar.outfit.id2 = 0) then exit;
        move(player[x].activeChar.outfit,player[x].activeChar.PersonalBank[i],$38);
        safemove(player[x].activeChar.outfit,s,$15,$38);
        fillchar(player[x].activeChar.outfit,$38,0);
        ChangeOutfit(x);
    end;
    if slot^ = 4 then begin
        if (player[x].activeChar.cast_arm.id1 = 0) and (player[x].activeChar.cast_arm.id2 = 0) then exit;
        move(player[x].activeChar.cast_arm,player[x].activeChar.PersonalBank[i],$38);
        safemove(player[x].activeChar.cast_arm,s,$15,$38);
        fillchar(player[x].activeChar.cast_arm,$38,0);
    end;
    if slot^ = 5 then begin
        if (player[x].activeChar.cast_leg.id1 = 0) and (player[x].activeChar.cast_leg.id2 = 0) then exit;
        move(player[x].activeChar.cast_leg,player[x].activeChar.PersonalBank[i],$38);
        safemove(player[x].activeChar.cast_leg,s,$15,$38);
        fillchar(player[x].activeChar.cast_leg,$38,0);
    end;
    if slot^ = 0 then begin
        if (player[x].activeChar.units[0].id1 = 0) and (player[x].activeChar.units[0].id2 = 0) then exit;
        move(player[x].activeChar.units[0],player[x].activeChar.PersonalBank[i],$38);
        safemove(player[x].activeChar.units[0],s,$15,$38);
        fillchar(player[x].activeChar.units[0],$38,0);
    end;
    if slot^ = 1 then begin
        if (player[x].activeChar.units[1].id1 = 0) and (player[x].activeChar.units[1].id2 = 0) then exit;
        move(player[x].activeChar.units[1],player[x].activeChar.PersonalBank[i],$38);
        safemove(player[x].activeChar.units[1],s,$15,$38);
        fillchar(player[x].activeChar.units[1],$38,0);
    end;
    if slot^ = 2 then begin
        if (player[x].activeChar.units[2].id1 = 0) and (player[x].activeChar.units[2].id2 = 0) then exit;
        move(player[x].activeChar.units[2],player[x].activeChar.PersonalBank[i],$38);
        safemove(player[x].activeChar.units[2],s,$15,$38);
        fillchar(player[x].activeChar.units[2],$38,0);
    end;

    inc(player[x].activechar.BankCount);
    sendtolobby(player[x].room,-1,s);

    player[x].maxhp:=GetPlayerMaxHp(x); //refresh max hp in case an armor change it

end;


procedure sendArmor(x:integer);
var s,a:ansistring;
    i,l,k:integer;
    id1,id2:dword;
begin
    {
     Xor: CF06  Sub: C5
      Xor: CF16  Sub: D5
      Xor: CF26  Sub: E5
      Xor: CF36  Sub: F5
      Xor: CF46  Sub: 85
      Xor: CF56  Sub: 95
      Xor: CF66  Sub: A5
      Xor: CF76  Sub: B5
      Xor: CF86  Sub: 45
      Xor: CF96  Sub: 55
      Xor: CFA6  Sub: 65
      Xor: CFB6  Sub: 75
      Xor: CFC6  Sub: 05
      Xor: CFD6  Sub: 15
      Xor: CFE6  Sub: 25
      Xor: CFF6  Sub: 35
    }
    //todo: find the layout and add the proper storage/handling
    //code 00, 01, 02 = unit?
    s:=#$CC#$00#$00#$00#$0F#$0C#$04#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
    +#$04#$00#$00#$00#$C1#$CF#$00#$00;

    a:=#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
    +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
    +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
    +#0#0#0#0#0#0#0#0#0#0#0#0;
    k:=0;
    //do units
    for l:=0 to 2 do
    if (player[x].activeChar.units[l].id1 <> 0) and (player[x].activeChar.units[l].id2 <> 0) then begin
        a[$39]:=ansichar(l);
        safemove(player[x].activeChar.units[l],a,1,$38);
        s:=s+a;
        inc(k);
    end;

    if (player[x].activeChar.outfit.id1 <> 0) and (player[x].activeChar.outfit.id2 <> 0) then begin
        a[$39]:=#3;
        safemove(player[x].activeChar.outfit,a,1,$38);
        s:=s+a;
        inc(k);
    end;

    if (player[x].activeChar.Cast_arm.id1 <> 0) and (player[x].activeChar.Cast_arm.id2 <> 0) then begin
        a[$39]:=#4;
        safemove(player[x].activeChar.Cast_arm,a,1,$38);
        s:=s+a;
        inc(k);
    end;

    if (player[x].activeChar.Cast_leg.id1 <> 0) and (player[x].activeChar.Cast_leg.id2 <> 0) then begin
        a[$39]:=#5;
        safemove(player[x].activeChar.Cast_leg,a,1,$38);
        s:=s+a;
        inc(k);
    end;

    a:=#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00   //code 03 = armor? //equiped weapon
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$09#$00#$00#$00;  //code 09 = weapon


    i:=player[x].activechar.weppal;
    move(player[x].activeChar.palette[i*$40],id1,4);
    move(player[x].activeChar.palette[4+(i*$40)],id2,4);
    for l:=0 to player[x].activeChar.BankCount-1 do
        if (player[x].activeChar.PersonalBank[l].id1 = id1) and (player[x].activeChar.PersonalBank[l].id2 = id2) then begin
            //send to all the new equiped weapon
            safemove(player[x].activeChar.PersonalBank[l],a,$1,$38);
            s:=s+a;
            inc(k);
            break;
        end;

    a:=#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00
    +#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;

    safemove(player[x].activeChar.weppal,a,$1,1);
    safemove(player[x].activeChar.weppal,a,$29,1);
    s:=s+a;

    safemove(player[x].gid,s,9,4);
    l:=length(s);
    safemove(l,s,1,4);

    a:=getmagic(k,$CF86,$45);
    safemove(a[1],s,$15,4);

    SendToplayer(x,s);


    UseWeaponPalette(x,player[x].activeChar.weppal);
    DisplayMag(x);
end;

procedure UseSubPalette(x,i:integer);
begin
    player[x].activeChar.subpal:=i;
end;

procedure SendPalette(x:integer);
var s:ansistring;
begin
    s:=#$f4#4#0#0#$21#$01#$00#$00+chr(player[x].activeChar.weppal)+#$00#$00#$00+chr(player[x].activeChar.subpal)+#$00#$00#$00
    +#0#$00#$00#$00;
    setlength(s,$4f4);
    safemove(player[x].activechar.palette[0],s,$15,$4e0);

    sendtoplayer(x,s);
end;

procedure SetQuickPalette(x:integer;s:ansistring);
begin
    move(s[9],player[x].activechar.palette[$180],$368);

    SendPalette(x);
end;

procedure SetWeaponPalette(x:integer;s:ansistring);
begin
    move(s[13],player[x].activechar.palette[$0],$180);

    SendPalette(x);
    UseWeaponPalette (x,player[x].activeChar.weppal);
end;

procedure SendPalette2(x:integer);
var s:ansistring;
begin
    s:=#$c8#8#0#0#$21#$0f#$00#$00;
    setlength(s,$8c8);
    safemove(player[x].activechar.palette2[0],s,$9,$8c0);

    sendtoplayer(x,s);
end;

procedure SavePalette2(x:integer;s:ansistring);
begin
    move(s[9],player[x].activechar.palette2[0],$680);

    SendPalette2(x);
end;

procedure SendFullPalette(x:integer);
var s:ansistring;
begin
    s:=#$B4#$0D#$00#$00#$21#$03#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
    setlength(s,$db4);
    safemove(player[x].activechar.palette[0],s,$15,$4e0);
    safemove(player[x].activechar.palette2[0],s,$4f5,$8c0);
    sendtoplayer(x,s);
end;




procedure LoadItemsDesc();
var f,i:integer;
begin
    f:=fileopen('data\items.dat',$40);
    i:=fileseek(f,0,2);
    fileseek(f,0,0);

    fileread(f,items[0],i);
    fileclose(f);
    itemcount:=i div sizeof(items[0]);

    for i:=0 to itemcount-1 do
        form1.ListBox2.Items.Add(items[i].name);
end;

procedure GetItemDescription(x:integer;s:ansistring);
var i,l:integer;
    a,b:ansistring;
begin
    {Xor: B10E  Sub: B2
    Xor: B14E  Sub: 72
    Xor: B18E  Sub: 32
    }
    {
    i:=finditem(pword(@s[9])^,pword(@s[$b])^,pword(@s[$d])^,pword(@s[$f])^);
    if i = -1 then exit;
    l:=0;
    while items[i].desc[l] <> #0 do inc(l);
    if l = 0 then exit;
    inc(l);
    setlength(a,l*2);
    move(items[i].desc[0],a[1],l*2);
    if l and 1 = 1 then a:=a+#0#0;    }
    //temp test
    a:='';
    for l:=9 to 24 do a:=a+inttohex(byte(s[l]),2)+' ';
    b:='';
    for l:=1 to 24 do
        b:=b+a[l]+#0;
    a:=b+#0#0#0#0;
    l:=26;

    a:=#$54#$00#$00#$00#$0F#$1D#$04#$00#$01#$00#$00#$00+copy(s,9,8)+getmagic(l,$B14E,$72)+a ;
    l:=length(a);
    safemove(l,a,1,4);
    sendtoplayer(x,a);
end;


function FindItem(t,s,u,e:word):integer;
var l:integer;
begin
    result:=-1; //not found
    for l:=0 to itemcount-1 do
        if (items[l].ItemSub = s) and (items[l].ItemType = t)
            and (items[l].unk = u) and (items[l].ItemEntry = e) then begin
            result:=l;
            break;
        end;
end;

procedure SendItemsName(x,c,si:integer;p:pansichar);
var ids,a,len:ansistring;
    txt,b:widestring;
    i,l,c2:integer;
    itm:^TInventoryItem;
begin
    ids:='';
    txt:='';
    len:='';
    c2:=0;
    for i:=0 to c-1 do begin
        itm:=@p[i*si];
        for l:=0 to itemcount-1 do
            if (items[l].ItemSub = itm.ItemSub) and (items[l].ItemType = itm.ItemType)
                and (items[l].unk = itm.unk) and (items[l].ItemEntry = itm.ItemEntry) then begin
                a:=#0#0#0#0#0#0#0#0;
                safemove(items[l].ItemType,a,1,8);
                ids:=ids+a;
                inc(c2);
                b:=items[l].name;
                txt:=txt+b;
                len:=len+chr(length(b));
                break;
            end;
    end;
    txt:=txt+#0;
    while length(len) and 3 > 0 do len:=len+#0;
    if c2 > 0 then begin
    //Xor: 9E22  Sub: 46
    setlength(a,length(txt)*2);
    safemove(txt[1],a,1,length(txt)*2);
    a:=#$74#$00#$00#$00#$0F#$30#$04#$00 + GetMagic(c2,$9e22,$46) + ids + GetMagic(length(txt),$9e22,$46) + a;
    if length(txt) and 1 = 1 then a:=a+#0#0;
    a:=a+GetMagic(c2,$9e22,$46) +len;
    l:=length(a);
    safemove(l,a,1,4);

    sendtoplayer(x,a);
    end;
end;


procedure SendInvetory(x:integer);
var s,a:ansistring;
    i:integer;
begin

    //Xor: 5533  Sub: 01
    //Xor: 5573  Sub: 41
    //xxxxXor: 55B3  Sub: 81
    //xxxxxxxXor: 55F3  Sub: C1

    SendItemsName(x,player[x].activechar.BankCount,$38,@player[x].activechar.personalbank[0]);

    s:=#$F8#$01#$00#$00#$0F#$0D#$04#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
	+#$04#$00#$00#$00+GetCharNameWithMagic(x,$5573,$41);
    move(player[x].gid,s[9],4);

    a:=#$68#$00#$00#$00#$00#$00#$00#$00#$32#$00#$00#$00+GetMagic(player[x].activechar.BankCount,$5573,$41);
    safemove(player[x].activechar.mesetas,a,1,4);
    s:=s+a;
    setlength(a,$38*player[x].activechar.BankCount);
    move(player[x].activechar.personalbank[0],a[1],$38*player[x].activechar.BankCount);
    s:=s+a;
    i:=length(s);
    safemove(i,s,1,4);
    sendtoplayer(x,s);

    

end;

function GetStackingMax(sub,unk,entry:integer):integer;
begin
    result:=10; //default max
    if sub = 1 then begin
        if entry = $b then result:=5; //Shiftaride - 5
        if entry = $c then result:=5; //Debandaride - 5
        if entry = $7 then result:=5; //Moon Atomizer - 5
        if entry = $8 then result:=5; //Star Atomizer - 5
    end;
    if sub = 2 then result:=999; //Advance Capsules and any quest-type items - 999
    if sub = 3 then result:=999; //Any support item(violet icon), like Grinder or Syntheser - 999

//Halfdoll - 1
//Scapedoll - 1

end;

Function AddToStorage(x:integer;itm:pinventoryitem;var bank:array of tinventoryitem;count:psmallint;max:integer):integer;
var i:integer;
begin
    result:=-1; //fail
    if (itm.ItemType = 3) and (GetStackingMax(itm.ItemSub,itm.unk ,itm.ItemEntry) > 1) then begin //stacking items
        for i:=0 to count^-1 do begin
            if (bank[i].ItemType=itm.ItemType) and (bank[i].ItemSub = itm.ItemSub)
                and (bank[i].unk=itm.unk) and (bank[i].ItemEntry = itm.ItemEntry) then begin
                    //add it to the stack
                    if max > 100 then begin
                      if bank[i].count+itm.count > 999 then exit; //storage max 999
                    end else if bank[i].count+itm.count > GetStackingMax(itm.ItemSub,itm.unk ,itm.ItemEntry) then exit; //maximum stack reatched
                    inc(bank[i].count, itm.count);
                    result:=i; //success
                    exit;
                end;
        end;
    end;

    if count^ < max then begin
        bank[count^]:=itm^;
        result:=count^;
        inc(count^);

    end;
end;

function removeFromStorage(x:integer;id1,id2,qty:dword;var bank:array of tinventoryitem;count:psmallint;max:integer):integer;
var i,l:integer;
begin
    result:=1; //fail
    for i:=0 to count^-1 do begin
        if (bank[i].id1 = id1) and (bank[i].id2 = id2) then begin
            if bank[i].ItemType = 3 then begin
                //stacking
                if bank[i].count >= qty then begin
                    dec(bank[i].count,qty);
                    result:=0;
                    if bank[i].count > 0 then exit; //we are done with this item
                end else exit; //fail
            end;

            //delete it
            for l:=i to count^-2 do
                bank[l]:=bank[l+1];
            result:=0;
            dec(count^);
            exit;
        end;
    end;
end;

procedure UpdateMesetas(x:integer);
var s:ansistring;
begin
    s:=#$10#$00#$00#$00#$0F#$14#$00#$00#$86#$04#$00#$00#$00#$00#$00#$00;
    safemove(player[x].activechar.mesetas,s,9,4);
    sendtoplayer(x,s);
end;

procedure PickupItem(x:integer;id:pdword);
var si,id2,m:pdword;
    i,l:integer;
    d:dword;
    s:ansistring;
begin
    i:=1;
    //find the item to pickup
    while i < length(player[x].FloorItems[player[x].Floor]) do begin
        si:=@player[x].FloorItems[player[x].Floor][i];
        id2:=@player[x].FloorItems[player[x].Floor][i+$3c];
        if id2^ = id^ then begin
            //right item
            if player[x].FloorItems[player[x].Floor][i+$14] = #$fe then begin //mesetas
                m:=@player[x].FloorItems[player[x].Floor][i+$1c]; //amount
                if form1.Memo8.Lines.IndexOf(player[x].username) > -1 then m^:=m^*10;
                inc(player[x].activeChar.Mesetas,m^);
                UpdateMesetas(x);
            end else begin
                //any other items
                s:=#$44#$00#$00#$00#$0F#$05#$00#$00#0#0#0#0#0#0#0#0
                +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
                +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
                +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
                +#1#0#0#0;
                //create the item
                safemove(player[x].FloorItems[player[x].Floor][i+$14],s,$11,$8); //copy item data
                safemove(player[x].FloorItems[player[x].Floor][i+$1c],s,$1b,$8); //copy item qty
                if s[$11] = #9 then safemove(player[x].FloorItems[player[x].Floor][i+$1c],s,$1a,$8); //copy lvl for disk
                d:=CreateItemID(x);
                safemove(d,s,9,4);
                safemove(player[x].activechar.charid,s,13,4);
                l:=AddToStorage(x,@s[9],player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);
                if l = -1 then exit; //error

                //copy the final data
                safemove(player[x].activechar.personalbank[l],s,$9,$8);
                sendtoplayer(x,s);

                //order item picked up?
                TestForOrderItemPickup(x,@player[x].activechar.personalbank[l]);
            end;

            //remove item from ground
            s:=#$20#$00#$00#$00#$04#$06#$40#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$B9#$00#$00#$00#$00#$00#$00#$00#$06#$00#$A9#$11;
            safemove(player[x].gid,s,9,4); //player id
            safemove(player[x].FloorItems[player[x].Floor][i+8],s,$15,8); //real id
            sendtolobby(player[x].room,-1,s);

            s:=#$1C#$00#$00#$00#$0F#$02#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00
                +#$04#$00#$00#$00#$02#$00#$00#$00#$01#$00#$00#$00;
            safemove(player[x].gid,s,9,4); //player id
            safemove(player[x].FloorItems[player[x].Floor][i+$3c],s,$15,4); //small id
            sendtolobby(player[x].room,-1,s);

            RemoveObjFromFloor(x, player[x].teamid , player[x].Floor,pint64(@player[x].FloorItems[player[x].Floor][i+8])^);
            delete(player[x].FloorItems[player[x].Floor],i,si^);

        end else
            inc(i,si^);
    end;
end;

procedure AddItemToInv(x:integer;it:ansistring);
var si,id2,m:pdword;
    i,l:integer;
    d:dword;
    s:ansistring;
begin
        //any other items
        s:=#$44#$00#$00#$00#$0F#$05#$00#$00#0#0#0#0#0#0#0#0
        +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
        +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
        +#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0#0
        +#1#0#0#0;
        //create the item
        move(it[1],s[$11],$8); //copy item data

        if s[$11] = #9 then begin
            if length(it) < 9 then it:=it+#1;
            s[$1a]:=it[9]; //copy lvl for disk
        end;
        d:=CreateItemID(x);
        safemove(d,s,9,4);
        safemove(player[x].activechar.charid,s,13,4);
        l:=AddToStorage(x,@s[9],player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);
        if l = -1 then begin
            sendsystemmsg(x,'Your inventory is full.');
            exit; //error
        end;
        //copy the final data
        safemove(player[x].activechar.personalbank[l],s,$9,$8);
        sendtoplayer(x,s);

        //order item picked up?
        TestForOrderItemPickup(x,@player[x].activechar.personalbank[l]);

        sendsystemmsg(x,'Item added to your inventory.');
end;


procedure SendStorage(x:integer);
var s,a,b:ansistring;
    sto:^TStorageInfo;
    i,l:integer;
begin
    s:=#$B4#$00#$00#$00#$0F#$13#$04#$00#$00#$00#$00#$00#$00#$00#$00#$00
	+#$6A#$77#$00#$00#$C8#$00#$00#$00#$04#$00#$00#$00#$00#$00#$00#$01
	+#$90#$01#$00#$00#$00#$00#$00#$00#$01#$01#$01#$01#$F4#$01#$00#$00
	+#$00#$00#$00#$00#$02#$02#$01#$00#$F4#$01#$00#$00#$00#$00#$00#$00
	+#$03#$02#$01#$00#$F4#$01#$00#$00#$00#$00#$00#$00#$04#$02#$01#$00
	+#$F4#$01#$00#$00#$00#$00#$00#$00#$05#$02#$01#$00#$F4#$01#$00#$00
	+#$00#$00#$00#$00#$06#$02#$01#$00#$1E#$00#$00#$00#$00#$00#$00#$00
	+#$0A#$03#$01#$01#$00#$00#$00#$00#$00#$00#$00#$00#$0B#$03#$01#$01
	+#$C8#$00#$00#$00#$00#$00#$00#$00#$0C#$02#$01#$01#$64#$00#$00#$00
	+#$00#$00#$00#$00#$0D#$02#$01#$01#$2C#$01#$00#$00#$04#$00#$00#$00
	+#$0E#$04#$00#$01;
    move(player[x].save.bankmesetas,s[9],8);
    for i:=0 to 11 do begin
        sto:=@s[$15+(i*sizeof(TStorageInfo))];
        sto.max:=200;
        sto.count:=0;
        sto.aviable:=0;
    end;

    SendItemsName(x,player[x].save.BasicStorageCount,$38,@player[x].save.BasicStorage[0]);
    SendItemsName(x,player[x].activechar.CharBankCount,$38,@player[x].activechar.CharBank[0]);

    //player[x].save.BasicStorageCount:=200;

    //set the proper info
    sto:=@s[$15+(0*sizeof(TStorageInfo))];  //basic
    sto.count:=player[x].save.BasicStorageCount;
    sto.aviable:=1;

    {sto:=@s[$15+(9*8)];  //otp
    sto.count:=player[x].save.OTPStorageCount;
    sto.aviable:=1;}

    sto:=@s[$15+(11*sizeof(TStorageInfo))];  //char
    sto.count:=player[x].activechar.CharBankCount;
    sto.aviable:=1;

    l:=0;
    //put the content of the bank here
    a:='';

    setlength(b,player[x].save.BasicStorageCount*$38);
    if player[x].save.BasicStorageCount > 0 then
        move(player[x].save.BasicStorage[0],b[1],length(b));
    a:=a+b;
    inc(l,player[x].save.BasicStorageCount);

    {setlength(b,player[x].save.OTPStorageCount*$38);
    move(player[x].save.OTPStorage[0],b[1],length(b));
    a:=a+b;
    inc(l,player[x].save.OTPStorageCount);
    }

    setlength(b,player[x].activechar.CharBankCount*$38);
    if player[x].activechar.CharBankCount > 0 then
        move(player[x].activechar.CharBank[0],b[1],length(b));
    a:=a+b;
    inc(l,player[x].activechar.CharBankCount);



    s:=s+getmagic(l,$77A5,$c3)+a;

    i:=length(s);
    safemove(i,s,1,4);
    sendtoplayer(x,s);

    {storage name:
        0 = basic
        1 = prem
        2 = ext. storage 1
        3 = ext. storage 2
        4 = ext. storage 3
        5 = ext. storage 4
        6 = ext. storage 5
        7 = temp
        11 = otp
        12 = pso2es
        15 = char     }


{
xxxxxXor: 7725  Sub: 43
xxxxxxXor: 7765  Sub: 03
Xor: 77A5  Sub: C3
Xor: 77E5  Sub: 83   }


end;

procedure DepositeToBank(x:integer;s:ansistring);
var i,l,c:integer;
    id1,id2,id:dword;
    a:ansistring;
begin
    //000000: 18 00 00 00 0F 0F 04 00 37 60 00 00 59 00 00 00     ........7`..Y...      //id
    //000010: 56 5F 9C 00 01 00 0E 00     V_�.....    //qty, storage
    i:=13;
    while i < length(s) do begin
        //find the entry
        move(s[i],id1,4);
        move(s[i+4],id2,4);
        for l:=0 to player[x].activechar.BankCount-1 do begin
            if (player[x].activechar.PersonalBank[l].id1 = id1) and
                (player[x].activechar.PersonalBank[l].id2 = id2) then begin
                    //item found remove it then add it
                    setlength(a,$38);
                    safemove(player[x].activechar.PersonalBank[l],a,1,$38);
                    removeFromStorage(x,id1,id2,byte(s[i+8]),player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);
                    if a[9] = #3 then begin
                        a[19] := s[i+8];

                        id:=CreateItemID(x);
                        safemove(id,a,1,4);
                    end;

                    if s[i+10] = #$e then
                        c := AddToStorage(x,@a[1],player[x].activechar.charbank,@player[x].activechar.charbankcount,200)
                    else c := AddToStorage(x,@a[1],player[x].save.BasicStorage,@player[x].save.BasicStorageCount,200);
                    if a[9] = #3 then move(id1,a[1],4);
                    //send confirmation
                    a:=copy(a,1,8)+#0#0+s[i+8]+#0#$8f#$e6#0#0+a;
                    if a[$19] = #3 then
                    if (l < player[x].activechar.BankCount) and
                        (player[x].activechar.PersonalBank[l].id1 = id1) and
                        (player[x].activechar.PersonalBank[l].id2 = id2) then a[9]:=ansichar(player[x].activechar.PersonalBank[l].count);

                    if s[i+10] = #$e then begin
                        safemove(player[x].activechar.charbank[c],a,17,$38);
                    end else begin
                        safemove(player[x].save.BasicStorage[c],a,17,$38);
                    end;

                    a:=#$5C#$00#$00#$00#$0F#$10#$04#$00#$8F#$E6#$00#$00+a+s[i+10]+#0#0#0#$8e#$e6#0#0;
                    sendtoplayer(x,a);
                    break;
                end;
        end;
        inc(i,12); //next entry
    end;

end;


procedure ProcessWidraw(x,cnt,str:integer;id1,id2:dword;var itms:array of tinventoryitem;itmcount:psmallint);
var id:dword;
    a:ansistring;
    l,c:integer;
begin
    for l:=0 to itmcount^-1 do begin
            if (itms[l].id1 = id1) and (itms[l].id2 = id2) then begin
                    //item found remove it then add it
                    setlength(a,$38);
                    safemove(itms[l],a,1,$38);
                    removeFromStorage(x,id1,id2,cnt,itms,itmcount,200);
                    if a[9] = #3 then begin
                        a[19] := ansichar(cnt);

                        id:=CreateItemID(x);
                        move(id,a[1],4);
                    end;

                    c := AddToStorage(x,@a[1],player[x].activechar.PersonalBank,@player[x].activechar.bankcount,50);

                    if a[9] = #3 then move(id1,a[1],4);
                    //send confirmation
                    a:=copy(a,1,8)+#0#0+chr(cnt)+#0+chr(str)+#0#0#0#$91#$f1#0#0+a;
                    if a[$1d] = #3 then
                    if (l < itmCount^) and (itms[l].id1 = id1) and
                        (itms[l].id2 = id2) then a[9]:=ansichar(itms[l].count);

                    safemove(player[x].activechar.PersonalBank[c],a,21,$38);


                    a:=#$5C#$00#$00#$00#$0F#$12#$04#$00#$91#$f1#$00#$00+a+chr(cnt)+#0#0#0;
                    sendtoplayer(x,a);
                    break;
                end;
        end;
end;

procedure WidrawFromBank(x:integer;s:ansistring);
var i,l,c:integer;
    id1,id2:dword;
begin
    //000000: 18 00 00 00 0F 0F 04 00 37 60 00 00 59 00 00 00     ........7`..Y...      //id
    //000010: 56 5F 9C 00 01 00 0E 00     V_�.....    //qty, storage
    i:=13;
    while i < length(s) do begin
        //find the entry
        move(s[i],id1,4);
        move(s[i+4],id2,4);
        if s[i+10] = #$e then begin
            ProcessWidraw(x,byte(s[i+8]),byte(s[i+10]),id1,id2,player[x].activechar.charbank,@player[x].activechar.charbankcount);
        end else begin
            ProcessWidraw(x,byte(s[i+8]),byte(s[i+10]),id1,id2,player[x].save.BasicStorage,@player[x].save.BasicStoragecount);
        end;
        inc(i,12); //next entry
    end;

end;



procedure ProcessMoveBank(x,cnt,str,strsrc:integer;id1,id2:dword;var itms:array of tinventoryitem;itmcount:psmallint;
                var itms2:array of tinventoryitem;itm2count:psmallint);
var id:dword;
    a:ansistring;
    l,c:integer;
begin
    for l:=0 to itmcount^-1 do begin
            if (itms[l].id1 = id1) and (itms[l].id2 = id2) then begin
                    //item found remove it then add it
                    setlength(a,$38);
                    safemove(itms[l],a,1,$38);
                    removeFromStorage(x,id1,id2,cnt,itms,itmcount,200);
                    if a[9] = #3 then begin
                        a[19] := ansichar(cnt);

                        id:=CreateItemID(x);
                        move(id,a[1],4);
                    end;

                    c := AddToStorage(x,@a[1],itms2,itm2count,200);

                    if a[9] = #3 then move(id1,a[1],4);
                    //send confirmation
                    a:=a+chr(strsrc)+#0#0#0#$91#$9A#$00#$00#$90#$9A#$00#$00+copy(a,1,8)+chr(str)+#0+chr(cnt)+#0;

                    if a[$9] = #3 then
                    if (l < itmCount^) and (itms[l].id1 = id1) and
                        (itms[l].id2 = id2) then a[$4f]:=ansichar(itms[l].count);

                    safemove(itms2[c],a,1,$38);


                    a:=#$60#$00#$00#$00#$0F#$19#$04#$00#$90#$9A#$00#$00+a+#0#0#0#0;
                    sendtoplayer(x,a);
                    break;
                end;
        end;
end;

procedure DepositeFromBank(x:integer;s:ansistring);
var i,l,c:integer;
    id1,id2:dword;
begin
    //000000: 1C 00 00 00 0F 18 04 00 00 00 0E 00 66 14 00 00     ............f... //from, magic
    //000010: 3A 79 98 27 3D D8 C7 0D 01 00 00 00     :y�'=��..... //id, count, storage
    i:=17;
    while i < length(s) do begin
        //find the entry
        move(s[i],id1,4);
        move(s[i+4],id2,4);
        if s[i+10] = #$e then begin
            ProcessMoveBank(x,byte(s[i+8]),byte(s[i+10]),byte(s[11]),id1,id2,player[x].activechar.charbank,@player[x].activechar.charbankcount
            ,player[x].save.BasicStorage,@player[x].save.BasicStoragecount);
        end else begin
            ProcessMoveBank(x,byte(s[i+8]),byte(s[i+10]),byte(s[11]),id1,id2,player[x].save.BasicStorage,@player[x].save.BasicStoragecount
            ,player[x].activechar.charbank,@player[x].activechar.charbankcount);
        end;
        inc(i,12); //next entry
    end;

end;


procedure bankmesetas(x:integer;s:ansistring);
var amt:int64;
    a:ansistring;
begin
    move(s[9],amt,8);
    if (s[$11] = #1) then begin
        //bank it
        if amt > player[x].activeChar.Mesetas then exit; //fail
        dec(player[x].activeChar.Mesetas,amt);
        inc(player[x].save.BankMesetas,amt);
    end;
    if (s[$11] = #2) then begin
        //get it
        if amt > player[x].save.BankMesetas then exit; //fail
        inc(player[x].activeChar.Mesetas,amt);
        dec(player[x].save.BankMesetas,amt);
    end;
    UpdateMesetas(x);
    //update bank mesetas
    a:=#$10#$00#$00#$00#$0F#$16#$00#$00#$F9#$00#$00#$00#$00#$00#$00#$00;
    safemove(player[x].save.BankMesetas,a,9,8);
    sendtoplayer(x,a);
end;

procedure UnlockFashionMenu(x:integer);
begin
    sendtoplayer(x,#$4C#$01#$00#$00#$11#$C2#$00#$00#$01#$00#$00#$00#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF
        +#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF#$FF);
end;

procedure FashionEdit(x:integer;s:ansistring);
var p:pansichar;
    a:ansistring;
begin
    {000000: EC 00 00 00 0F B7 00 00 00 00 00 00 00 00 00 00     �....�..........
000010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000030: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000040: 00 00 00 00 00 00 00 00 02 00 01 00 00 00 74 27     ..............t' //02 00 01 = priority, 0x4e = hair style
000050: 12 27 13 27 00 00 18 2A 31 75 31 75 00 00 00 00     .'.'...*1u1u.... //body paint 1, body paint 2, ?? to 0x1a4, ?? to 0x1c6, ?? to 0x1c8
000060: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000070: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
000080: 00 00 00 00 00 00 00 00 00 00 00 00 14 00 46 00     ..............F. //access 1 to 4
000090: 08 00 28 00 00 00 00 00 00 00 00 00 00 00 00 00     ..(.............
0000A0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000B0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000C0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000D0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00     ................
0000E0: 00 00 00 00 00 00 00 00 00 00 00 00     ............}
    p:=@player[x].activechar.base.name[0];
    move(s[$49],p[$1fa - $70],4); //priority
    move(s[$4f],p[$1b6 - $70],2); //hair
    move(s[$51],p[$1a6 - $70],2); // body paint 1
    move(s[$53],p[$1ca - $70],2); // body paint 2
    move(s[$57],p[$1a4 - $70],2); //  ??
    move(s[$59],p[$1c6 - $70],4); //  ?? ??
    move(s[$8d],p[$1b8 - $70],6); //  access 1 - 3
    move(s[$93],p[$1c4 - $70],2); //  access 4

    //confirm edit
    a:=#$F8#$00#$00#$00#$0F#$B8#$00#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#4#0#0#0+copy(s,9,$e4);
    safemove(player[x].gid,a,9,4);
    SendToLobby(player[x].room,-1,a);

end;

procedure SetAccessoryFashion(x:integer;s:ansistring);
var i,c:integer;
    p:pansichar;
    a:ansistring;
begin
    //clean all accessory
    p:=@player[x].activechar.base.name[0];
    //i:=0;
    //move(i,p[$1b8 - $70],4); //  access 1 - 2
    //move(i,p[$1bc - $70],2); //  access 3
    //move(i,p[$1c4 - $70],2); //  access 4
    c:=13;
    while c < length(s) do begin
        i:=byte(s[c]);
        if i = 0 then move(s[c+4],p[$1b8 - $70],2);
        if i = 1 then move(s[c+4],p[$1ba - $70],2);
        if i = 2 then move(s[c+4],p[$1bc - $70],2);
        if i = 3 then move(s[c+4],p[$1c4 - $70],2);
        inc(c,$14);
    end;
    c:=length(s) - 12;
    a:=#$2C#$00#$00#$00#$0F#$88#$04#$00#$56#$5F#$9C#$00#$00#$00#$00#$00#4#0#0#0
        +getmagic(c div $14,$9704,$48)+copy(s,13,c);
    safemove(player[x].gid,a,9,4);
    c:=length(a);
    safemove(c,a,1,4);
    sendtolobby(player[x].room,-1,a);
end;


procedure TrashStuff(x:integer;s:ansistring);
var i,l,m,k:integer;
    cnt:pword;
    a,b:ansistring;
    id:dword;
begin
    i:=13;
    while i < length(s)-4 do begin
        //process every entry
        cnt:=@s[i+8];

        //find the item in bank
        for m:=0 to player[x].activechar.BankCount-1 do
            if (pdword(@s[i])^ = player[x].activeChar.PersonalBank[m].id1)
                and (pdword(@s[i+4])^ = player[x].activeChar.PersonalBank[m].id2) then begin
                    a:=#$1C#$00#$00#$00#$0F#$06#$04#$00#$F0#$AD#$00#$00#$35#$D7#$AD#$A1
                    +#$B9#$28#$A6#$04#$00#$00#$01#$00#$01#$00#$00#$00;
                    safemove(player[x].activeChar.PersonalBank[m].id1,a,13,4);
                    safemove(player[x].activeChar.PersonalBank[m].id2,a,17,4);
                    if player[x].activeChar.PersonalBank[m].ItemType = 3 then begin
                        a[$15]:=ansichar(player[x].activeChar.PersonalBank[m].count-cnt^);
                        if a[$15] <> #0 then a[$19]:=#2;
                    end;

                    removeFromStorage(x,player[x].activeChar.PersonalBank[m].id1,player[x].activeChar.PersonalBank[m].id2,cnt^
                        ,player[x].activeChar.PersonalBank,@player[x].activechar.bankcount,50);
                    sendtoplayer(x,a);
                    break;
                end;

        inc(i,12);     
    end;
end;


procedure TrashStuffBank(x:integer;s:ansistring);
var i,l,m,k:integer;
    cnt:pword;
    a,b:ansistring;
    id:dword;
begin
    i:=13;
    while i < length(s)-4 do begin
        //process every entry
        cnt:=@s[i+8];

        //find the item in bank
        for m:=0 to player[x].activechar.CharBankCount-1 do
            if (pdword(@s[i])^ = player[x].activeChar.CharBank[m].id1)
                and (pdword(@s[i+4])^ = player[x].activeChar.CharBank[m].id2) then begin
                    a:=#$30#$00#$00#$00#$0F#$22#$04#$00#$E8#$4D#$00#$00#$E9#$4D#$00#$00
                      +#$AE#$CA#$6C#$D5#$78#$86#$2B#$0D#$00#$00#$01#$00#$00#$00#$00#$00
                      +#$E8#$4D#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                    safemove(player[x].activeChar.CharBank[m].id1,a,17,4);
                    safemove(player[x].activeChar.CharBank[m].id2,a,21,4);
                    if player[x].activeChar.CharBank[m].ItemType = 3 then begin
                        a[$19]:=ansichar(player[x].activeChar.CharBank[m].count-cnt^);
                        a[$1b]:=ansichar(cnt^);
                    end;

                    removeFromStorage(x,player[x].activeChar.CharBank[m].id1,player[x].activeChar.CharBank[m].id2,cnt^
                        ,player[x].activeChar.CharBank,@player[x].activechar.CharBankCount,200);
                    sendtoplayer(x,a);
                    break;
                end;

        //find the item in bank
        for m:=0 to player[x].save.BasicStorageCount -1 do
            if (pdword(@s[i])^ = player[x].save.BasicStorage[m].id1)
                and (pdword(@s[i+4])^ = player[x].save.BasicStorage[m].id2) then begin
                    a:=#$30#$00#$00#$00#$0F#$22#$04#$00#$E8#$4D#$00#$00#$E9#$4D#$00#$00
                      +#$AE#$CA#$6C#$D5#$78#$86#$2B#$0D#$00#$00#$01#$00#$0e#$00#$00#$00
                      +#$E8#$4D#$00#$00#$01#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00#$00;
                    safemove(player[x].save.BasicStorage[m].id1,a,17,4);
                    safemove(player[x].save.BasicStorage[m].id2,a,21,4);
                    if player[x].save.BasicStorage[m].ItemType = 3 then begin
                        a[$19]:=ansichar(player[x].save.BasicStorage[m].count-cnt^);
                        a[$1b]:=ansichar(cnt^);
                    end;

                    removeFromStorage(x,player[x].save.BasicStorage[m].id1,player[x].save.BasicStorage[m].id2,cnt^
                        ,player[x].save.BasicStorage,@player[x].save.BasicStorageCount,200);
                    sendtoplayer(x,a);
                    break;
                end;
        inc(i,12);     
    end;
end;

end.
